<!DOCTYPE html>
<html lang="pt">

	<head>
		<meta http-equiv="Content-Language" content="pt-br">
		<title>Gerenciador de Conteúdo</title>
		<meta charset="utf-8">
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script type="text/javascript" src="lib/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="js/script.js"></script>

	</head>
<html>
	<body>
		<?php
		require_once ('../config.php');
		// require_once ('../conf/conexao.php');
		// require_once ('../funcoes/cadastroPagina.php');

		require_login();
		$USUARIO = &$_SESSION['USER'];
		?>
		
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<ul class="nav">
					
					<li>
						<a href="#">Identificação: <?php echo $USER -> firstname; ?></a>
					</li>
					
				</ul>
			</div>
		</div>
		<br>
		<br>
		<br>
		<?php

			if(empty($_GET['pagina'])){
			//se variavél pagina não existir
			$temp = "principal";
			
			}else{
			if ($_GET['pagina']=="caminhos"){
			$temp = "caminhos";
			
			}
			
			if ($_GET['pagina']=="paginas"){
			$temp = "paginas";
			
			}
			}
			?>
		<div class="container">

			<?php

			if ($temp == "principal") {
				include ('principal.php');
			}
			if ($temp == "caminhos") {
				include ('caminhos.php');
			}
			if ($temp == "paginas") {
				include ('paginas.php');
			}
			?>
			
			
			
			<hr>

			<footer>
				<p>
					&copy; Saber Tecnologias Educacionais 2014
				</p>
			</footer>

		</div>
		<!-- /container -->

	</body>
</html>