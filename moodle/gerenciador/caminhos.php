<h3>Saúde da Família</h3>

<script type="text/javascript">
		function cadastros(nomeFuncaoPhp){
			var hiddenControl = document.getElementById('action');
			var theForm = document.getElementById('formulario');

			 hiddenControl.value = nomeFuncaoPhp;
			 theForm.submit();
			alert('OK!');
		}
	</script>

<div class="accordion" id="accordion2">

	<div class="accordion-group">
		<div class="accordion-heading" style="background-color: #EEEEEE">
			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#turmadois"> Turma 2 </a>
		</div>
		<div id="turmadois" class="accordion-body collapse" >
			<!--<form method="post" action="<?= $action ?>"-->
			<div class="accordion-inner">
				<p>
					<table>
						<tr>
							<td><b>Disciplina</b>:</td>
							<td>
							<select name="disc" id="disc" class="input-large input-combo">
								<option value="disciplina" selected=""> - Selecione a disciplina desejada - </option>
								<option name="disc" value="1">1. Introdução à Educação a Distância e ao Ambiente Virtual de Aprendizagem</option>
								<option name="disc" value="2">2. Saúde e Sociedade</option>
								<option name="disc" value="3">3. Conceitos e Ferramentas da Epidemiologia</option>
								<option name="disc" value="4">4. Processo de Trabalho e Planejamento na Estratégia Saúde da Família</option>
								<option name="disc" value="5">5. Pesquisa e Uso da Informação em Saúde - Unidade 1</option>
								<option name="disc" value="6">6. Avaliação na Estratégia de Saúde da Família</option>
								<option name="disc" value="7">7. Educação Popular em Saúde</option>
								<option name="disc" value="8">8. Pesquisa e Uso da Informação em Saúde - Unidade 2</option>
								<option name="disc" value="9">9. Saúde da Criança</option>
								<option name="disc" value="10">10. Pesquisa e Uso da Informação em Saúde - Unidade 3</option>
								<option name="disc" value="11">11. Saúde do Adolescente e do Jovem</option>
								<option name="disc" value="12">12. Saúde do Adulto I</option>
								<option name="disc" value="13">13. Saúde do Adulto II</option>
								<option name="disc" value="14">14. Saúde do Idoso</option>
								<option name="disc" value="15">15. Defesa do TCC e final do curso</option>
							</select></td>
						</tr>

						<label>Disciplina Vigente:</label>
						<select name="disc_vigente" id="disc_vigente">
							<option value="disciplina_vigente" selected="">Escolha</option>									

							<?php 
						 	$buscar = mysql_query("SELECT * FROM mdl_caminho") or die(mysql_error());
								while ($linha = mysql_fetch_array($buscar)) {
					
									echo "<option> ".$linha['link']." </option>";									
								}							
							?>
						</select>

						<!--Ajax p tratar disciplina vigente-->
						<script type="text/javascript">
							$('#disc_vigente').change(function(){ 
								var valor = $('#disc_vigente option:selected').val();
								$.ajax({
								  type: "POST", //METODO
								  url: "funcoes/disc_vigente.php", //Pagina a ser chamada
								  data: { disc_vigente : valor}, //Os dados a serem enviados
								  success: function(msg){ //Funcao de callback
								    alert('id: '+valor);
								    alert(msg);
								  }
								});
						});
						</script>


						<!--Imprime na tela o link da disciplina que foi clicada-->
						<script type="text/javascript">
							$('#disc').change(function(){ 
								var valor = $('#disc option:selected').val();
								$.ajax({
								  type: "POST", //METODO
								  url: "funcoes/saude_familia.php", //Pagina a ser chamada
								  data: { nome_disciplina: valor}, //Os dados a serem enviados
								  success: function(msg){ //Funcao de callback
								    $('input[name=link]').val(msg); //salva o valor do select no input Link
								   // alert('valor correto:'+nome_disciplina);
								  }
								});
						});
						</script>

						
						<script type="text/javascript">
							function deletar(){
								alert('funcao javascript deletar');
								var valor1 = $('#disc option:selected').val();
								
								$.ajax({
								  type: "POST", //METODO
								  url: "funcoes/deletar.php", //Pagina a ser chamada
								  data: { valor_disciplina: valor1}, //Os dados a serem enviados
								  success: function(msg){ //Funcao de callback
								  	//$('input[name=turma]').val(msg); //salva o valor do select no input Link
								  	alert('valor ok:'+valor1);
								  	alert(msg);
								    							    
								  }
								});
							

							}
						</script>

						<!--Esta funcao realiza a inserção/atualização do link do curso de saude da familia-->
						<script type="text/javascript">
							function cadastrar(){
								alert('funcao javascript cadastrar');
								var valor1 = $('#disc option:selected').val(); //pega o valor da opcao clicada.
								var link = $('input[name=link]').val(); //pegando o valor do input do link, para salvar no banco
								//var disc = $('input[name=disc]').val();
								$.ajax({
								  type: "POST", //METODO
								  url: "funcoes/cadastro.php", //Pagina a ser chamada
								  data: { valor_disciplina: valor1, valor_link: link}, //Os dados a serem enviados
								  success: function(msg){ //Funcao de callback
								  	//$('input[name=turma]').val(msg); //salva o valor do select no input Link
								  	alert('valor ok:'+valor1);
								  	alert(msg);
								    							    
								  }
								});
							

							}
						</script>

						<tr>
							<td><b>Link</b>:</td>
							<td>
							<input type="text" name="link" class="input-large input-grande" />
							</td>
						</tr>
					</table>

				</p>

				<p>

					<div class="grupo">
						<input id="cx" type="button" value="Salvar" name="salvar" onclick="cadastrar();" class="btn btn-primary btn-small"/>
						<!--<input id="cx" type="button" value="Editar" name="editar1" class="btn btn-warning btn-small" />-->
						<input id="cx" type="button" value="Excluir" name="excluir" onclick="deletar();" class="btn btn-danger btn-small" />
					</div>
				</p>
			</div>

		</div>
	</div>
	</form>
	
						
						
</div>

</div>

</div>

<div class="container">
	<div>
		<h3>Capacitação para a Atenção e o Cuidado da Saúde Bucal da Pessoa com Deficiência</h3>
	</div>

	<div class="accordion" id="accordion3">
		<div class="accordion-group">
			<div class="accordion-heading" style="background-color: #EEEEEE">
				<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#acpd1"> Turma 1 </a>
			</div>
			<div id="acpd1" class="accordion-body collapse" >
				<div class="accordion-inner">

					<div id="cd">
						<h5><b>Cirurgiões Dentistas</b></h5>
						<input type="radio" name="cd" value="inicia" checked="">
						Início
						<br>
						<input type="radio" name="cd" value="modulo1">
						Módulo 1
						<br>
						<input type="radio" name="cd" value="modulo2">
						Módulo 2
						<br>
						<input type="radio" name="cd" value="modulo3">
						Módulo 3
						<br>
						<input type="radio" name="cd" value="modulo4">
						Módulo 4
						<br>
						<input type="radio" name="cd" value="modulo5">
						Módulo 5
						<br>
						<input type="radio" name="cd" value="modulo6">
						Módulo 6
						<br>
						<input type="radio" name="cd" value="modulo7">
						Módulo 7
						<br>
						<input type="radio" name="cd" value="modulo8">
						Módulo 8
						<br>
						<input type="radio" name="cd" value="modulo9">
						Módulo 9
						<br>
						<input type="radio" name="cd" value="modulo10">
						Módulo 10
						<br>
						<input type="radio" name="cd" value="modulo11">
						Módulo 11
						<br>
						<input type="radio" name="cd" value="modulo12">
						Módulo 12
						<br>
					</div>

					<div id="sb">
						<h5><b>Auxiliar de Saúde Bucal</b></h5>
						<input type="radio" name="sb" value="inicia" checked="">
						Início
						<br>
						<input type="radio" name="sb" value="modulo01">
						Módulo 1
						<br>
						<input type="radio" name="sb" value="modulo02">
						Módulo 2
						<br>
						<input type="radio" name="sb" value="modulo03">
						Módulo 3
						<br>
						<input type="radio" name="sb" value="modulo04">
						Módulo 4
						<br>
						<input type="radio" name="sb" value="modulo05">
						Módulo 5
						<br>
						<input type="radio" name="sb" value="modulo06">
						Módulo 6
						<br>
						<input type="radio" name="sb" value="modulo07">
						Módulo 7
						<br>
						<input type="radio" name="sb" value="modulo08">
						Módulo 8
						<br>
						<input type="radio" name="sb" value="modulo09">
						Módulo 9
						<br>
						
						<input type="radio" name="sb" value="modulo010">
						Módulo 10
						<br>
						<input type="radio" name="sb" value="modulo011">
						Módulo 11
						<br>
					</div>

					<br>
					<!--<input id="cx" type="submit" value="Salvar" name="salvar" class="btn btn-primary btn-small"/>-->

				</div>

			</div>
		</div>

		<div class="accordion-group">
			<div class="accordion-heading" style="background-color: #EEEEEE">
				<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#acpd2"> Turma 2 </a>
			</div>
			<div id="acpd2" class="accordion-body collapse" >
				<div class="accordion-inner">

					<div id="cdt2">
						<h5><b>Cirurgiões Dentistas</b></h5>
						<input type="radio" name="cdt2" value="iniciat2" checked="">
						Início
						<br>
						<input type="radio" name="cdt2" value="modulo1t2">
						Módulo 1
						<br>
						<input type="radio" name="cdt2" value="modulo2t2">
						Módulo 2
						<br>
						<input type="radio" name="cdt2" value="modulo3t2">
						Módulo 3
						<br>
						<input type="radio" name="cdt2" value="modulo4t2">
						Módulo 4
						<br>
						<input type="radio" name="cdt2" value="modulo5t2">
						Módulo 5
						<br>
						<input type="radio" name="cdt2" value="modulo6t2">
						Módulo 6
						<br>
						<input type="radio" name="cdt2" value="modulo7t2">
						Módulo 7
						<br>
						<input type="radio" name="cdt2" value="modulo8t2">
						Módulo 8
						<br>
						<input type="radio" name="cdt2" value="modulo9t2">
						Módulo 9
						<br>
						<input type="radio" name="cdt2" value="modulo10t2">
						Módulo 10
						<br>
						<input type="radio" name="cdt2" value="modulo11t2">
						Módulo 11
						<br>
						<input type="radio" name="cdt2" value="modulo12t2">
						Módulo 12
						<br>
					</div>

					<div id="sbt2">
						<h5><b>Auxiliar de Saúde Bucal</b></h5>
						<input type="radio" name="sbt2" value="iniciat2" checked="">
						Início
						<br>
						<input type="radio" name="sbt2" value="modulo01t2">
						Módulo 1
						<br>
						<input type="radio" name="sbt2" value="modulo02t2">
						Módulo 2
						<br>
						<input type="radio" name="sbt2" value="modulo03t2">
						Módulo 3
						<br>
						<input type="radio" name="sbt2" value="modulo04t2">
						Módulo 4
						<br>
						<input type="radio" name="sbt2" value="modulo05t2">
						Módulo 5
						<br>
						<input type="radio" name="sbt2" value="modulo06t2">
						Módulo 6
						<br>
						<input type="radio" name="sbt2" value="modulo07t2">
						Módulo 7
						<br>
						<input type="radio" name="sbt2" value="modulo08t2">
						Módulo 8
						<br>
						<input type="radio" name="sbt2" value="modulo09t2">
						Módulo 9
						<br>
						<input type="radio" name="sbt2" value="modulo010t2">
						Módulo 10
						<br>
						<input type="radio" name="sbt2" value="modulo011t2">
						Módulo 11
						<br>
					</div>

					<br>
					<input id="cx" type="submit" value="Salvar" name="salvar" class="btn btn-primary btn-small"/>

				</div>

			</div>
		</div>
		<div class="accordion-group">
			<div class="accordion-heading" style="background-color: #EEEEEE">
				<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#acpd3"> Turma 3 </a>
			</div>
			<div id="acpd3" class="accordion-body collapse" >
				<div class="accordion-inner">

					<div id="cdt3">
						<h5><b>Cirurgiões Dentistas</b></h5>
						<input type="radio" name="cdt3" value="iniciat3" checked="">
						Início
						<br>
						<input type="radio" name="cdt3" value="modulo1t3">
						Módulo 1
						<br>
						<input type="radio" name="cdt3" value="modulo2t3">
						Módulo 2
						<br>
						<input type="radio" name="cdt3" value="modulo3t3">
						Módulo 3
						<br>
						<input type="radio" name="cdt3" value="modulo4t3">
						Módulo 4
						<br>
						<input type="radio" name="cdt3" value="modulo5t3">
						Módulo 5
						<br>
						<input type="radio" name="cdt3" value="modulo6t3">
						Módulo 6
						<br>
						<input type="radio" name="cdt3" value="modulo7t3">
						Módulo 7
						<br>
						<input type="radio" name="cdt3" value="modulo8t3">
						Módulo 8
						<br>
						<input type="radio" name="cdt3" value="modulo9t3">
						Módulo 9
						<br>
						<input type="radio" name="cdt3" value="modulo10t3">
						Módulo 10
						<br>
						<input type="radio" name="cdt3" value="modulo11t3">
						Módulo 11
						<br>
						<input type="radio" name="cdt3" value="modulo12t3">
						Módulo 12
						<br>
					</div>

					<div id="sbt3">
						<h5><b>Auxiliar de Saúde Bucal</b></h5>
						<input type="radio" name="sbt3" value="iniciat3" checked="">
						Início
						<br>
						<input type="radio" name="sbt3" value="modulo01t3">
						Módulo 1
						<br>
						<input type="radio" name="sbt3" value="modulo02t3">
						Módulo 2
						<br>
						<input type="radio" name="sbt3" value="modulo03t3">
						Módulo 3
						<br>
						<input type="radio" name="sbt3" value="modulo04t3">
						Módulo 4
						<br>
						<input type="radio" name="sbt3" value="modulo05t3">
						Módulo 5
						<br>
						<input type="radio" name="sbt3" value="modulo06t3">
						Módulo 6
						<br>
						<input type="radio" name="sbt3" value="modulo07t3">
						Módulo 7
						<br>
						<input type="radio" name="sbt3" value="modulo08t3">
						Módulo 8
						<br>
						<input type="radio" name="sbt3" value="modulo09t3">
						Módulo 9
						<br>
						<input type="radio" name="sbt3" value="modulo010t3">
						Módulo 10
						<br>
						<input type="radio" name="sbt3" value="modulo011t3">
						Módulo 11
						<br>
					</div>

					<br>
					<input id="cx" type="submit" value="Salvar" name="salvar" class="btn btn-primary btn-small"/>

				</div>

			</div>
		</div>
	</div>

	<script type="text/javascript">
				// UPDATE da disciplina de CD
								$('#cd').change(function(){

									//qnd se trabalha com radio button, a variavel valor1 
									//precisa receber o inout correto, o que difere da sintaxe do selct option.
									var valor1 = $("input[name='cd']:checked").val(); //pega o valor da opcao clicada.
									$.ajax({
									  type: "POST", //METODO
									  url: "funcoes/cd.php", //Pagina a ser chamada
									  data: { valor_radio: valor1}, //Os dados a serem enviados
									  success: function(msg){ //Funcao de callback
									  	$('input[name=cd]').val(msg); //salva o valor do select no input Link								  	
									  	//alert('valor cd:'+valor1);
									  	alert(msg);
									    							    
								  }
								});

								}); //ultimo		
						</script>	



	<script type="text/javascript">
								$('#sb').change(function(){

									//qnd se trabalha com radio button, a variavel valor1 
									//precisa receber o inout correto, o que difere da sintaxe do selct option.
									var valor_sb = $("input[name='sb']:checked").val(); //pega o valor da opcao clicada.
									$.ajax({
									  type: "POST", //METODO
									  url: "funcoes/sb.php", //Pagina a ser chamada
									  data: { valor_radio_sb: valor_sb}, //Os dados a serem enviados
									  success: function(msg){ //Funcao de callback
									  	$('input[name=sb]').val(msg); //salva o valor do select no input Link								  	
									  	//alert('valor cd:'+valor1);
									  	alert(msg);
									    							    
								  }
								});

								}); //ultimo		
						</script>

						<script type="text/javascript">
								$('#cdt2').change(function(){

									//qnd se trabalha com radio button, a variavel valor1 
									//precisa receber o inout correto, o que difere da sintaxe do selct option.
									var valor1 = $("input[name='cdt2']:checked").val(); //pega o valor da opcao clicada.
									$.ajax({
									  type: "POST", //METODO
									  url: "funcoes/cd_t2.php", //Pagina a ser chamada
									  data: { valor_cd: valor1}, //Os dados a serem enviados
									  success: function(msg){ //Funcao de callback
									  	$('input[name=cdt2]').val(msg); //salva o valor do select no input Link								  	
									  	//alert('valor cd:'+valor1);
									  	alert(msg);
									    							    
								  }
								});

								}); //ultimo		
						</script>

						<script type="text/javascript">
								$('#sbt2').change(function(){

									//qnd se trabalha com radio button, a variavel valor1 
									//precisa receber o inout correto, o que difere da sintaxe do selct option.
									var valor = $("input[name='sbt2']:checked").val(); //pega o valor da opcao clicada.
									$.ajax({
									  type: "POST", //METODO
									  url: "funcoes/sb_t2.php", //Pagina a ser chamada
									  data: { valor_sb: valor}, //Os dados a serem enviados
									  success: function(msg){ //Funcao de callback
									  	$('input[name=sbt2]').val(msg); //salva o valor do select no input Link								  	
									  	//alert('valor cd:'+valor1);
									  	alert(msg);
									    							    
								  }
								});

								}); //ultimo		
						</script>

						<script type="text/javascript">
								$('#cdt3').change(function(){

									//qnd se trabalha com radio button, a variavel valor1 
									//precisa receber o inout correto, o que difere da sintaxe do selct option.
									var valor1 = $("input[name='cdt3']:checked").val(); //pega o valor da opcao clicada.
									$.ajax({
									  type: "POST", //METODO
									  url: "funcoes/cd_t3.php", //Pagina a ser chamada
									  data: { valor_cd3: valor1}, //Os dados a serem enviados
									  success: function(msg){ //Funcao de callback
									  	$('input[name=cdt3]').val(msg); //salva o valor do select no input Link								  	
									  	//alert('valor cd:'+valor1);
									  	alert(msg);
									    							    
								  }
								});

								}); //ultimo		
						</script>

						<script type="text/javascript">
								$('#sbt3').change(function(){

									//qnd se trabalha com radio button, a variavel valor1 
									//precisa receber o inout correto, o que difere da sintaxe do selct option.
									var valor = $("input[name='sbt3']:checked").val(); //pega o valor da opcao clicada.
									$.ajax({
									  type: "POST", //METODO
									  url: "funcoes/sb_t3.php", //Pagina a ser chamada
									  data: { valor_sb3: valor}, //Os dados a serem enviados
									  success: function(msg){ //Funcao de callback
									  	$('input[name=sbt3]').val(msg); //salva o valor do select no input Link								  	
									  	//alert('valor cd:'+valor1);
									  	alert(msg);
									    							    
								  }
								});

								}); //ultimo		
						</script>
	
<a href="http://ava.unasusufpe.com.br/moodle/extra_moodle/gerenciador" class="btn">Voltar</a>
