      <!-- Main hero unit for a primary marketing message or call to action -->
      <div class="hero-unit">
        <h3>Olá <?php echo $USER->firstname; ?>!</h3>
        <p>Um Sistema de Gerenciamento de Conteúdo permite que o administrador tenha total autonomia sobre o conteúdo das páginas dos cursos e a evolução dos 
        	Caminhos lúdicos, dispensando quase que por completo a assistência de pessoas especializadas para manutenções de rotina. Não há inclusive a necessidade 
        	de um profissional dedicado (webmaster), diminuindo os custos com recursos 
        	humanos. A habilidade necessária para trabalhar com um Sistema de Gerenciamento de Conteúdo não vai muito além dos conhecimentos necessários para 
        	um editor de texto.</p>
        
      </div>

      <!-- Example row of columns -->
      <div class="row">
        <div class="span4">
          <h3>Caminhos Lúdicos</h3>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn" href="http://ava.unasusufpe.com.br/moodle_interno/gerenciador/index.php?pagina=caminhos">Gerenciar</a></p>
        </div>
        <div class="span4">
          <h3>Páginas de disciplinas</h3>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn" href="http://ava.unasusufpe.com.br/moodle_interno/gerenciador/index.php?pagina=paginas">Gerenciar</a></p>
       </div>
        <div class="span4">
          <h3>Saber Comunidades</h3>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn" href="http://ava.unasusufpe.com.br/moodle_interno/gerenciador/index.php?pagina=saber">Gerenciar</a></p>
        </div>
      </div>