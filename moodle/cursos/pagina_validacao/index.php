<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8">

<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<script src="jquery-1.10.1.min.js"></script>
<title>Capacitação para Implantação do Sistema e-SUS na Atenção Básica</title>
<meta name="description" content="">
<meta name="author" content="Rodrigo Lins">

<meta name="viewport" content="width=device-width; initial-scale=1.0">

<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->

</head>

<body>
	<div id="ceu">

		<!--<img class="casa" src="Icn_U1.png" />-->
	</div>
	<div id="grama">
		<h3>Capacitação para Implantação do Sistema e-SUS na Atenção Básica</h3>
		<div class="right">
			<a href="https://sistemas.unasus.gov.br/suporte/index.php"
				target="_blank"> <b class="posit">Suporte</b> <img src="suporte.svg"></a>
		</div>
		<hr />
		<br> Este curso disponibiliza duas modalidades de participação: "<a
			href="https://ufpe.unasus.gov.br/moodle_unasus" target="_blank">Acesso
			como visitante identificado</a>" e  "<a
			href="https://ufpe.unasus.gov.br/moodle_unasus" target="_blank">Matrícula
			para certificação</a>". <br>
		<h4>
			<i>Matrícula na versão do curso com certificação</i>
		</h4>
		Para que você possa realizar o curso e receber a declaração de
		conclusão de curso, é preciso atender às especificações abaixo: <br> <br>
		<input type="text" class="input" name="cpf" maxlength="11"
			placeholder="Ex:04925787454" onblur="return verificarCPF(this.value)" />
		<button id="submit">Verificar</button>
		<span id="loading"></span>
		<div id="container"></div>
		<div id="containerSucess" class="alert alert-success">
			Olá <span class="username"></span>, parabéns... Texto sucesso!
		</div>
		<div id="containerFail" class="alert alert-error">Texto de falha!</div>

	</div>



	<script>
	//focus
	 $(".input").focus();
	//validacao CPF
	function TestaCPF(strCPF) {
	$('#container').html("");
	$('#containerSucess').slideUp(500);
	$('#containerFail').slideUp(500);
    var Soma;
    var Resto;
    Soma = 0;
	if (strCPF == "00000000000") return false;
    
	for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
	Resto = (Soma * 10) % 11;
	
    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;
	
	Soma = 0;
    for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;
	
    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
    return true;
}
//var strCPF = "04925787452";
//alert(TestaCPF(strCPF));

function verificarCPF(cpf){
	var result=TestaCPF(cpf);
	if(result==true){
		post();
	}else{
		$('#container').html("<div class='alert-error'>CPF inválido</div>");
	}
}


		
		//post php
		function post(){
			$('#containerSucess').slideUp(500);
			$('#containerFail').slideUp(500);
			var iconCarregando = $('<br><span>Carregando</span> <img src="loader.gif">');
			var cpf= $('.input').val();
			$.ajax({
	            type      : 'post',
	 
	            url       : 'core.php',
	 
	            data      : 'cpf='+cpf,
	 
	            dataType  : 'html',
	 			beforeSend: function(){
	 	 			$('#loading').html(iconCarregando);
	 	 			//
	 			},
	 			complete:function(){
	 				$(iconCarregando).remove();
	 				//$('#loading').html('Recomeçar os estudos');
	 				
	 			},
	            success : function(data){
		            if(data==""){
		            	$('#containerFail').slideDown(500);
		            }else{
			            $(".username").html(data);
		            	$('#containerSucess').slideDown(500);
			            }
	            	
	                }
	        });
		}

	
		
		
		</script>

</body>
</html>
<style>
.casa {
	margin-bottom: -54px;
}

.right a:link,a:visited {
	text-decoration: none;
	color: rgb(29, 64, 153);
}

.right a:hover {
	text-decoration: none;
	color: rgb(29, 64, 153);
}

.right a:active {
	text-decoration: none color:rgb(29, 64, 153);
}

.right {
	float: right;
	color: rgb(29, 64, 153);
	position: relative;
	margin-top: -37px;
}

.posit {
	position: absolute;
	margin-top: 0px;
	margin-left: -64px;
}

body {
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	line-height: 20px;
	background-color: #fff;
	margin: 0px 0px 0px 0px;
}

#ceu {
	width: 100%;
	padding-top: 47px;
	padding-left: 40px;
	background-color: #1e3042;
	margin-top: -8px;
}

#grama {
	background-color: #fff;
	margin: 60px 40px 40px 40px;
}

.alert-success {
	margin-top: 20px;
	color: #468847;
	background-color: #dff0d8;
	border-color: #d6e9c6;
	border-radius: 5px;
	padding: 10px;
	font-size: 14px;
}

.alert-error {
	margin-top: 20px;
	color: #b94a48;
	background-color: #f2dede;
	border-color: #eed3d7;
	border-radius: 5px;
	padding: 10px;
	font-size: 14px;
}

#containerSucess,#containerFail {
	display: none;
}
</style>
