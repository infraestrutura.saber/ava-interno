/*Alternativas do verdadeiro/falso*/        			
var opcao1 = 'A baciloscopia de escarro deve ser realizada em, no mínimo, duas amostras, sendo uma na ocasião da primeira consulta e a outra na manhã do dia seguinte, independente do resultado da primeira.';
var opcao2 = 'Para o diagnóstico de TB, a baciloscopia deverá ser realizada para pessoas sintomáticas respiratórias. Pessoas com suspeita clínica de TB, mas com tosse de duração inferior a três semanas, não devem realizar o exame.';        			
var opcao3 = 'Febre de caráter intermitente e de duração variada, a depender da espécie do plasmódio.';        			
var opcao4 = 'Manifestações graves, como convulsões, anemia intensa, sangramentos, dispneia,  são mais comuns nas infecções causadas pelo P. vivax.';        			/*array de variaveis que vai para a funcao geradora*/        			
var entrada = [opcao1, opcao2, opcao3, opcao4];        			/*variavel com o name do formulario. Deve ser único na mesma página*/        			
var name = 'tuberculoseex1';        			
/*gabarito da questao.*/        			
var gabarito = '(\'V\', \'F\', \'F\', \'F\')';        			
/*funcao que gera o html*/        			
gerarInputVF(entrada, name, gabarito, 'tuberculoseex1', 37);          		

