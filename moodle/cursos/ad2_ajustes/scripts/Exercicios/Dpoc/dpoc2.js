/*Alternativas do verdadeiro/falso*/
	var opcao1 = 'A Doença Pulmonar Obstrutiva Crônica (DPOC) caracteriza-se pela presença de obstrução crônica do fluxo aéreo que não é totalmente reversível. A obstrução se dá por uma resposta inflamatória exacerbada dos pulmões a partículas tóxicas, causadas, principalmente, pelo tabagismo.';
	var opcao2 = 'A DPOC é a quarta causa principal de morte e, segundo estudos, a única que vem aumentando, podendo se tornar a terceira cause em 2020, devido ao aumento do tabagismo nos países em desenvolvimento e ao envelhecimento da população.';
	var opcao3 = 'A presença de sintomas respiratórios crônicos, como tosse, secreção, dispneia e sibilos nos pacientes com exposição a fatores de risco (tabagismo, poeira ocupacional, fumaça de lenha) ou fatores individuais (deficiência de alfa 1 antritripsina), deve levar à suspeita clínica de DPOC.';
	var opcao4 = 'O Diagnóstico diferencial se dá com várias doenças respiratórias, uma vez que os sinais e sintomas da DPOC são inespecíficos. Asma brônquica é a doença com maior confusão diagnóstica. A asma apresenta boa resposta clínica ao uso de corticoide inalatório.';
	/*array de variaveis que vai para a funcao geradora*/
	var entrada = [opcao1, opcao2, opcao3, opcao4];
	/*variavel com o name do formulario. Deve ser único na mesma página*/
	var name = 'dpocex2';
	/*gabarito da questao.*/
	var gabarito = '(\'F\', \'V\', \'V\', \'F\')';
	/*funcao que gera o html*/
	gerarInputVF(entrada, name, gabarito, 'dpocex2',6); 
