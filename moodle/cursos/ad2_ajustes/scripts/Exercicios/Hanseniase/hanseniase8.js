var alternativas = new Array();
alternativas[0] = "O acompanhamento de pessoas diagnosticadas com hanseníase deve ser realizado em ambulatório especializado em dermatologia.";
alternativas[1] = "Após a alta, o paciente não necessita manter o acompanhamento, uma vez que a doença não apresenta recidiva.";
alternativas[2] = "Os centros de referência e ou hospitais não fazem parte da rede de assistência às pessoas com hanseníase.";
alternativas[3] = "Ao se referenciar a pessoa para outro serviço, todas as informações disponíveis sobre o caso devem ser registradas por escrito.";
var name = "hanseniase8";
gerarinputmultipla(alternativas, name, 3, 61);