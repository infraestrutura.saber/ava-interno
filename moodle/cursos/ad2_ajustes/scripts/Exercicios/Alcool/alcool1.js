var alternativas = new Array();
alternativas[0] = "A dependência química (de álcool e outras drogas) é uma doença aguda, que causa alterações irreversíveis no cérebro.";
alternativas[1] = "Todas as pessoas que experimentam a cocaína se tornam dependentes, e o mesmo ocorre com o crack.";
alternativas[2] = "O cuidado aos usuários de álcool e outras drogas deve ser individualizado com um plano terapêutico singular.";
alternativas[3] = "A abstinência também é um sintoma da dependência e se caracteriza por alteração comportamental e não é fisiológica; decorre da diminuição ou da parada do uso da droga. O suporte social mais importante que o paciente possui é a família.";
var name = "alcoolex1";
gerarinputmultipla(alternativas, name, 2, 51);
