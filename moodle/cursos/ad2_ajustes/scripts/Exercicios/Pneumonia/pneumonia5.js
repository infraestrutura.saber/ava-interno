

var opcao1 = 'A terapia dirigida para patógenos identificados é preferível; para isso, o início do antibiótico deve ser realizado após a cultura de secreção pulmonar.';
var opcao2 = 'Para pacientes previamente sadios, sem comorbidades e sem história de tratamento com antibióticos nos últimos 3 meses, as drogas de primeira escolha são os macrolídeos em monoterapia.';
var opcao3 = 'A equipe deve orientar os pacientes fumantes diagnosticados com PAC a cessarem o tabagismo.';
var opcao4 = 'A gravidade da pneumonia define o local onde o paciente será tratado, assim como o antibiótico a ser prescrito e a via de administração.';
/*array de variaveis que vai para a funcao geradora*/
var entrada = [opcao1, opcao2, opcao3, opcao4];/*variavel com o name do formulario. Deve ser único na mesma página*/
var name = 'pneumoniaex5';/*gabarito da questao.*/
var gabarito = '(\'F\', \'V\', \'V\', \'V\')';/*funcao que gera o html*/
gerarInputVF(entrada, name, gabarito, 'pneumoniaex5', 14);
