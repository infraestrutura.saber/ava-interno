var alternativas = new Array();
alternativas[0] = "Bradicardia (batimentos cardíacos abaixo de 90 por minuto e febre).";
alternativas[1] = "Taquicardia (batimentos cardíacos acima de 90 por minuto) e bradipneia (frequência respiratória abaixo de 16 por minuto).";
alternativas[2] = "Febre (acima de 38 °C ) e taquipneia (aumento da frequência respiratória acima de 20 por minuto).";
alternativas[3] = "Taquipneia (aumento da frequência respiratória acima de 20 por minuto).";
var name = "ituex3";
gerarinputmultipla(alternativas, name, 2, 44);

