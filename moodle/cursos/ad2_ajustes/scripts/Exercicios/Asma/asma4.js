			         			
/*Alternativas do verdadeiro/falso*/        			
var opcao1 = 'A ausência de sibilo à ausculta pulmonar não exclui o diagnóstico de asma.';
var opcao2 = 'Exames adicionais são recomendados nas situações em que não há resposta satisfatória após o tratamento.';        			
var opcao3 = 'Dispneia, sibilância, desconforto torácico e tosse crônica, principalmente à noite ou no início da manhã, sugerem o diagnóstico de asma.';        			
var opcao4 = 'O exame físico de uma pessoa com asma é bastante rico com sinais e sintomas patognomônicos dessa condição.';        			/*array de variaveis que vai para a funcao geradora*/        			
var entrada = [opcao1, opcao2, opcao3, opcao4];        			/*variavel com o name do formulario. Deve ser único na mesma página*/        			
var name = 'asmaex3';        			
/*gabarito da questao.*/        			
var gabarito = '(\'V\', \'V\', \'V\', \'F\')';        			
/*funcao que gera o html*/        			
gerarInputVF(entrada, name, gabarito, 'asmaex3',4);          		
