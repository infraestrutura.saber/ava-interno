var alternativas = new Array();
alternativas[0] = "A fase sintomática inicial caracteriza-se por cefaleia, náuseas, vômitos, mal-estar, cansaço, mialgia.";
alternativas[1] = "Ataque paroxístico que se inicia com calafrios os quais podem durar de 15 minutos a 1 hora, seguidos de febre alta e período de defervescência.";
alternativas[2] = "Febre de caráter intermitente e de duração variada, a depender da espécie do plasmódio.";
alternativas[3] = "Manifestações graves, como convulsões, anemia intensa, sangramentos, dispneia,  são mais comuns nas infecções causadas pelo P. vivax.";
var name = "malariaex1";
gerarinputmultipla(alternativas, name, 3, 29);