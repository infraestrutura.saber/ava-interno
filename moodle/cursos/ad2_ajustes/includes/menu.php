<?php
//require_once "../../../../config.php";                    
//include 'functions/conection.php'; //chamando arquivo php, 
 //include 'functions/funcoes.php'; //este arquivo contem as funcoes de cadastro e impressao dos posts do alunos

session_start('config');
$user_id = $_SESSION['id'];
$username = $_SESSION['cpf'];
//alert_postage($user_id);
?>
<div id="menu">
		 <ul>
			 <li>
			 	<?php 

			 		if(end(explode("/", getcwd())) == "apresentacao"){ 

			 			$nivel = "../";
			 			$bool = false;

		 	 		} else if(end(explode("/", getcwd())) == "forum_discussoes"){

		 	 			$nivel = "../";
			 			$bool = true;

		 	 		} else { 

		 	 			$nivel = "../../../";

						$bool = true; 

					} 

					echo '<a href="'.$nivel.'apresentacao/index.php"><img id="icone_home" src="' .$nivel. 'imagens/home.png">Início</a>';
				?>

			 </li>
			 <li>
				 <a>Unidades didáticas <b class="caret"></b></a>
				 <ul>
				 	<!--UNIDADE 1-->
					<li>
						<a>Doenças respiratórias <b class="caret-right"></b></a>
						<ul class="nivel_1">
								<li id="ps1menu">
									<?php 
									//echo '<a href="'.$nivel.'unidades/unidade1/asma/index.php?uni=1&caso=1&topico=0&resource=1">Asma</a>';
									
									echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\''.$nivel.'\', 1);">Asma</a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>
								</li>
								<li id="ps2menu">
									<?php 
									echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\''.$nivel.'\', 2);">Doença pulmonar obstrutiva crônica (DPOC)</a>';

									//echo '<a href="'.$nivel.'unidades/unidade1/dpoc/index.php?uni=1&caso=2&topico=0&resource=70"></a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>
								</li>
								<li id="ps3menu">
									<?php 
									echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\''.$nivel.'\', 3);">Pneumonia</a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>

								</li>
						</ul>

					</li>
					<!--UNIDADE 2-->
					<li>
						<a >Doenças cardiovasculares<b class="caret-right"></b></a>
						<ul class="nivel_2">
								<li id="ps4menu">
									<?php 
									echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\''.$nivel.'\', 4);">Coronariopatia</a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>

								</li>
								<li id="ps5menu">
									<?php 
									echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\''.$nivel.'\', 5);">Doenças vasculares periféricas</a>';
									//echo '<a href="'.$nivel.'unidades/unidade2/doencas_vasculares/index.php?uni=2&caso=5&topico=0&resource=15"></a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>

								</li>
								<li id="ps6menu">
									<?php 
									echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\''.$nivel.'\', 6);">Insuficiência cardíaca</a>';
									//echo '<a href="'.$nivel.'unidades/unidade2/insuficiencia_cardiaca/index.php?uni=2&caso=6&topico=0&resource=25">Insuficiência cardíaca</a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>

								</li>
						</ul>


					</li>
					<!--UNIDADE 3-->
			      	<li>
			      			<a>Doenças infectocontagiosas <br>parasitárias crônicas<b style="margin-top: -5px;!important" class="caret-right"></b></a>
						<ul class="nivel_3">
								<li id="ps7menu">
									<?php 

									echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\''.$nivel.'\', 7);">Hanseníase</a>';

									//echo '<a href="'.$nivel.'unidades/unidade3/hanseniase/index.php?uni=3&caso=7&topico=0&resource=140">Hanseníase</a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>
								</li>
								<li id="ps8menu">
									<?php 

									echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\''.$nivel.'\', 8);">Síndrome da imunodeficiência adquirida (SIDA)</a>';

									//echo '<a href="'.$nivel.'unidades/unidade3/sida/index.php?uni=3&caso=9&topico=0&resource=1">SIDA</a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>

								</li>
								<li id="ps9menu" >
									<?php 

									echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\''.$nivel.'\', 9);">Malária</a>';
									//echo '<a href="'.$nivel.'unidades/unidade3/malaria/index.php?uni=3&caso=8&topico=0&resource=1">Malária</a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>

								</li>
								<li id="ps10menu">
									<?php 

									echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\''.$nivel.'\', 10);">Tuberculose</a>';

									//echo '<a href="'.$nivel.'unidades/unidade3/tuberculose/index.php?uni=3&caso=10&topico=0&resource=1">Tuberculose</a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>
								</li>
						</ul>

			      	</li>
			      	<!--UNIDADE 4-->
			      	<li>
			      		<a>Infecção do trato urinário<b class="caret-right"></b></a>
						<ul class="nivel_4">
								<li id="ps11menu">
									<?php 
									echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\''.$nivel.'\', 11);">Infec&ccedil;&atilde;o do Trato Urin&aacute;rio</a>';
									//echo '<a href="'.$nivel.'unidades/unidade4/itu/index.php?uni=4&caso=11&topico=0&resource=100">Infecção do trato urinário</a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>
								</li>
						</ul>


			      	</li>

			      	<!--UNIDADE 5-->
			      	<li>
			      		<a>Problemas ortopédicos<b class="caret-right"></b></a>
						<ul class="nivel_5">
								<li id="ps12menu">
									<?php 
									
									echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\''.$nivel.'\', 13);">Osteomielite</a>';

									//echo '<a href="'.$nivel.'unidades/unidade5/osteomielite/index.php?uni=5&caso=13&topico=0&resource=34">Osteomielite</a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>
								</li>

								<li id="ps13menu">
									<?php 

									echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\''.$nivel.'\', 12);">Pós-operatório em ortopedia</a>';
									//echo '<a href="'.$nivel.'unidades/unidade5/pos_ortopedia/index.php?uni=5&caso=12&topico=0&resource=43">Pós-operatório</a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>

								</li>
						</ul>

			      	</li>
			      	<!--UNIDADE6-->
			      	<li>
			      		<a>Transtornos psiquiátricos<b class="caret-right"></b></a>
						<ul class="nivel_6">
								<li id="ps14menu">
									<?php 
									echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\''.$nivel.'\', 14);">Transtornos psiquiátricos</a>';
									//echo '<a href="'.$nivel.'unidades/unidade6/trans_psiquiatricos/index.php?uni=6&caso=14&topico=0&resource=1">Transtornos psiquiátricos</a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>
								</li>
						</ul>


			      	</li>
			      	<!--UNIDADE7-->	
			      	<li> 
			      		<a>Álcool e outras drogas<b class="caret-right"></b></a>
						<ul class="nivel_7">
								<li id="ps15menu">
									<?php 
										echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\''.$nivel.'\', 15);">Alcoolismo e outras drogas</a>';

									//echo '<a href="'.$nivel.'unidades/unidade7/alcool/index.php?uni=7&caso=15&topico=0&resource=97">Álcool e outras drogas</a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>
								</li>
								<!-- <li>
									<?php 
									//echo '<a href="'.$nivel.'unidades/unidade7/drogas/index.php?uni=7&caso=16&topico=0&resource=1">Drogas</a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>

								</li> -->
						</ul>
			      	</li>
			      	<!--UNIDADE8-->
			      	<li>
			      		<a >Doenças neurodegenerativas<b class="caret-right"></b></a>
						<ul class="nivel_8">
								<li id="ps16menu">
									<?php 
									echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\''.$nivel.'\', 17);">Esclerose múltipla</a>';
									//echo '<a href="'.$nivel.'unidades/unidade8/esclerose_multipla/index.php?uni=8&caso=18&topico=0&resource=122">Esclerose múltipla</a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>
								</li>
								<li id="ps17menu">
									<?php
									echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\''.$nivel.'\', 16);">Esclerose lateral</a>'; 
									//echo '<a href="'.$nivel.'unidades/unidade8/esclerose_lateral/index.php?uni=8&caso=17&topico=0&resource=131">Esclerose lateral</a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>

								</li>
						</ul>

			      	</li>
				 </ul>
			 </li>
		 <li>
		 
				<?php  
					echo '<a href="'.$nivel.'forum_discussoes">Fórum de discussões</a>';
				?>

		 </li>
      <li><a class="inline" href="#modal_certificacao" onclick="abreModal();">Certificação</a></li>
      <li><a class="inline" href="#modal_perguntas" onclick="abreModal();">Perguntas frequentes</a></li>
      <li><a class="inline" href="#modal_perfil" onclick="abreModal();">Perfil e opinião</a></li>
      <li><a class="inline" href="#modal_creditos" onclick="abreModal();">Créditos</a></li>
		 </ul>
		<div id="sair">Sair</div>
		<div id="fechar">
			<p>
			<?php
				include('sair.php');
				echo '<img id="icone_sair" src="'. $nivel.'imagens/sair.png"></a>';
			?>
			</p>
		</div>

	  </div>
<?php

	if($bool == true){
		include ("modal_unidades.php");
	}
	 
?>


	   <script type="text/javascript">

							  		function abreModal () {
							  			 $(".inline").colorbox({inline:true, width:"1050px"});

							  			 if (localStorage.getItem('modal_block') == 'on') { 
							  			 		if(document.getElementById('check_modal') != null)
												document.getElementById('check_modal').checked = true;
										 }else{
										 		if(document.getElementById('check_modal') != null)
										 		document.getElementById('check_modal').checked = false;
										 }

							  		}

							  		function abreModalUnidade (tipo) {

							  			if(tipo == "menu"){

								  			if(localStorage.getItem('modal_block') == null){
												abreModal();
												$(".inline").colorbox({inline:true, width:"1050px"});
											}else if(localStorage.getItem('modal_block') == 'off'){
								  				abreModal();
								  				$(".inline").colorbox({inline:true, width:"1050px"});
								  			}else if (localStorage.getItem('modal_block') == 'on') {  
											    $.colorbox.remove();
											}

							  			}else{
							  				abreModal();

							  			}
										

							  		}

							  		function abreModalUnidade (tipo, nivel, caso) {

							  			if(tipo == "menu"){

								  			if(localStorage.getItem('modal_block') == null){
												abreModal();
												$(".inline").colorbox({inline:true, width:"1050px"});
											}else if(localStorage.getItem('modal_block') == 'off'){
								  				abreModal();
								  				$(".inline").colorbox({inline:true, width:"1050px"});
								  			}else if (localStorage.getItem('modal_block') == 'on') {  
												redireciona(nivel,caso);

												$.colorbox.remove();
											}

							  			}else{
							  				abreModal();

							  			}
										

							  		}

							  		function redireciona(nivel, caso){

							  			if(caso == 1){
							  				location.href=nivel + 'unidades/unidade1/asma/index.php?uni=1&caso=1&topico=0&resource=87';

							  			}else if(caso == 2){
							  				location.href=nivel + 'unidades/unidade1/dpoc/index.php?uni=1&caso=2&topico=0&resource=70';

							  			}else if(caso == 3){
							  				location.href=nivel + 'unidades/unidade1/pneumonia/index.php?uni=1&caso=3&topico=0&resource=62';

							  				
							  			}else if(caso == 4){

							  				location.href= nivel +'unidades/unidade2/coronariopatia/index.php?uni=2&caso=4&topico=0&resource=5';
							  				
							  			}else if(caso == 5){

							  				location.href= nivel +'unidades/unidade2/doencas_vasculares/index.php?uni=2&caso=5&topico=0&resource=15';
							  				
							  			}else if(caso == 6){

							  				location.href= nivel +'unidades/unidade2/insuficiencia_cardiaca/index.php?uni=2&caso=6&topico=0&resource=25';

							  				
							  			}else if(caso == 7){

							  				location.href= nivel +'unidades/unidade3/hanseniase/index.php?uni=3&caso=7&topico=0&resource=143';

							  				
							  			}else if(caso == 9){

							  				location.href= nivel +'unidades/unidade3/malaria/index.php?uni=3&caso=8&topico=0&resource=160';

							  				
							  			}else if(caso == 8){

							  				location.href= nivel +'unidades/unidade3/sida/index.php?uni=3&caso=9&topico=0&resource=151';

							  			}else if(caso == 10){

							  				location.href= nivel +'unidades/unidade3/tuberculose/index.php?uni=3&caso=10&topico=0&resource=167';

							  			}else if(caso == 11){

							  				location.href= nivel +'unidades/unidade4/itu/index.php?uni=4&caso=11&topico=0&resource=100';

							  			}else if(caso == 12){

							  				location.href= nivel +'unidades/unidade5/pos_ortopedia/index.php?uni=5&caso=12&topico=0&resource=43';

							  			}else if(caso == 13){

							  				location.href= nivel +'unidades/unidade5/osteomielite/index.php?uni=5&caso=13&topico=0&resource=34';

							  			}else if(caso == 14){

							  				location.href= nivel +'unidades/unidade6/trans_psiquiatricos/index.php?uni=6&caso=14&topico=0&resource=50';

							  			}else if(caso == 15){

							  				location.href= nivel +'unidades/unidade7/alcool/index.php?uni=7&caso=15&topico=0&resource=97';

							  			}else if(caso == 16){

							  				location.href= nivel +'unidades/unidade8/esclerose_lateral/index.php?uni=8&caso=17&topico=0&resource=131';

							  			}else if(caso == 17){

							  				location.href= nivel +'unidades/unidade8/esclerose_multipla/index.php?uni=8&caso=18&topico=0&resource=122';

							  			}
							  		}



		</script>

			<!-- Certificacao-->
			<div style='display:none'>
				<div id="modal_certificacao" style='padding:10px; background:#fff;'>
					<h4>Certificação</h4>

					<br>

					Para que você receba a declaração de conclusão deste módulo, é necessário fazer a avaliação  final. Recomendamos que esta seja a última etapa, depois que você tiver concluído seus estudos.  <br><br>
					A avaliação vale de zero (0,0) a dez (10,0). Para receber a declaração de conclusão do módulo é necessário tirar, pelo menos, 7,0 (sete). São permitidas 3 (três tentativas). Caso você  não atinja nota mínima 7,0 (sete), será necessário fazer o curso novamente.   Após atingir nota igual ou superior a 7,0 (sete), você receberá, por e-mail, um link que  permitirá a impressão do certificado. O link será encaminhado em até 72 horas após a conclusão da avaliação.   <br><br>
					Clique <a href="http://ufpe.unasus.gov.br/moodle_unasus/mod/quiz/view.php?id=21" target="_blank">aqui</a> para iniciar a avaliação final.

			
				<br>
				
				</div>
			</div><!-- fim -->

			<!-- Perguntas-->
			<div style='display:none'>
				<div id="modal_perguntas" style='padding:10px; background:#fff;'>
					<h4>Perguntas frequentes</h4>

					<br>

					Página em construção.

			
				<br>
				
				</div>
			</div><!-- fim -->

			<!-- Opiniao-->
			<div style='display:none'>
				<div id="modal_perfil" style='padding:10px; background:#fff;'>
					<h4>Perfil e opinião</h4>

					<br>
				Gostaríamos de conhecer o seu perfil tecnológico e a sua opinião sobre o curso.  <br>
				Clique <a href=" http://ufpe.unasus.gov.br/moodle_unasus/mod/feedback/view.php?id=20" target="_blank">aqui</a> para responder ao questionário.


			
				<br>
				
				</div>
			</div><!-- fim -->

			<!-- Creditos-->
			<div style='display:none'>
				<div id="modal_creditos" style='padding:10px; background:#fff;' style="font-size: 14">
					<h4>Créditos</h4>
						<br>
						<b>© 2014. Ministério da Saúde. Secretaria de Atenção à Saúde.  Departamento de Atenção Básica. 
						<br>Coordenação Geral de Atenção Domiciliar.</b><br><br>
Todos os direitos reservados. É permitida a reprodução parcial ou total desta obra, desde que citada a  fonte e que não seja para venda ou qualquer fim comercial. A responsabilidade pelos direitos autorais  de textos e imagens desta obra é de responsabilidade da área técnica.<br>
						<br>

				<div id="left">
				<!-- <div class="span6 no_left"> -->
				
					<b>Curso online Abordagem de situações clínicas comuns em adultos</b><br>
					*Primeira oferta: março de 2014 <br><br>

					<b>Elaboração, difusão e informações</b><br>
	Universidade Aberta do Sistema Único De Saúde (UNA-SUS)<br>
	Secretaria de Gestão do Trabalho e da Educação Na Saúde (SGTES)<br>
	Secretaria de Vigilância em Saúde - Programa Nacional de Imunização (SVS)<br><br>


	<b>UNA-SUS | Universidade Federal de Pernambuco</b><br>

	Coordenação Geral: Cristine Martins Gomes de Gusmão<br>
	Coordenação Técnica: Josiane Lemos Machiavelli<br>
	Coordenadora de Educação a Distância: Sandra de Albuquerque Siebra<br>
	Gerente de Projetos: Júlio Venâncio de Menezes Júnior<br>
	</p>
	<br>
	<p><b><u><i>Equipe de produção do curso</i></u></b></p>


	<p><b>Editora</b><br>
	Josiane Lemos Machiavelli (UNA-SUS UFPE)<br><br>

	<b>Conteudistas</b><br>
	Maria Beatriz Kneipp (Instituto Nacional de Câncer José Alencar Gomes da Silva)<br>
	Maria Cristina Willemann (Programa Nacional de Imunização)<br>
	Helena Keico Sato - Divisão de Imunização da Secretaria Estadual de Saúde de São Paulo<br><br>

	<b>Conteudista colaboradora - Unidade 1</b><br>
	Flávia de Miranda Corrêa (Instituto Nacional de Câncer José Alencar Gomes da Silva e Instituto Fernandes Figueira)
<br><br>

	<b>Designer instrucional</b><br>
	Patrícia Pereira da Silva (UNA-SUS UFPE)<br><br>

	<b>Designers gráficos</b><br>
	Juliana Leal (UNA-SUS UFPE)<br>Rodrigo Lins (UNA-SUS UFPE)<br><br>

	<b>Ilustrações</b><br>
	Cleyton Nicollas de Oliveira Guimarães (UNA-SUS UFPE)<br>
	Juliana Leal (UNA-SUS UFPE)<br>
	Vinícius Haniere Saraiva Milfont (UNA-SUS UFPE)<br>
	</p>

</div>

				
<div id="right">
<!-- <div class="span6"> -->
	<p>
	<b>Produção audiovisual</b><br>
	Caroline Barbosa Rangel (UNA-SUS UFPE)<br>
	Geraldo Luiz Monteiro do Nascimento (UNA-SUS UFPE)<br>
	Nahyara Batista da Silva (UNA-SUS UFPE)<br>
	Eduardo Dias (UNA-SUS)<br><br>

	<b>Revisão linguística</b><br>
	Emanuel Cordeiro da Silva  (UNA-SUS UFPE)<br><br>

	<b>Equipe de tecnologia da informação</b><br>
	Lucy do Nascimento Cavalcante (UNA-SUS UFPE)<br>
	Mirela Natali Vieira de Souza (UNA-SUS UFPE)<br>
	Rodrigo Cavalcanti Lins (UNA-SUS UFPE)<br>
	Wellton Thiago Machado Ferreira (UNA-SUS UFPE)<br><br>

	<b>Equipe de ciência da informação</b><br>
	Vildeane da Rocha Borba (UNA-SUS UFPE)<br>
	Jacilene Adriana da Silva Correia (UNA-SUS UFPE)<br><br>

	<b>Equipe de supervisão acadêmica</b><br>
	Fabiana de Barros Lima (UNA-SUS UFPE)<br>
	Geisa Ferreira da Silva (UNA-SUS UFPE)<br>
	Isabella Maria Lorêto da Silva (UNA-SUS UFPE)<br>
	Rosilândia Maria da Silva (UNA-SUS UFPE)<br><br>
	</p>

	<p>
	<b>Colaboradoras na validação interna do curso</b><br>
	Ana Goretti Kalume Maranhão (Programa Nacional de Imunizações)<br>
	Lucinadja Gomes da Silva (Programa Nacional de Imunizações)<br>
	Maria da Guia de Oliveira (Área Técnica da Saúde do Adolescente/Ministério da Saúde)<br>
	Valéria Giorgetti (Programa Nacional de Imunizações)<br><br>

	<b>Colaboradores na validação externa do curso</b><br>
	Célia Vidal de Negreiros (Prefeitura Municipal de Escada-PE)<br>
Geisa Ferreira da Silva (UNA-SUS UFPE)<br>
Margareth B. de Lacerda (Programa de Imunização, Recife-PE)<br>
Maria de Lourdes Pereira da Silva (Prefeitura Municipal  de Escada e Jaboatão dos Guararapes-PE)
<br><br>

	</p>

</div>

			


			
				<br>
				
				</div>
			</div><!-- fim -->


			<style type="text/css">

				#right{
					width: 48%;
					float: right;
					/*font-size: 12px !important;*/
					margin-left: 10px; 

				}

				#left{
					width: 46%;
					float: left;
					/*font-size: 12px !important;*/

				}

				#modal_creditos b{
					font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif; !important;
				}

			</style>

