			<!-- modal Unidade-->
			<div style='display:none'>
				<div id="modal_unidade" style='background:#fff;'>
						<div id="topo_modal">Doenças respiratórias</div>
						<div id="troca">

						</div>
			<div style="position: absolute;bottom: -5;left: 706;">
				<input style="position:absolute;" type="checkbox" name="check_modal" id="check_modal" value=""> <p style="margin-left: 18px;">Não exibir na próxima vez</p>
			</div>

			
				
				</div>
			</div><!-- fim -->


			<div class="hide">

			<!--Caso 1-->
			<div id="caso1" >
				
						<div id="personagem">
							<div id="bloco_esquerdo">
								<div id="corpo_texto">
									<p id="titulo_texto">Abordagem ao paciente com asma no domicílio</p>
									<!-- <p id="descricao_texto">Descricâo do caso clínico</p> -->
									<br>
									<p id="titulo_objetivo">Objetivos</p>
									<p id="texto_objetivo">Esta unidade abordará sobre como conduzir o paciente portador de asma para que permaneça no domicílio clinicamente estável e confortável. Serão tratados dos seguintes aspectos: </p>

									<ul>
										<li>conceito e epidemiologia da asma; </li>
										<li>diagnóstico e avaliação da asma; </li>
										<li>manejo dos pacientes portadores de asma; </li>
										<li>orientações ao cuidador e plano de alta; </li>
										<li>reconhecimento de quando a equipe deve referenciar o caso para uma unidade 
										de saúde. </li>

									</ul>
								
								</div>
								<?php echo '<a href="'.$nivel.'unidades/unidade1/asma/index.php?uni=1&caso=1&topico=0&resource=87">'; ?>
								<button class="btn btn-inverse" id="acesso">Acesso ao conteúdo</button>
								</a><br><br>	
								<div id="barra">
									<p class="porc"><?php porcentagem(1,$cpf);?></p><div id="barra_porc" > 
									<div class="barra_porc_interna" style="width: <?php porcentagem(1,$cpf); ?> "></div></div>
								<?php echo '<img class="icon_evolucao" src="'.$nivel.'imagens/info.png" title="Evolução" alt="Evolução">'; ?>
								</div>
								
							</div>
							<div id="bloco_direito">
								
									<?php echo '<img src="' .$nivel. 'imagens/apresentacao/asma.png" alt="asma" />';?>
									 
								
							</div>
						</div>
						

			</div>
			
			<!--Caso 2-->
			<div id="caso2">
						
						<div id="personagem">
							<div id="bloco_esquerdo">
								<div id="corpo_texto">
									<p id="titulo_texto">Abordagem ao paciente com Doença pulmonar obstrutiva crônica no domicílio</p>
									<!-- <p id="descricao_texto">Descricâo do caso clínico</p> -->
									<br>
									<p id="titulo_objetivo">Objetivos</p>
									<p id="texto_objetivo">

										Esta unidade abordará sobre como conduzir um paciente com Doença Pulmonar Obstrutiva 
Crônica na atenção domiciliar. Serão tratados dos seguintes aspectos: 
									<ul>
										<li>conceito e epidemiologia da DPOC; 
</li>
										<li>diagnóstico e avaliação da DPOC; 
</li>
										<li>manejo dos pacientes portadores da DPOC; 
</li>
										<li>orientações ao cuidador e plano de alta; 
</li>
										<li>reconhecimento de quando a equipe deve referenciar o caso para uma unidade de 
saúde. 
</li>
									</ul>



									</p>
								
								</div>	
								
								<?php echo '<a href="'.$nivel.'unidades/unidade1/dpoc/index.php?uni=1&caso=2&topico=0&resource=70">'; ?>

								<button class="btn btn-inverse" id="acesso">Acesso ao conteúdo</button>
								</a><br><br>
								<div id="barra">
								<p class="porc"><?php porcentagem(2,$cpf);?></p><div id="barra_porc" >
								<div class="barra_porc_interna" style="width: <?php porcentagem(2,$cpf);?>"></div></div>
								<?php echo '<img class="icon_evolucao" src="'.$nivel.'imagens/info.png" title="Evolução" alt="Evolução">'; ?>
							</div>
							</div>
							<div id="bloco_direito">
									
									<?php echo '<img src="' .$nivel. 'imagens/apresentacao/dpoc.png" alt="DPOC" />';?>
									 
									
								
							</div>
						</div>
			</div>
			
			<!--Caso 3-->
			<div id="caso3">

				<div id="personagem">
							<div id="bloco_esquerdo">
								<div id="corpo_texto">
									<p id="titulo_texto">Abordagem ao paciente com pneumonia no domicílio 
</p>
									<!-- <p id="descricao_texto">Descricâo do caso clínico</p> -->
									<br>
									<p id="titulo_objetivo">Objetivos</p>
									<p id="texto_objetivo">
										Esta unidade abordará sobre como conduzir o paciente com pneumonia para que permaneça 
no domicílio clinicamente estável e confortável. Serão abordados os seguintes pontos: 

										<ul>
											<li>conceito e epidemiologia da pneumonia;</li>
											<li>diagnóstico e avaliação dos quadros de pneumonia; </li>
											<li>manejo dos pacientes com pneumonia; </li>
											<li>orientações ao cuidador e plano de alta;</li>
											<li>reconhecimento de quando a equipe deve referenciar o caso para uma unidade de saúde.</li>
										</ul>




									</p>
								
								</div>	
								
								<?php echo '<a href="'.$nivel.'unidades/unidade1/pneumonia/index.php?uni=1&caso=3&topico=0&resource=62">'; ?>
								<button class="btn btn-inverse" id="acesso">Acesso ao conteúdo</button>
								</a>
								<br><br>
								<div id="barra">
									<p class="porc"><?php porcentagem(3,$cpf);?></p><div id="barra_porc" >
									<div class="barra_porc_interna" style="width: <?php porcentagem(3,$cpf);?> "></div></div>
									<?php echo '<img class="icon_evolucao" src="'.$nivel.'imagens/info.png" title="Evolução" alt="Evolução">'; ?>
								</div>
							</div>
							<div id="bloco_direito">
									<?php echo '<img src="' .$nivel. 'imagens/apresentacao/pneumonia.png" alt="pneumonia" />';?>
									 
									
								
							</div>
				</div>
				
			</div>
					

	

			<!--Caso 4-->
			<div id="caso4">
				
					
					
					<div id="personagem">
							<div id="bloco_esquerdo">
								<div id="corpo_texto">
									<p id="titulo_texto">Abordagem ao paciente com cardiopatia isquêmica crônica no domicílio 
</p>
									<!-- <p id="descricao_texto">Descricâo do caso clínico</p> -->
									<br>
									<p id="titulo_objetivo">Objetivos</p>
									<p id="texto_objetivo">
										Esta unidade abordará como conduzir o paciente portador de coronariopatia crônica no 
domicílio. Serão apresentados os seguintes pontos: <br>
			
									<ul>
									<li> conceito e fisiopatologia das coronariopatias; </li>
									<li> diagnóstico e a avaliação corretos das doenças cardíacas supracitadas; </li>
									<li> manejo dos pacientes portadores de coronariopatias, plano de alta e orientações da 
									equipe para o cuidador/paciente/familiares na tentativa de evitar complicações e 
									informar sobre a evolução da doença crônica; </li>
									<li> reconhecimento de quando a equipe deve referenciar o caso para a rede de serviços 
									de saúde. </li>
									</ul>







									</p>
								
								</div>	
								
								<?php echo '<a href="'.$nivel.'unidades/unidade2/coronariopatia/index.php?uni=2&caso=4&topico=0&resource=5">'; ?>
								<button class="btn btn-inverse" id="acesso">Acesso ao conteúdo</button>
								</a>
								<br><br>
								<div id="barra">
										<p class="porc"><?php porcentagem(4,$cpf);?></p><div id="barra_porc" >
										<div class="barra_porc_interna" style="width: <?php porcentagem(4,$cpf);?>"></div></div>
										<?php echo '<img class="icon_evolucao" src="'.$nivel.'imagens/info.png" title="Evolução" alt="Evolução">'; ?>
								</div>
							</div>
							<div id="bloco_direito">
								<?php echo '<img src="' .$nivel. 'imagens/apresentacao/coronariopatia.png" alt="coronariopatia" />';?>
								 
								
							</div>
						</div>
					
					
			</div>
			
			<!--Caso 5-->
			<div id="caso5">
					
					
					<div id="personagem">
							<div id="bloco_esquerdo">
								<div id="corpo_texto">
									<p id="titulo_texto">Abordagem ao paciente com doen&ccedila vascular perif&eacute;rica no domicílio</p>
									<!-- <p id="descricao_texto">Descricâo do caso clínico</p> -->
									<br>
									<p id="titulo_objetivo">Objetivos</p>
									<p id="texto_objetivo">

										Esta unidade abordará como conduzir o paciente portador de Doença Vascular Periférica para 
										que permaneça no domicilio clinicamente estável e confortável. Serão apresentados os 
										seguintes pontos: 
										
									<ul>
										<li> conceito e fisiopatologia da insuficiência vascular periférica; </li>
										<li> diagnóstico e a avaliação da doença vascular periférica; </li>
										<li> manejo dos pacientes portadores de insuficiência vascular periférica e plano de alta; 
										<li> orientações da equipe para o cuidador/paciente/familiares na tentativa de evitar 
										complicações e informar sobre a evolução da doença crônica; </li>
										<li> reconhecimento de quando a equipe deve referenciar o caso para a rede de serviços 
										de saúde. </li>
									</ul>



									</p>
								
								</div>	
								

							
							<?php echo '<a href="'.$nivel.'unidades/unidade2/doencas_vasculares/index.php?uni=2&caso=5&topico=0&resource=15">'; ?>

								<button class="btn btn-inverse" id="acesso">Acesso ao conteúdo</button>
								</a>
								<br><br>
								<div id="barra">
									<p class="porc"><?php porcentagem(5,$cpf);?></p><div id="barra_porc" >
									<div class="barra_porc_interna" style="width: <?php porcentagem(5,$cpf);?>"></div></div>
									<?php echo '<img class="icon_evolucao" src="'.$nivel.'imagens/info.png" title="Evolução" alt="Evolução">'; ?>
								</div>
							</div>
							<div id="bloco_direito">
								
								<?php echo '<img src="' .$nivel. 'imagens/apresentacao/doenc_vascular_periferica.png" alt="Doença Cardiovascular" />';?>								
								 
							</div>
						</div>
					
			</div>
			
			<!--Caso 6-->
			<div id="caso6">
					
					<div id="personagem">
							<div id="bloco_esquerdo">
								<div id="corpo_texto">
									<p id="titulo_texto">Abordagem domiciliar ao paciente com insuficiência cardíaca 
</p>
									<!-- <p id="descricao_texto">Descricâo do caso clínico</p> -->
									<br>
									<p id="titulo_objetivo">Objetivos</p>
									<p id="texto_objetivo">
										Esta unidade abordará sobre como conduzir o paciente portador de insuficiência cardíaca para 
que permaneça no domicílio clinicamente estável e confortável. Serão tratados dos seguintes 
aspectos: <br>


										<ul>

										 <li> conceito e fisiopatologia da insuficiência cardíaca; </li>
										<li> diagnóstico e a avaliação da insuficiência cardíaca; </li>
										<li> manejo dos pacientes portadores da insuficiência cardíaca e plano de alta; </li>
										<li> orientações da equipe para o cuidador/paciente/familiares na tentativa de evitar 
										complicações e informar sobre a evolução da doença crônica;</li> 
										<li> reconhecimento de quando a equipe deve referenciar o caso para a rede de serviços 
										de saúde.</li>

										</ul>



									</p>
								
								</div>	
								
							<?php echo '<a href="'.$nivel.'unidades/unidade2/insuficiencia_cardiaca/index.php?uni=2&caso=6&topico=0&resource=25">'; ?>
								<button class="btn btn-inverse" id="acesso">Acesso ao conteúdo</button>
								</a><br><br>
								<div id="barra">
									<p class="porc"><?php porcentagem(6,$cpf);?></p><div id="barra_porc" >
									<div class="barra_porc_interna" style="width: <?php porcentagem(6,$cpf);?>"></div></div>
									<?php echo '<img class="icon_evolucao" src="'.$nivel.'imagens/info.png" title="Evolução" alt="Evolução">'; ?>
								</div>
							</div>
							<div id="bloco_direito">
																
								<?php echo '<img src="' .$nivel. 'imagens/apresentacao/insuficiencia_cardiaca.png" alt="Insuficiência Cadíaca" />';?>								

								 
								
							</div>
						</div>

			</div>
			

			<!--Caso 7-->
			<div id="caso7">
				
					
					
						<div id="personagem">
							<div id="bloco_esquerdo">
								<div id="corpo_texto">
									<p id="titulo_texto">Abordagem ao paciente com hanseníase no domicílio 
</p>
									<!-- <p id="descricao_texto">Descricâo do caso clínico</p> -->
									<br>
									<p id="titulo_objetivo">Objetivos</p>
									<p id="texto_objetivo">

										<ul>
											Aqui, abordaremos como conduzir o paciente portador de hanseníase no domicílio. Serão 
tratados os seguintes aspectos: <br>
										<li> conceito e epidemiologia da hanseníase; </li>
										<li> diagnóstico e/ou avaliação da hanseníase; </li>
										<li> manejo dos pacientes portadores de hanseníase;</li> 
										<li> orientações ao cuidador e plano de alta; </li>
										<li> reconhecimento de quando a equipe deve referenciar o caso para uma unidade de 
										saúde.</li>

										</ul>


									</p>
								
								</div>	
								
						<?php echo '<a href="'.$nivel.'unidades/unidade3/hanseniase/index.php?uni=3&caso=7&topico=0&resource=143">'; ?>
						
								<button class="btn btn-inverse" id="acesso">Acesso ao conteúdo</button>
								</a><br><br>
								<div id="barra">
						
									<p class="porc"><?php porcentagem(7,$cpf);?></p><div id="barra_porc" >
									<div class="barra_porc_interna" style="width: <?php porcentagem(7,$cpf);?>"></div></div>
									<?php echo '<img class="icon_evolucao" src="'.$nivel.'imagens/info.png" title="Evolução" alt="Evolução">'; ?>
								</div>
							</div>
							<div id="bloco_direito">
								
								<?php echo '<img src="' .$nivel. 'imagens/apresentacao/hanseniase.png" alt="Hanseníase"  />';?>
								 
								
							</div>
						</div>
					
					
					
					
			</div>
			
			<!--Caso 8-->
			<div id="caso8">
					
					<div id="personagem">
							<div id="bloco_esquerdo">
								<div id="corpo_texto">
									<p id="titulo_texto">Abordagem ao paciente com mal&aacute;ria</p>
									<!-- <p id="descricao_texto">Descricâo do caso clínico</p> -->
									<br>
									<p id="titulo_objetivo">Objetivos</p>
									<p id="texto_objetivo">

											Aqui, abordaremos como conduzir o paciente portador de malária, inclusive no domicílio. Serão tratados 
os seguintes aspectos: 
<ul><li>conceito e epidemiologia da malária; </li>
<li>diagnóstico e/ou avaliação da malária; </li>
<li>manejo dos pacientes portadores de malária;</li> 
<li>orientações ao cuidador e plano de alta; </li>
<li>reconhecimento de quando a equipe deve referenciar o caso para uma unidade de saúde. </li>
</ul>




									</p>
								
								</div>	
								
						<?php echo '<a href="'.$nivel.'unidades/unidade3/malaria/index.php?uni=3&caso=8&topico=0&resource=160">'; ?>
								<button class="btn btn-inverse" id="acesso">Acesso ao conteúdo</button>
								</a><br><br>
								<div id="barra">
						
									<p class="porc"><?php porcentagem(8,$cpf);?></p><div id="barra_porc" >
									<div class="barra_porc_interna" style="width: <?php porcentagem(8,$cpf);?>"></div></div>
									<?php echo '<img class="icon_evolucao" src="'.$nivel.'imagens/info.png" title="Evolução" alt="Evolução">'; ?>
								</div>
							</div>
							<div id="bloco_direito">
								
								<?php echo '<img src="' .$nivel. 'imagens/apresentacao/malaria.png" alt="Malária" />';?>
								 
								
							</div>
					</div>
					
			</div>
			
			<!--Caso 9-->
			<div id="caso9">
					
					
					<div id="personagem">
							<div id="bloco_esquerdo">
								<div id="corpo_texto">
									<p id="titulo_texto">Abordagem domiciliar ao paciente com Síndrome da Imunodeficiência Adquirida (SIDA)</p>
									<!-- <p id="descricao_texto">Descricâo do caso clínico</p> -->
									<br>
									<p id="titulo_objetivo">Objetivos</p>
									<p id="texto_objetivo">

										Aqui, abordaremos como conduzir o paciente portador de SIDA para que permaneça no 
domicílio clinicamente estável e confortável. Serão tratados os seguintes aspectos: 
<ul>
	<li>conceito e epidemiologia da SIDA; </li>
<li>diagnóstico e/ou avaliação da SIDA; </li>
<li>manejo dos pacientes portadores de SIDA;</li> 
<li>orientações ao cuidador e plano de alta; </li>
<li>reconhecimento de quando a equipe deve referenciar o caso para uma unidade de 
saúde.</li> </ul>



									</p>
								
								</div>	
								<?php echo '<a href="'.$nivel.'unidades/unidade3/sida/index.php?uni=3&caso=9&topico=0&resource=151">'; ?>
								<button class="btn btn-inverse" id="acesso">Acesso ao conteúdo</button>
								</a><br><br>
								<div id="barra">
						
									<p class="porc"><?php porcentagem(9,$cpf);?></p><div id="barra_porc" >
									<div class="barra_porc_interna" style="width: <?php porcentagem(9,$cpf);?>"></div></div>
									<?php echo '<img class="icon_evolucao" src="'.$nivel.'imagens/info.png" title="Evolução" alt="Evolução">'; ?>
								</div>

							</div>
							<div id="bloco_direito">
								
								<?php echo '<img src="' .$nivel. 'imagens/apresentacao/sida.png" alt="SIDA" />';?>
							
								
							</div>
					</div>
					
			</div>
			
			<!--Caso 10-->
			<div id="caso10">
					
					<div id="personagem">
							<div id="bloco_esquerdo">
								<div id="corpo_texto">
									<p id="titulo_texto">Abordagem ao paciente com tuberculose no domicílio</p>
									<!-- <p id="descricao_texto">Descricâo do caso clínico</p> -->
									<br>
									<p id="titulo_objetivo">Objetivos</p>
									<p id="texto_objetivo">
										<p>Aqui, abordaremos como conduzir o paciente portador de tuberculose, inclusive no domicílio. Serão tratados os seguintes aspectos:</p><p></p><ul><li>conceito e epidemiologia da tuberculose;</li><li>diagnóstico e/ou avaliação da tuberculose;</li><li>manejo dos pacientes portadores de tuberculose;</li><li>orientações ao cuidador e plano de alta;</li><li>reconhecimento de quando a equipe deve referenciar o caso para uma unidade de saúde.</li></ul><p></p>
										
										
									</p>
								
								</div>	
								
							<?php echo '<a href="'.$nivel.'unidades/unidade3/tuberculose/index.php?uni=3&caso=10&topico=0&resource=167">'; ?>
								<button class="btn btn-inverse" id="acesso">Acesso ao conteúdo</button>
								</a><br><br>
								<div id="barra">
						
									<p class="porc"><?php porcentagem(10,$cpf);?></p><div id="barra_porc" >
									<div class="barra_porc_interna" style="width: <?php porcentagem(10,$cpf);?>"></div></div>
									<?php echo '<img class="icon_evolucao" src="'.$nivel.'imagens/info.png" title="Evolução" alt="Evolução">'; ?>
								</div>
							</div>
							<div id="bloco_direito">
								
								<?php echo '<img src="' .$nivel. 'imagens/apresentacao/tuberculose.png" alt="Tuberculose" />';?>
								 
								
							</div>
					</div>
			</div>

			<!--Caso 11-->
			<div id="caso11" >
						
						<div id="personagem">
							<div id="bloco_esquerdo">
								<div id="corpo_texto">
									<p id="titulo_texto">Abordagem ao paciente com infec&ccedil;&atilde;o do trato urin&aacute;rio no domicílio</p>
									<!-- <p id="descricao_texto">Descricâo do caso clínico</p> -->
									<br>
									<p id="titulo_objetivo">Objetivos</p>
									<p id="texto_objetivo">
										<p>Aqui, abordaremos como conduzir o paciente portador de infecção do trato urinário no domicílio. Serão apresentados os seguintes aspectos:</p><p>• conceito de infecção do trato urinário;<br />• sintomas e sinais que possam ser controlados no domicílio de pacientes com infecção do trato urinário;<br />• manejo dos pacientes com infecção do trato urinário;<br />• plano de alta e orientações ao cuidador sobre como prevenir e identificar sinais e sintomas de infecção do trato urinário;<br />• reconhecimento de quando a equipe deve referenciar o caso para a rede de serviços de saúde.</p>

									</p>
								
								</div>	
								
					<?php echo '<a href="'.$nivel.'unidades/unidade4/itu/index.php?uni=4&caso=11&topico=0&resource=100">'; ?>
								<button class="btn btn-inverse" id="acesso">Acesso ao conteúdo</button>
								</a><br><br>
								<div id="barra">
							
									<p class="porc"><?php porcentagem(11,$cpf);?></p><div id="barra_porc" >
									<div class="barra_porc_interna" style="width: <?php porcentagem(11,$cpf);?>"></div></div>
									<?php echo '<img class="icon_evolucao" src="'.$nivel.'imagens/info.png" title="Evolução" alt="Evolução">'; ?>
								</div>
							</div>
							<div id="bloco_direito">
								
							<?php echo '<img src="' .$nivel. 'imagens/apresentacao/infeccao_trato_urinario.png" alt="Infecção Trato Urinário" />';?>	
							 
								
							</div>
					</div>
					
			</div>

			<!--Caso 12-->
			
			<div id="caso12">
						
						<div id="personagem">
							<div id="bloco_esquerdo">
								<div id="corpo_texto">
									<p id="titulo_texto">Abordagem ao paciente pós-operatório de problemas ortopédicos no domicílio</p>
									<!-- <p id="descricao_texto">Descricâo do caso clínico</p> -->
									<br>
									<p id="titulo_objetivo">Objetivos</p>
									<p id="texto_objetivo">

										 Aqui, abordaremos como conduzir um paciente em pós-operatório de problemas ortopédicos para que permaneça no domicílio clinicamente estável e confortável. Serão tratados os seguintes aspectos:
										 <ul>
										 	<li>as condições clínicas no pós-operatório de problemas ortopédicos e traumatológicos e os dados epidemiológicos que envolvem as internações por estes fins;</li>
										 	<li>a avaliação da situação clínica de problemas ortopédicos e traumatológicos incapacitantes;</li>
										 	<li>o reconhecimento de quais pacientes podem se beneficiar da Atenção Domiciliar para reduzir o tempo de internação;</li>
										 	<li>o manejo dos pacientes com problemas ortopédicos e traumatológicos incapacitantes temporários ou permanentes, orientações ao cuidador e plano de alta;</li>
										 	<li>o reconhecimento de quando a equipe deve referenciar o caso para a rede de serviços de saúde.</li>
										 </ul>


									</p>
								
								</div>	
								
						<?php echo '<a href="'.$nivel.'unidades/unidade5/pos_ortopedia/index.php?uni=5&caso=12&topico=0&resource=43">'; ?>
								<button class="btn btn-inverse" id="acesso">Acesso ao conteúdo</button>
								</a><br><br>
								<div id="barra">
							
								<p class="porc"><?php porcentagem(12,$cpf);?></p><div id="barra_porc" >
								<div class="barra_porc_interna" style="width: <?php porcentagem(12,$cpf);?>"></div></div>
								<?php echo '<img class="icon_evolucao" src="'.$nivel.'imagens/info.png" title="Evolução" alt="Evolução">'; ?>
							</div>
							</div>
							<div id="bloco_direito">
							
							<?php echo '<img src="' .$nivel. 'imagens/apresentacao/pos_operatorio_ortopedia.png" alt="Pós operatório" />';?>		
							 
								
							</div>
					</div>
						
						
			</div>
			
			
			<!--Caso 13-->
			<div id="caso13" >
						
						<div id="personagem">
							<div id="bloco_esquerdo">
								<div id="corpo_texto">
									<p id="titulo_texto">Abordagem ao paciente com osteomielite no domicílio</p>
									<!-- <p id="descricao_texto">Descricâo do caso clínico</p> -->
									<br>
									<p id="titulo_objetivo">Objetivos</p>
									<p id="texto_objetivo">

										Esta unidade abordará como conduzir o paciente portador de osteomielite crônica para que 
permaneça no domicílio clinicamente estável e confortável. Serão tratados os seguintes 
aspectos: <br>
							<ul>
								<li>as condições clínicas em quadros de osteomielite oriundos dos problemas ortopédicos 
								e/ou traumatológicos; </li>
								<li>os os dados epidemiológicos que envolvem as internações por este fim; </li>
								<li>a avaliação da situação clínica mediante quadro de osteomielite; </li>
								<li>identificação de quais pacientes podem se beneficiar da atenção domiciliar, seja para 
								evitar hospitalização ou para reduzir o tempo de internação; </li>
								<li>o manejo dos pacientes com osteomielite, orientações ao cuidador e plano de alta; </li>
								<li>reconhecimento de quando a equipe deve referenciar o caso para a rede de serviços 
								de saúde. </li>
							</ul>


									</p>
								
								</div>	
								
						<?php echo '<a href="'.$nivel.'unidades/unidade5/osteomielite/index.php?uni=5&caso=13&topico=0&resource=34">'; ?>

								<button class="btn btn-inverse" id="acesso">Acesso ao conteúdo</button>
								</a><br><br>
								<div id="barra">
							
								<p class="porc"><?php porcentagem(13,$cpf);?></p><div id="barra_porc" >
								<div class="barra_porc_interna" style="width: <?php porcentagem(13,$cpf);?>"></div></div>
								<?php echo '<img class="icon_evolucao" src="'.$nivel.'imagens/info.png" title="Evolução" alt="Evolução">'; ?>
							</div>
							</div>
							<div id="bloco_direito">
							
							<?php echo '<img src="' .$nivel. 'imagens/apresentacao/osteomielite.png" alt="Osteomielite"/>';?>		
	
							 		

							</div>
					</div>

			</div>

			<!--Caso 14-->
			<div id="caso14" >
						
						
						<div id="personagem">
							<div id="bloco_esquerdo">
								<div id="corpo_texto">
									<p id="titulo_texto">Abordagem ao paciente com transtornos psiquiátricos no domicílio</p>
									<!-- <p id="descricao_texto">Descricâo do caso clínico</p> -->
									<br>
									<p id="titulo_objetivo">Objetivos</p>
									<p id="texto_objetivo">
										<p>Aqui, abordaremos como conduzir o paciente com transtornos psiquiátricos no domicílio para que permaneça clinicamente estável e confortável. Serão tratados os seguintes aspectos:</p><p>• tipos de transtornos psiquiátricos;<br />• sintomas e sinais de pacientes portadores de transtornos psiquiátricos incapacitantes que possam ser controlados no domicílio;<br />• manejo dos pacientes portadores de transtornos psiquiátricos incapacitantes, plano de alta e orientações ao cuidador sobre efeitos adversos de medicamentos e interações medicamentosas;<br />• reconhecimento de quando a equipe deve referenciar o caso para a rede de serviços de saúde.</p>
									</p>
								
								</div>	
							
						<?php echo '<a href="'.$nivel.'unidades/unidade6/trans_psiquiatricos/index.php?uni=6&caso=14&topico=0&resource=50">'; ?>
								<button class="btn btn-inverse" id="acesso">Acesso ao conteúdo</button>
								</a><br><br>
								<div id="barra">
								<p class="porc"><?php porcentagem(14,$cpf);?></p><div id="barra_porc" >
								<div class="barra_porc_interna" style="width: <?php porcentagem(14,$cpf);?>"></div></div>
								<?php echo '<img class="icon_evolucao" src="'.$nivel.'imagens/info.png" title="Evolução" alt="Evolução">'; ?>
							</div>
							</div>
							<div id="bloco_direito">
							
							<?php echo '<img src="' .$nivel. 'imagens/apresentacao/transtorno_psiq_incapacitante.png" alt="Transtorno Psiquiátrico" />';?>		
							 
								
							</div>
					</div>

			</div>

			<!--Caso 15-->
			<div id="caso15" >
					
						
						
						<div id="personagem">
							<div id="bloco_esquerdo">
								<div id="corpo_texto">
									<p id="titulo_texto">Abordagem ao paciente usuário de álcool e crack no domicílio</p>
									<!-- <p id="descricao_texto">Descricâo do caso clínico</p> -->
									<br>
									<p id="titulo_objetivo">Objetivos</p>
									<p id="texto_objetivo">
										<p>Aqui, abordaremos como conduzir o paciente usuário de álcool e outras drogas na Atenção Domiciliar (AD). Serão tratados os seguintes aspectos:</p><ul>  <li>sinais e sintomas da dependência de álcool e outras drogas;</li>  <li>manejo dos pacientes dependentes de álcool e outras drogas;</li>  <li>orientações ao cuidador quanto às crises de abstinência;</li>  <li>reconhecimento de quando a equipe deve referenciar o caso para a rede de serviços de saúde.</li></ul>


									</p>
								
								</div>	
							
						<?php echo '<a href="'.$nivel.'unidades/unidade7/alcool/index.php?uni=7&caso=15&topico=0&resource=97">'; ?>
								<button class="btn btn-inverse" id="acesso">Acesso ao conteúdo</button>
								</a><br><br>
								<div id="barra">
								<p class="porc"><?php porcentagem(15,$cpf);?></p><div id="barra_porc" >
								<div class="barra_porc_interna" style="width: <?php porcentagem(15,$cpf);?>"></div></div>
								<?php echo '<img class="icon_evolucao" src="'.$nivel.'imagens/info.png" title="Evolução" alt="Evolução">'; ?>
							</div>
							</div>
							<div id="bloco_direito">
								<div id="aux_modal">

									<?php echo '<img onclick="alterna(0);" id="drogas" src="' .$nivel. 'imagens/apresentacao/drogas.png" alt="Drogas"  />';?>	

									<?php echo '<img onclick="alterna(1);" id="alcoolismo" src="' .$nivel. 'imagens/apresentacao/alcoolismo.png" alt="Alcoolismo" />';?>

									<center><div class="check" style="position: absolute; bottom: 0; left: 100;"></div></center>

								</div>
							</div>
					</div>
			</div>

			
			<!--Caso 16 est? junto com o 15-->
			<!-- <div id="caso16" >
						
						
						<div id="personagem">
							<a href="../unidades/unidade7/drogas/index.php?uni=7&caso=16&topico=0&resource=1">
								<img src="../imagens/apresentacao/drogas.png" alt="Drogas" /></a>
							<div id="barra">
							
								<p class="porc"><?php //porcentagem(16,$cpf);?></p><div id="barra_porc" >
								<div class="barra_porc_interna" style="width: <?php //porcentagem(16,$cpf);?>"></div></div>
							</div>
						</div>

			</div> -->

			<!--Caso  17-->
			<div id="caso17" >
				
						
						<div id="personagem">
							<div id="bloco_esquerdo">
								<div id="corpo_texto">
									<p id="titulo_texto">Abordagem ao paciente com Esclerose Lateral Amiotrófica no domicílio</p>
									<!-- <p id="descricao_texto">Descricâo do caso clínico</p> -->
									<br>
									<p id="titulo_objetivo">Objetivos</p>
									<p id="texto_objetivo">

										Aqui, abordaremos como conduzir o paciente portador de Esclerose Lateral Amiotrófica para que permaneça no domicílio clinicamente estável e confortável. Serão tratados os seguintes aspectos:
<ul>
<li>conceito e epidemiologia da Esclerose Lateral Amiotrófica (ELA);</li>
<li>avaliação da ELA e os tipos de paciente que podem se beneficiar dos cuidados da assistência domiciliar;</li>
<li>manejo dos pacientes portadores de ELA;</li>
<li>orientações ao cuidador sobre a história natural (sinais e sintomas controláveis no domicílio) e plano de alta;</li>
<li>reconhecimento de quando a equipe deve referenciar o caso para a rede de serviços de saúde.</li>
</ul>


									</p>
								
								</div>	
							
						<?php echo '<a href="'.$nivel.'unidades/unidade8/esclerose_lateral/index.php?uni=8&caso=17&topico=0&resource=131">'; ?>
								<button class="btn btn-inverse" id="acesso">Acesso ao conteúdo</button>
								</a><br><br>
								<div id="barra">
							
								<p class="porc"><?php porcentagem(17,$cpf);?></p><div id="barra_porc" >
								<div class="barra_porc_interna" style="width: <?php porcentagem(17,$cpf);?>"></div></div>
								<?php echo '<img class="icon_evolucao" src="'.$nivel.'imagens/info.png" title="Evolução" alt="Evolução">'; ?>
							</div>
							</div>
							<div id="bloco_direito">
							
							<?php echo '<img src="' .$nivel. 'imagens/apresentacao/ela.png" alt="ELA" />';?>		
							 
								
							</div>
					</div>
						
						
			</div>
			
			<div id="caso18">
						
						<div id="personagem">
							<div id="bloco_esquerdo">
								<div id="corpo_texto">
									<p id="titulo_texto">Abordagem ao paciente com Esclerose Múltipla</p>
									<!-- <p id="descricao_texto">Descricâo do caso clínico</p> -->
									<br>
									<p id="titulo_objetivo">Objetivos</p>
									<p id="texto_objetivo">
										Aqui, abordaremos a assistência domiciliar ao paciente portador de Esclerose Múltipla. Serão apresentados os seguintes aspectos:
<ul>
<li>Conceito e epidemiologia da Esclerose Múltipla (EM);</li>
<li>Avaliação da EM e os tipos de paciente que podem se beneficiar dos cuidados da assistência domiciliar;</li>
<li>Manejo dos pacientes portadores de EM;</li>
<li>Orientações ao cuidador sobre a história natural (sinais e sintomas controláveis no domicílio) e plano de alta;</li>
<li>Reconhecimento de quando a equipe deve referenciar o caso para a rede de serviços de saúde.</li>
</ul>


									</p>
								
								</div>	
							
						<?php echo '<a href="'.$nivel.'unidades/unidade8/esclerose_multipla/index.php?uni=8&caso=18&topico=0&resource=122">'; ?>
								<button class="btn btn-inverse" id="acesso">Acesso ao conteúdo</button>
								</a><br><br>
								<div id="barra">
							
								<p class="porc"><?php porcentagem(18,$cpf);?></p><div id="barra_porc" >
								<div class="barra_porc_interna" style="width: <?php porcentagem(18,$cpf);?>"></div></div>
								<?php echo '<img class="icon_evolucao" src="'.$nivel.'imagens/info.png" title="Evolução" alt="Evolução">'; ?>
							</div>
							</div>
							<div id="bloco_direito">
							
							<?php echo '<img src="' .$nivel. 'imagens/apresentacao/em.png" alt="EM"  />';?>		
							 
								
							</div>
					</div>
						
			</div>

		</div>


		<script type="text/javascript">

			$( "#check_modal" ).change(function() {
			 
			 	if($("#check_modal").is(":checked") == true){
			 		 localStorage.setItem('modal_block', 'on');
			 	}else{
			 		 localStorage.setItem('modal_block', 'off');
			 	}



			});


		</script>


		<style type="text/css">
				#aux_modal{
					position: relative;
					width: 383px;
					height: 472px;
				}

				#alcoolismo{
					position: absolute;
					left: 46;
					top: 8;
					cursor: pointer;
					width: 84% !important;
				}

				
				#drogas{
					position: absolute;
					left: -18;
					top: -12;
					cursor: pointer;
					width: 84% !important;
				}

				#topo_modal{
					height: 30px;
					padding: 10px;
					padding-left: 20px;
					line-height: 25px;
					font-size: 20pt;
					font-weight: bold;
				}

				.icon_evolucao{
					position: absolute;
					bottom: 51;
					right: 237;
				}

			</style>
			
