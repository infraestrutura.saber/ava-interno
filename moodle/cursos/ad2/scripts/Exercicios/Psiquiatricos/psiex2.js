var alternativas = new Array();
alternativas[0] = "Deve guardar os medicamentos em local de difícil acesso para o paciente devido ao risco de tentativa de suicídio através da tomada de superdosagem de medicamentos.";
alternativas[1] = "Garantir que o paciente esteja tomando os medicamentos nas doses e horários corretos. Deve ensiná-lo a tomar os medicamentos pelas cores.";
alternativas[2] = "Estudar e passar para a equipe, não para o cuidador, os principais sintomas que possam significar alguma reação de abstinência do fármaco, a Síndrome Serotoninérgica ou a Síndrome Neuroléptica Maligna.";
alternativas[3] = "A Síndrome Serotoninérgica pode ser conduzida no domicílio.";
var name = "psiex2";
gerarinputmultipla(alternativas, name, 0, 50);

