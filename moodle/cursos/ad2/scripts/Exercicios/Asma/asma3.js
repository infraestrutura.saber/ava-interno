/*Alternativas do verdadeiro/falso*/
	var opcao1 = 'Em se tratando de adolescentes com asma, a adequada orientação aos cuidadores objetiva, entre outros fatores, reduzir falta à escola e idas desnecessárias às emergências.';
	var opcao2 = 'Asma de difícil controle é uma condição que indica avaliar a necessidade de referenciar o paciente para a rede especializada.';
	var opcao3 = 'Um plano educativo escrito deve ser elaborado no momento da alta da pessoa com asma do SAD para acompanhamento na USF.';
	var opcao4 = 'Não há indicação de confirmação diagnóstica da asma em serviços especializados, mesmo que haja solicitação dos pais da criança.';
	/*array de variaveis que vai para a funcao geradora*/
	var entrada = [opcao1, opcao2, opcao3, opcao4];
	/*variavel com o name do formulario. Deve ser único na mesma página*/
	var name = 'asmaex4';
	/*gabarito da questao.*/
	var gabarito = '(\'V\', \'V\', \'V\', \'F\')';
	/*funcao que gera o html*/
	gerarInputVF(entrada, name, gabarito, 'asmaex4',3); 
