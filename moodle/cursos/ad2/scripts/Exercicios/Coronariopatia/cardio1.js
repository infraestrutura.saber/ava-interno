var alternativas = new Array();
alternativas[0] = "Angina típica definitiva: desconforto subesternal, provocado por estresse emocional ou por esforço, há alívio em repouso ou com o uso de nitrato sublingual.";
alternativas[1] = "Angina típica provável: reúne três das características da angina típica definitiva.";
alternativas[2] = "Dor torácica não cardíaca: reúne duas das características da angina típica.";
alternativas[3] = "A dor do infarto agudo do miocárdio piora com o nitrato sublingual.";
var name = "cardio1";
gerarinputmultipla(alternativas, name, 0, 15);