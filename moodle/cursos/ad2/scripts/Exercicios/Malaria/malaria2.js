var alternativas = new Array();
alternativas[0] = "O diagnóstico de certeza dos casos de malária é eminentemente clínico, a tríade cefaleia, calafrios e febre fecha o diagnóstico de malária.";
alternativas[1] = "Gota espessa é o teste diagnóstico utilizado no Brasil para confirmação diagnóstica de Malária, por ser simples, rápido e de baixo custo.";
alternativas[2] = "O diagnóstico de malária só é possível pela demonstração do parasito ou antígenos relacionados no sangue periférico.";
alternativas[3] = "O exame de gota espessa é expresso em cruzes que se relacionam com a densidade parasitária no sangue periférico.";
var name = "malariaex2";
gerarinputmultipla(alternativas, name, 0, 30);