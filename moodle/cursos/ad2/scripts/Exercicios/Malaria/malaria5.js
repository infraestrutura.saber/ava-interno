/*Alternativas do verdadeiro/falso*/
	var opcao1 = 'A presença de sinais de alarme indica a necessidade de se referenciar o paciente para unidade hospitalar.';
	var opcao2 = 'São considerados sinais de alarme: hipotensão arterial, oligúria, convulsões, icterícia.';
	var opcao3 = 'O diagnóstico não deve ser postergado; o exame de gota espessa ou imunocromatográfico devem sempre ser solicitados na suspeita de malária.';
	var opcao4 = 'Suspeitar de infecção mista quando na presença de parasitemia de +++ ou ++++ e resultado positivo para <i>P.vivax</i>.';
	/*array de variaveis que vai para a funcao geradora*/
	var entrada = [opcao1, opcao2, opcao3, opcao4];
	/*variavel com o name do formulario. Deve ser único na mesma página*/
	var name = 'malariaex5';
	/*gabarito da questao.*/
	var gabarito = '(\'V\', \'V\', \'V\', \'V\')';
	/*funcao que gera o html*/
	gerarInputVF(entrada, name, gabarito, 'malariaex5',64); 
