/*Alternativas do verdadeiro/falso*/        			
var opcao1 = '...mulheres grávidas no segundo e no terceiro trimestre são mais susceptíveis aos quadros graves de malária, causados pelo <i>P.falciparum</i>.<br />';
var opcao2 = '...podem ocorrer complicações como abortamento espontâneo, baixo peso ao nascer e pré-maturidade.<br />';        			
var opcao3 = '...a gestante deverá ser tratada com drogas específicas.<br />';        			
var opcao4 = '...o recém-nascido precisa ser avaliado devido ao risco de malária congênita.</p>';        			/*array de variaveis que vai para a funcao geradora*/        			
var entrada = [opcao1, opcao2, opcao3, opcao4];        			/*variavel com o name do formulario. Deve ser único na mesma página*/        			
var name = 'malariaex4';        			
/*gabarito da questao.*/        			
var gabarito = '(\'V\', \'V\', \'V\', \'V\')';        			
/*funcao que gera o html*/        			
gerarInputVF(entrada, name, gabarito, 'malariaex4', 32);          		