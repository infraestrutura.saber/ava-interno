var alternativas = new Array();
alternativas[0] = "A utilização dos fármacos visa interromper a transmissão da forma assexuada do parasita.";
alternativas[1] = "Após o fim do esquema terapêutico, lâmina de verificação de cura deverá ser realizada.";
alternativas[2] = "Pacientes que não respondem aos esquemas habituais para tratamento de malária devem ser referenciados para outros níveis de atenção.";
alternativas[3] = "Gestantes infectadas pelo <i>P. vivax</i> devem ser tratadas com Cloroquina.";
var name = "malariaex3";
gerarinputmultipla(alternativas, name, 0, 31);