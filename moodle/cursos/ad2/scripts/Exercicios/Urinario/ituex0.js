
/*Alternativas do verdadeiro/falso*/
	var opcao1 = 'Em mais de 95% dos casos de ITU, o agente causador da infecção é único.';
	var opcao2 = 'A infecção urinária dificilmente ocorre em pacientes hospitalizados.';
	var opcao3 = 'A infecção do trato urinário, na vida adulta, tem incidência elevada e predomina no sexo feminino.';
	var opcao4 = 'No homem, a uretra mais curta e o fator antibacteriano prostático são fatores protetores contra a infecção urinária. Na mulher, o maior comprimento uretral, o maior fluxo urinário e a maior proximidade do ânus com o vestíbulo da vagina e uretra facilitam a infecção.';
	/*array de variaveis que vai para a funcao geradora*/
	var entrada = [opcao1, opcao2, opcao3, opcao4];
	/*variavel com o name do formulario. Deve ser único na mesma página*/
	var name = 'ituex0';
	/*gabarito da questao.*/
	var gabarito = '(\'V\', \'F\', \'V\', \'F\')';
	/*funcao que gera o html*/
	gerarInputVF(entrada, name, gabarito, 'ituex0', 41); 
