var alternativas = new Array();
alternativas[0] = "A dieta deve ser leve, desde que tolerada, com atenção especial à hidratação.";
alternativas[1] = "Nas refeições, propiciar ambiente calmo, confortável e com bastante estimulação audiovisual.";
alternativas[2] = "Durante o período agudo de abstinência, as visitas e a circulação do paciente em ambientes externos ao domiciliar devem ser estimuladas. O contato com outras pessoas pode trazer estímulo positivo à melhor aceitação da dieta.";
alternativas[3] = "Orientar que a família não se preocupe caso a paciente apresente recusa alimentar em várias refeições.";
var name = "alcoolex2";
gerarinputmultipla(alternativas, name, 0, 52);
