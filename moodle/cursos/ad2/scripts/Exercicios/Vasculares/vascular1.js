/*Alternativas do verdadeiro/falso*/
var opcao1 = 'A úlcera venosa é consequência do aumento da pressão venosa devido à insuficiência venosa crônica.';
var opcao2 = 'Na úlcera neuropática diabética, o pulso torna-se imperceptível, e pacientes insulinodependentes são imunes a esse tipo de lesão.';
var opcao3 = 'As características da tromboangeíte obliterante são: frequentemente apresenta úlceras no pé, claudicação é um sintoma raro; começo de sintomas normalmente antes dos 40 anos; sempre associado com o tabagismo e normalmente em homens jovens.';
var opcao4 = 'A dor da neuropatia isquêmica é intensa, difusa e espasmódica. A dor é referida pelo doente como um puxão, rasgando, queimação, entorpecimento ou parestesia do membro.';
/*array de variaveis que vai para a funcao geradora*/
var entrada = [opcao1, opcao2, opcao3, opcao4];
/*variavel com o name do formulario. Deve ser único na mesma página*/
var name = 'vascularesex1';
/*gabarito da questao.*/
var gabarito = '(\'V\', \'F\', \'V\', \'V\')';
/*funcao que gera o html*/	
gerarInputVF(entrada, name, gabarito, 'vascularesex1', 17);