
var opcao1 = 'Os principais sinais e sintomas da DPOC são: tosse, chiado no peito, falta de ar e expectoração.';
var opcao2 = 'A tosse é o sintoma mais encontrado, podendo aparecer antes ou associada à dispneia, de forma intermitente ou diariamente.';
var opcao3 = 'Existe correlação entre a dependência da nicotina e o consumo tabágico, e existem técnicas que podem ser ofertadas para apoio à cessação do tabagismo.';
var opcao4 = 'O estadiamento da DPOC é fundamental para a definição do tratamento a ser instituído.';
/*array de variaveis que vai para a funcao geradora*/
var entrada = [opcao1, opcao2, opcao3, opcao4];/*variavel com o name do formulario. Deve ser único na mesma página*/
var name = 'dpocex3';/*gabarito da questao.*/
var gabarito = '(\'V\', \'V\', \'V\', \'V\')';/*funcao que gera o html*/
gerarInputVF(entrada, name, gabarito, 'dpocex3',7);


// <div id="dpocex3"></div>
// <div id="boxdpocex3" class="box"></div><div class="feedback feedbackdpocex3">Feedback<br /><br />

// a) Verdadeiro. Os principais sinais e sintomas da DPOC são: tosse,	falta de ar, chiado no peito e expectoração crônica. Embora a DPOC comprometa	os pulmões, ela também causa alterações sistêmicas significativas.<br /><br>
// b) Verdadeiro. A tosse é o sintoma mais encontrado, podendo aparecer	antes ou associado à dispneia, de forma intermitente ou diariamente. A tosse no	fumante é tão frequente que pode ser atribuído ao cigarro um pigarro, não sendo percebida pelos pacientes como um sintoma. 50% dos fumantes apresentam	tosse produtiva.<br /><br>
// c) Verdadeiro. O conhecimento de que a dependência da nicotina tem	participação no consumo tabágico tem respaldado as equipes a utilizarem	técnicas individuais e coletivas, como aconselhamento sobre saúde, educação e métodos cognitivos-comportamentais para apoio aos pacientes, visando à cessação	do tabagismo.<br /><br>
// d) Verdadeiro. O estadiamento da DPOC é fundamental para a definição do tratamento clínico a ser	instituído. O estadiamento mais utilizado mundialmente se baseia nos valores	espirométricos para a classificação dos pacientes com DPOC, classificados como	apresentando doença leve, moderada, grave e muito grave.

// </div><!--fim exercicio-->
