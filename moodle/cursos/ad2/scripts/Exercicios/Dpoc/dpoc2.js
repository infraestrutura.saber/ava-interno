/*Alternativas do verdadeiro/falso*/
	var opcao1 = 'A Doença Pulmonar Obstrutiva Crônica (DPOC) caracteriza-se pela presença de obstrução crônica totalmente reversível ao fluxo aéreo, que se dá por uma resposta inflamatória do pulmão.';
	var opcao2 = 'Devido ao aumento do tabagismo nos países em desenvolvimento e ao envelhecimento da população, a taxa de mortalidade pela DPOC vem aumentando.';
	var opcao3 = 'A presença de sintomas crônicos respiratórios, como tosse, dispneia e sibilos em pacientes com a história de tabagismo, deve levar à suspeita clínica de DPOC.';
	var opcao4 = 'O principal diagnóstico diferencial da DPOC deve se dá com a asma, e a presença de boa resposta ao corticoide inalatório fala a favor do diagnóstico de DPOC.';
	/*array de variaveis que vai para a funcao geradora*/
	var entrada = [opcao1, opcao2, opcao3, opcao4];
	/*variavel com o name do formulario. Deve ser único na mesma página*/
	var name = 'dpocex2';
	/*gabarito da questao.*/
	var gabarito = '(\'F\', \'V\', \'V\', \'F\')';
	/*funcao que gera o html*/
	gerarInputVF(entrada, name, gabarito, 'dpocex2',6); 
