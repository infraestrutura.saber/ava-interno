var alternativas = new Array();alternativas[0] = "Radiografia de Tórax";alternativas[1] = "Espirometria";alternativas[2] = "Gasometria arterial";alternativas[3] = "Oximetria de pulso";var name = "dpocex4";gerarinputmultipla(alternativas, name, 1);



// <script type='text/javascript' src='../../lib/exercicios/dpoc/dpoc4.js'></script>

// <script type='text/javascript' src='../../../scripts/Exercicios/Dpoc/dpoc4.js'></script>


// <div id="boxdpocex4" class="box"></div><div class="feedback feedbackdpocex4">	
// Feedback
// 	<br />	<br />	<div class="feed">		a) Essa	 não é a alternativa adequada. Na DPOC, deve-se solicitar rotineiramente uma	 radiografia de tórax nas posições póstero-anterior e perfil, não para definição	 da doença, mas para afastar outras doenças pulmonares, principalmente neoplasias. Na suspeita clínica de DPOC, a Espirometria é mandatória, devendo	 ser realizada antes e após administração de broncodilatador, de preferência em	 fase estável da doença.
// 	</div>	<div class="feed">		b) Parabéns,	 você acertou. Na suspeita clínica de DPOC, a Espirometria é muito importante,	 devendo ser realizada antes e após administração de broncodilatador, de preferência	 em fase estável da doença. Os parâmetros mais importantes do ponto de vista	 clínico são: CVF (Capacidade Vital Forçada), VEF1 (Volume Expiratório Forçado	 no Primeiro Segundo), relação VEF1/CVF. VEF1/CVF abaixo de 0,7 após broncodilatador	 define limitação ao fluxo aéreo.
// 	</div>	<div class="feed">		c) Essa	 não é a alternativa adequada. Gasometria arterial pode ser realizada, mas não	 para definição da doença. É utilizada para se avaliar pao2 e paco2, quando a	 saturação periférica de o2 está inferior a 90%. Na suspeita clínica de DPOC, a	 Espirometria é mandatória, devendo ser realizada antes e após administração de	 broncodilatador, de preferência em fase estável da doença.
// 	</div>	<div class="feed">		d) Essa	 não é a alternativa adequada. A oximetria de pulso deve ser realizada, sempre	 que houve exacerbação da DPOC, não para definição da doença. Na suspeita	 clínica de DPOC, a Espirometria é mandatória, devendo ser realizada antes e após administração de broncodilatador, de preferência em fase estável da	 doença.
// 	</div></div>




// <div id="boxdpocex4" class="box"></div><div class="feedback feedbackdpocex4">	Feedback
// 	<br />	<br />	<div class="feed">		a) Essa	 não é a alternativa adequada. Na DPOC, deve-se solicitar rotineiramente uma	 radiografia de tórax nas posições póstero-anterior e perfil, não para definição	 da doença, mas para afastar outras doenças pulmonares, principalmente neoplasias. Na suspeita clínica de DPOC, a Espirometria é mandatória, devendo	 ser realizada antes e após administração de broncodilatador, de preferência em	 fase estável da doença.
// 	</div>	<div class="feed">		b) Parabéns,	 você acertou. Na suspeita clínica de DPOC, a Espirometria é muito importante,	 devendo ser realizada antes e após administração de broncodilatador, de preferência	 em fase estável da doença. Os parâmetros mais importantes do ponto de vista	 clínico são: CVF (Capacidade Vital Forçada), VEF1 (Volume Expiratório Forçado	 no Primeiro Segundo), relação VEF1/CVF. VEF1/CVF abaixo de 0,7 após broncodilatador	 define limitação ao fluxo aéreo.
// 	</div>	<div class="feed">		c) Essa	 não é a alternativa adequada. Gasometria arterial pode ser realizada, mas não	 para definição da doença. É utilizada para se avaliar pao2 e paco2, quando a	 saturação periférica de o2 está inferior a 90%. Na suspeita clínica de DPOC, a	 Espirometria é mandatória, devendo ser realizada antes e após administração de	 broncodilatador, de preferência em fase estável da doença.
// 	</div>	<div class="feed">		d) Essa	 não é a alternativa adequada. A oximetria de pulso deve ser realizada, sempre	 que houve exacerbação da DPOC, não para definição da doença. Na suspeita	 clínica de DPOC, a Espirometria é mandatória, devendo ser realizada antes e após administração de broncodilatador, de preferência em fase estável da	 doença.
// 	</div></div>		
// </div></div></div>



// <script type="text/javascript" src="../../lib/exercicios/dpoc/dpoc4.js"></script><script type="text/javascript" src="../../../scripts/Exercicios/Dpoc/dpoc4.js"></script><!--Feedback--></p></div></div></div>