/*Alternativas do verdadeiro/falso*/
	var opcao1 = 'A hanseníase é uma doença infecciosa, causada pelo Mycobacterium leprae, sendo as pessoas com as formas paucibacilares importante fonte de transmissão da doença.';
	var opcao2 = 'A transmissão da hanseníase se dá pelo contato de indivíduos saudáveis com a pele dos indivíduos com as formas paucibacilares da doença.';
	var opcao3 = 'Houve uma queda no número de casos novos de hanseníase no mundo, sendo que, no Brasil, as regiões Norte, Nordeste e Centro-Oeste são responsáveis pelo maior número de casos novos.';
	var opcao4 = 'A hanseníase apresenta longo período de incubação, em média, 2 a 7 anos, sendo que a conversão de infecção em doença depende de fatores do ambiente, individuais e do próprio M. leprae.';
	/*array de variaveis que vai para a funcao geradora*/
	var entrada = [opcao1, opcao2, opcao3, opcao4];
	/*variavel com o name do formulario. Deve ser único na mesma página*/
	var name = 'hans9';
	/*gabarito da questao.*/
	var gabarito = '(\'F\', \'F\', \'V\', \'V\')';
	/*funcao que gera o html*/
	gerarInputVF(entrada, name, gabarito, 'hans9',22); 
