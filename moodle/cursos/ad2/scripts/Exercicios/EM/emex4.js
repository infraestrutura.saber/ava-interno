
	var opcao1 = 'Entre as drogas modificadoras da doença, encontram-se os Interferon-β e acetato de Glatiramer, que devem ser iniciados precocemente.';
	var opcao2 = 'Dor, fraqueza, entorpecimento, disfunção vesical e constipação são sintomas comuns na Esclerose Múltipla.';
	var opcao3 = 'Gabapentina, Baclofeno e os benzodiazepínicos não são drogas eficazes no manejo da espasticidade.';
	var opcao4 = 'Nos casos em que há sintomas de disfunção sexual, a abordagem individualizada que inclua aconselhamento e avaliação dos efeitos adversos da medicação é mandatória.';
	/*array de variaveis que vai para a funcao geradora*/
	var entrada = [opcao1, opcao2, opcao3, opcao4];
	/*variavel com o name do formulario. Deve ser único na mesma página*/
	var name = 'emex4';
	/*gabarito da questao.*/
	var gabarito = '(\'V\', \'V\', \'F\', \'V\')';
	/*funcao que gera o html*/
	gerarInputVF(entrada, name, gabarito, 'emex4',60); 

