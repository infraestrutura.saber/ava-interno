/*Alternativas do verdadeiro/falso*/
	var opcao1 = 'Amantadina está entre as drogas mais utilizadas para o tratamento da fadiga.';
	var opcao2 = 'Não há evidência de que a fadiga pode ser secundária a casos de depressão; logo antidepressivos não devem ser utilizados.';
	var opcao3 = 'Fisioterapia não é uma opção de tratamento, pois poderá agravar o quadro.';
	var opcao4 = 'O arsenal terapêutico para manejo da fadiga é bastante restrito ao uso de medicações.';
	/*array de variaveis que vai para a funcao geradora*/
	var entrada = [opcao1, opcao2, opcao3, opcao4];
	/*variavel com o name do formulario. Deve ser único na mesma página*/
	var name = 'emex2';
	/*gabarito da questao.*/
	var gabarito = '(\'V\', \'F\', \'F\', \'F\')';
	/*funcao que gera o html*/
	gerarInputVF(entrada, name, gabarito, 'emex2', 58); 
