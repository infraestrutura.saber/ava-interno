var alternativas = new Array();
alternativas[0] = "O curativo da incisão cirúrgica deve ser realizado apenas com solução fisiológica a 0,9% e cobertura seca nas primeiras 48 horas ou enquanto houver secreção.";
alternativas[1] = "Não se deve usar álcool 70% nas feridas cirúrgicas. O álcool deve ser a 90%.";
alternativas[2] = "Quanto ao uso de soluções para realização de curativo, o uso de Polvidine é considerado adequado.";
alternativas[3] = "Não se devem realizar curativos, as feridas cirúrgicas devem permanecer abertas desde o primeiro dia.";
var name = "posex1";
gerarinputmultipla(alternativas, name, 0, 45);

