var alternativas = new Array();
alternativas[0] = "Transferir Jorge para avaliação do ortopedista, pois já realizou a antibioticoterapia pelo máximo de tempo recomendado.";
alternativas[1] = "Solicitar hemocultura, pois trata-se de uma osteomielite hematogênica, e, por isso, Jorge está tendo febre, e há saída de secreção purulenta espessa misturada com sangue coagulado pela ferida.";
alternativas[2] = "Solicitar um hemograma, e se estiver com resultado normal, procurar outro sítio de infecção diferente da osteomielite.";
alternativas[3] = "Solicitar uma radiografia simples, que poderá mostrar sinais de destruição óssea e reação periosteal, e encaminhar o paciente para avaliação pelo ortopedista.";
var name = "osteoex2";
gerarinputmultipla(alternativas, name, 3, 47);

