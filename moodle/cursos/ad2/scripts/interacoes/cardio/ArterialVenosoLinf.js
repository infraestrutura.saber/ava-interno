$("#img_coro").attr("src", "../../../scripts/interacoes/cardio/arterial_lin_ven-3.jpg");
$(".tab_coro").css({"border-top-color": "#ef434f", 
             "border-top-width":"10px", 
             "border-top-style":"solid"});

$(".arterial").click(function (){

			$("#img_coro").attr("src", "../../../scripts/interacoes/cardio/arterial_lin_ven-3.jpg");
			$(".tab_coro").css({"border-top-color": "#ef434f"});
	});

$(".venoso").click(function (){

			$("#img_coro").attr("src", "../../../scripts/interacoes/cardio/arterial_lin_ven-2.jpg");
			$(".tab_coro").css({"border-top-color": "#3593d1"});
	});

$(".linfatico").click(function (){
			
			$("#img_coro").attr("src", "../../../scripts/interacoes/cardio/arterial_lin_ven-1.jpg");
			$(".tab_coro").css({"border-top-color": "#cca996"});

	});


(function($) {
    $.fn.blink = function(options) {
        var defaults = {
            delay: 500
        };
        var options = $.extend(defaults, options);

        return this.each(function() {
            var obj = $(this);
            setInterval(function() {
                if ($(obj).css("visibility") == "visible") {
                    //$(obj).css('visibility', 'hidden');
                    $(obj).fadeOut(1000);
                    $(obj).fadeIn(1000);
                }
            }, options.delay);
        });
    }
}(jQuery)) 

$(document).ready(function() {
    $('#blink').blink(); // default is 500ms blink interval.
    // $('.blink_second').blink({
    //     delay: 100
    // }); // causes a 100ms blink interval.
    // $('.blink_third').blink({
    //     delay: 1500
    // }); // causes a 1500ms blink interval.     
});