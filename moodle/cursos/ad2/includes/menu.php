<?php 

//pedindo sessao do moodle
require_login();
$USUARIO = &$_SESSION['USER'];

session_start('config');
$user_id = $_SESSION['id'];
$username = $_SESSION['cpf'];
$nome = $_SESSION['name'];
$ultimoNome= $_SESSION['ultimoNome'];
//verifica se o aluno esta com a sessao aberta(logado)
if ($_SESSION['logado'] == 0) {
	//echo "<script> alert('Barrado!'); </script>";
	echo "<script>window.self.location.href ='http://www.sabertecnologias.com.br/moodle_interno/cursos/ad2'</script>";
}


?>
<div id="menu">
	
		 <ul>
			 <li>
			 	<?php
				if (end(explode("/", getcwd())) == "apresentacao") {

					$nivel = "../";
					$nivelFuncionAlert="../";
					$bool = false;
					require_once "../../../config.php";
					require_once '../modulos/gamefication/functions/verificaAutorizacao.php';
					
				} else if (end(explode("/", getcwd())) == "forum_discussoes") {

					$nivel = "../";
					$nivelFuncionAlert="../../";
					$bool = true;
					require_once '../modulos/gamefication/functions/verificaAutorizacao.php';

				} else {

					$nivel = "../../../";
					$nivelFuncionAlert="../../";
					$bool = true;
					include "../../../../config.php";
					require_once '../../../modulos/gamefication/functions/verificaAutorizacao.php';

				}
				
				echo '<a href="' . $nivel . 'apresentacao/index.php"><img id="icone_home" src="' . $nivel . 'imagens/home.png">Início</a>';
				?>


			 </li>
			 <li>
				 <a>Unidades didáticas <b class="caret"></b></a>
				 <ul>
				 	<!--UNIDADE 1-->
					<li>
						<a>Doenças respiratórias <b class="caret-right"></b></a>
						<ul class="nivel_1">
								<li id="ps1menu">
									<?php //echo '<a href="'.$nivel.'unidades/unidade1/asma/index.php?uni=1&caso=1&topico=0&resource=1">Asma</a>';

									echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\'' . $nivel . '\', 1);">Asma</a>';
									//porcentagem($caso_clinico,$topico,$cpf);
									?>
								</li>
								<li id="ps2menu">
									<?php echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\'' . $nivel . '\', 2);">Doença pulmonar obstrutiva crônica (DPOC)</a>';

									//echo '<a href="'.$nivel.'unidades/unidade1/dpoc/index.php?uni=1&caso=2&topico=0&resource=70"></a>';
									//porcentagem($caso_clinico,$topico,$cpf);
									?>
								</li>
								<li id="ps3menu">
									<?php echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\'' . $nivel . '\', 3);">Pneumonia</a>';
									//porcentagem($caso_clinico,$topico,$cpf);
									?>

								</li>
						</ul>

					</li>
					<!--UNIDADE 2-->
					<li>
						<a >Doenças cardiovasculares<b class="caret-right"></b></a>
						<ul class="nivel_2">
								<li id="ps4menu">
									<?php echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\'' . $nivel . '\', 4);">Coronariopatia</a>';
									//porcentagem($caso_clinico,$topico,$cpf);
									?>

								</li>
								<li id="ps5menu">
									<?php echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\'' . $nivel . '\', 5);">Doença vascular periférica</a>';
									//echo '<a href="'.$nivel.'unidades/unidade2/doencas_vasculares/index.php?uni=2&caso=5&topico=0&resource=15"></a>';
									//porcentagem($caso_clinico,$topico,$cpf);
									?>

								</li>
								<li id="ps6menu">
									<?php echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\'' . $nivel . '\', 6);">Insuficiência cardíaca</a>';
									//echo '<a href="'.$nivel.'unidades/unidade2/insuficiencia_cardiaca/index.php?uni=2&caso=6&topico=0&resource=25">Insuficiência cardíaca</a>';
									//porcentagem($caso_clinico,$topico,$cpf);
									?>

								</li>
						</ul>


					</li>
					<!--UNIDADE 3-->
			      	<li>
			      			<a>Doenças infectocontagiosas <br>parasitárias crônicas<b style="margin-top: -5px;!important" class="caret-right"></b></a>
						<ul class="nivel_3">
								<li id="ps7menu">
									<?php echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\'' . $nivel . '\', 7);">Hanseníase</a>';

									//echo '<a href="'.$nivel.'unidades/unidade3/hanseniase/index.php?uni=3&caso=7&topico=0&resource=140">Hanseníase</a>';
									//porcentagem($caso_clinico,$topico,$cpf);
									?>
								</li>
								<li id="ps8menu">
									<?php echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\'' . $nivel . '\', 8);">Síndrome da imunodeficiência adquirida (SIDA)</a>';

									//echo '<a href="'.$nivel.'unidades/unidade3/sida/index.php?uni=3&caso=9&topico=0&resource=1">SIDA</a>';
									//porcentagem($caso_clinico,$topico,$cpf);
									?>

								</li>
								<li id="ps9menu" >
									<?php echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\'' . $nivel . '\', 9);">Malária</a>';
									//echo '<a href="'.$nivel.'unidades/unidade3/malaria/index.php?uni=3&caso=8&topico=0&resource=1">Malária</a>';
									//porcentagem($caso_clinico,$topico,$cpf);
									?>

								</li>
								<li id="ps10menu">
									<?php echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\'' . $nivel . '\', 10);">Tuberculose</a>';

									//echo '<a href="'.$nivel.'unidades/unidade3/tuberculose/index.php?uni=3&caso=10&topico=0&resource=1">Tuberculose</a>';
									//porcentagem($caso_clinico,$topico,$cpf);
									?>
								</li>
						</ul>

			      	</li>
			      	<!--UNIDADE 4-->
			      	<li>
			      		<a>Infecção do trato urinário<b class="caret-right"></b></a>
						<ul class="nivel_4">
								<li id="ps11menu">
									<?php echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\'' . $nivel . '\', 11);">Infec&ccedil;&atilde;o do Trato Urin&aacute;rio</a>';
									//echo '<a href="'.$nivel.'unidades/unidade4/itu/index.php?uni=4&caso=11&topico=0&resource=100">Infecção do trato urinário</a>';
									//porcentagem($caso_clinico,$topico,$cpf);
									?>
								</li>
						</ul>


			      	</li>

			      	<!--UNIDADE 5-->
			      	<li>
			      		<a>Problemas ortopédicos<b class="caret-right"></b></a>
						<ul class="nivel_5">
								<li id="ps12menu">
									<?php echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\'' . $nivel . '\', 13);">Osteomielite</a>';

									//echo '<a href="'.$nivel.'unidades/unidade5/osteomielite/index.php?uni=5&caso=13&topico=0&resource=34">Osteomielite</a>';
									//porcentagem($caso_clinico,$topico,$cpf);
									?>
								</li>

								<li id="ps13menu">
									<?php echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\'' . $nivel . '\', 12);">Pós-operatório em ortopedia</a>';
									//echo '<a href="'.$nivel.'unidades/unidade5/pos_ortopedia/index.php?uni=5&caso=12&topico=0&resource=43">Pós-operatório</a>';
									//porcentagem($caso_clinico,$topico,$cpf);
									?>

								</li>
						</ul>

			      	</li>
			      	<!--UNIDADE6-->
			      	<li>
			      		<a>Transtornos psiquiátricos<b class="caret-right"></b></a>
						<ul class="nivel_6">
								<li id="ps14menu">
									<?php echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\'' . $nivel . '\', 14);">Transtornos psiquiátricos</a>';
									//echo '<a href="'.$nivel.'unidades/unidade6/trans_psiquiatricos/index.php?uni=6&caso=14&topico=0&resource=1">Transtornos psiquiátricos</a>';
									//porcentagem($caso_clinico,$topico,$cpf);
									?>
								</li>
						</ul>


			      	</li>
			      	<!--UNIDADE7-->	
			      	<li> 
			      		<a>Álcool e outras drogas<b class="caret-right"></b></a>
						<ul class="nivel_7">
								<li id="ps15menu">
									<?php echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\'' . $nivel . '\', 15);">Álcool	
  e	
  outras	
  drogas</a>';

									//echo '<a href="'.$nivel.'unidades/unidade7/alcool/index.php?uni=7&caso=15&topico=0&resource=97">Álcool e outras drogas</a>';
									//porcentagem($caso_clinico,$topico,$cpf);
									?>
								</li>
								<!-- <li>
									<?php 
									//echo '<a href="'.$nivel.'unidades/unidade7/drogas/index.php?uni=7&caso=16&topico=0&resource=1">Drogas</a>';
									//porcentagem($caso_clinico,$topico,$cpf); 
									?>

								</li> -->
						</ul>
			      	</li>
			      	<!--UNIDADE8-->
			      	<li>
			      		<a >Doenças neurodegenerativas<b class="caret-right"></b></a>
						<ul class="nivel_8">
								<li id="ps16menu">
									<?php echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\'' . $nivel . '\', 17);">Esclerose múltipla</a>';
									//echo '<a href="'.$nivel.'unidades/unidade8/esclerose_multipla/index.php?uni=8&caso=18&topico=0&resource=122">Esclerose múltipla</a>';
									//porcentagem($caso_clinico,$topico,$cpf);
									?>
								</li>
								<li id="ps17menu">
									<?php echo '<a class="inline" href="#modal_unidade" onclick="abreModalUnidade(\'menu\',\'' . $nivel . '\', 16);">Esclerose lateral amiotrófica</a>';
									//echo '<a href="'.$nivel.'unidades/unidade8/esclerose_lateral/index.php?uni=8&caso=17&topico=0&resource=131">Esclerose lateral</a>';
									//porcentagem($caso_clinico,$topico,$cpf);
									?>

								</li>
						</ul>

			      	</li>
				 </ul>
			 </li>
		 <li>
		 
				<?php echo '<a href="' . $nivel . 'forum_discussoes">Fórum de discussões</a>';
			 		include $nivelFuncionAlert.'functions/functions.php';
			 		include  $nivel.'modulos/gamefication/functions/autorizarNomeRanking.php';
			 		alert_postage($user_id);
			 		//atualizar_alert($user_id);
					
				?>

		 </li>
      <li><a class="inline" href="#modal_certificacao" onclick="abreModal();">Certificação</a></li>
      <li><a class="inline" href="#modal_perguntas" onclick="abreModal();">Perguntas frequentes</a></li>
      <li><a class="inline" href="#modal_perfil" onclick="abreModal();">Perfil e opinião</a></li>
      <li><a class="inline" href="#modal_creditos" onclick="abreModal();">Créditos</a></li>
      <li><a class="inline trophyAction" href="#modal_ranking" onclick="abreModalRanking();">Top10 <?php echo '<div class="trophy"> <img  src="' . $nivel . 'imagens/trophy.png"></div>';?></a></li>
      
      <!--<li><?php echo '<a class="inline" href="#modal_ranking" onclick="abreModalRanking();"><img  src="' . $nivel . 'imagens/trophy.png" style="float:left;margin-right:3px;margin-top:-1px">Game</a>'; ?></li>-->
		 </ul>
		 
		 <div id="usuario">
							  		<p>Olá, <span style="margin-right: 8px;"><?php echo $nome . " |"; ?></p>  
					</div>
		<div id="sair">Sair</div>
		<div id="fechar">
			<p>
			<?php
			include ('sair.php');
			echo '<img id="icone_sair" src="' . $nivel . 'imagens/sair.png"></a>';
			?>
			</p>
		</div>
		
	  </div>
	  
	  
	  <div id="container_game">

	  	<!-- ICONE MOVIMENTANDO
		  <div id="gamefication">
		  	  <?php echo '<img id="icone_sair" src="' . $nivel . 'imagens/trophy.png"></a>'; ?>
		  </div> 

		  <div id="barraGame">
			  <?php echo ' &nbsp; &nbsp;<img  src="' . $nivel . 'imagens/thumbs-up.png">';?> 2  <?php echo '<img  src="' . $nivel . 'imagens/thumbs-up.png">';?> 5 <?php echo '<img  src="' . $nivel . 'imagens/file.png">';?> 23 <?php echo '<img  src="' . $nivel . 'imagens/pencil.png">';?> 7 &nbsp; &nbsp; &nbsp; <strong>13º/1000</strong> &nbsp; &nbsp; &nbsp;<a class="inline" style="display: inline;" href="#modal_ranking" onclick="abreModalRanking();">Ver ranking</a>
		  </div> -->


		 <!-- BARRA MOVIMENTANDO -->
		 <!-- <a class="inline" style="display: inline;" href="#modal_ranking" onclick="abreModalRanking();">  
		 	<div id="gamefication">
		  	 
		  	  <?php echo '<img id="icone_sair" src="' . $nivel . 'imagens/trophy.png"></a>'; ?>

		  	</div>  
		  </a> -->
		 
		  <div id="barraGame">
		  	<?php 
		  	//select ranking da sessao
						$sql = mysql_query("SELECT * FROM mdl_gamefication where user_id='$user_id' ORDER BY total_pontuacao DESC") or die(mysql_error());
						
						while ($resultado = mysql_fetch_array($sql)) {
							
							$likesr_sessao = $resultado['likesRespostas'];
							$likesp_sessao = $resultado['likesPerguntas'];
							$exe_sessao = $resultado['exercicios'];
							$prova_sessao = $resultado['prova'];
							$total_sessao =$resultado['total_pontuacao'];
							
						}	
						$select_total= mysql_query("SELECT COUNT(*) as totalparticipantes FROM mdl_gamefication")or die(mysql_error());
						$data_total=mysql_fetch_assoc($select_total);
						
						//select posicao
						$sql_position = mysql_query("SELECT COUNT(*) as total FROM mdl_gamefication  WHERE total_pontuacao > '$total_sessao'")or die(mysql_error());
						$data=mysql_fetch_assoc($sql_position);
						$position=$data['total']+1;
		  	?>
			 <?php echo ' &nbsp; &nbsp;<img  src="' . $nivel . 'imagens/simb_4.png"> '.$likesr_sessao;?><?php echo ' <img  src="' . $nivel . 'imagens/simb_5.png"> '.$exe_sessao;?> <?php echo ' <img  src="' . $nivel . 'imagens/simb_6.png"> '.$prova_sessao;?> <?php echo ' <img  src="' . $nivel . 'imagens/simb_3.png"> '.$likesp_sessao;?>   &nbsp; &nbsp; &nbsp; <strong>Colocação: <?php echo $position.' / '.$data_total['totalparticipantes']; ?></strong>
		  </div> 




	  </div>

	  <!-- <div id="barraGamefication">
		 <div id="menuGamefication"> <?php echo 'Ola, '.$nome.' '.$ultimoNome.'<img  src="' . $nivel . 'imagens/thumbs-up.png">';?> 2 <?php echo '<img  src="' . $nivel . 'imagens/file.png">';?> 23 <?php echo '<img  src="' . $nivel . 'imagens/pencil.png">';?> 2  Colocação 10/1000 </div>
		 </div> -->
	 
<?php
if ($bool == true) {
	include ("modal_unidades.php");
}
?>


	   <script type="text/javascript">

/* Objeto movendo

	   $('#gamefication').click(function(event) {

	   		var value = 0;
	   		var fechar = false;
		   
		  	if ($('#gamefication').css('left') =='362px'){
		  		fechar = false;
		   		value = '0';
		    } else if($('#gamefication').css('left') =='0px'){
		    	fechar = true;
		    	 $("#barraGame").fadeOut();
		    	 $('#gamefication').removeClass("sombra_game");
		    	value = '362px';

		    } 

		   
	    	$('#gamefication').animate({
	          left: value
		      }, function(){
			      	if(fechar == false){
			      		 $("#barraGame").fadeIn();
			      		 $('#gamefication').addClass("sombra_game");
			      	}
		      	
		      });
	    
		    
		   
		});

*/
		
		$('.trophyAction').mouseenter(function(event) {

	   		var value = '357px';
	   		$("#barraGame").slideDown(300);
	   				   
	    	/*$('#barraGame').animate({
	          width: value

		      }, function(){
			      			$("#barraGame").fadeIn();
			      		 
			      		 $('#gamefication').addClass("sombra_game");
			      
		      	
		      });
	    */
		    
		   
		});

		$('.trophyAction').mouseleave(function(event) {

	   		var value = '0px';
	   		 $("#barraGame").slideUp(100);
	   		
	   				   
	    	/*$('#barraGame').animate({
	          width: value

		      }, function(){
			      	
			      		$("#barraGame").hide();
			      		 $('#gamefication').removeClass("sombra_game");
			      
		      	
		      });*/
	    
		    
		   
		});



		function abreModalRanking() {
			$(".inline").colorbox({
				inline : true,
				width : "850px"
			});
		}

		function abreModal() {
			$(".inline").colorbox({
				inline : true,
				width : "1050px"
			});

			if (localStorage.getItem('modal_block') == 'on') {
				if (document.getElementById('check_modal') != null)
					document.getElementById('check_modal').checked = true;
			} else {
				if (document.getElementById('check_modal') != null)
					document.getElementById('check_modal').checked = false;
			}

		}

		function abreModalUnidade(tipo) {

			if (tipo == "menu") {

				if (localStorage.getItem('modal_block') == null) {
					abreModal();
					$(".inline").colorbox({
						inline : true,
						width : "1050px"
					});
				} else if (localStorage.getItem('modal_block') == 'off') {
					abreModal();
					$(".inline").colorbox({
						inline : true,
						width : "1050px"
					});
				} else if (localStorage.getItem('modal_block') == 'on') {
					$.colorbox.remove();
				}

			} else {
				abreModal();

			}

		}

		function abreModalUnidade(tipo, nivel, caso) {

			if (tipo == "menu") {

				if (localStorage.getItem('modal_block') == null) {
					abreModal();
					$(".inline").colorbox({
						inline : true,
						width : "1050px"
					});
				} else if (localStorage.getItem('modal_block') == 'off') {
					abreModal();
					$(".inline").colorbox({
						inline : true,
						width : "1050px"
					});
				} else if (localStorage.getItem('modal_block') == 'on') {
					redireciona(nivel, caso);

					$.colorbox.remove();
				}

			} else {
				abreModal();

			}

		}

		function redireciona(nivel, caso) {

			if (caso == 1) {
				location.href = nivel + 'unidades/unidade1/asma/index.php?uni=1&caso=1&topico=0&resource=87';

			} else if (caso == 2) {
				location.href = nivel + 'unidades/unidade1/dpoc/index.php?uni=1&caso=2&topico=0&resource=70';

			} else if (caso == 3) {
				location.href = nivel + 'unidades/unidade1/pneumonia/index.php?uni=1&caso=3&topico=0&resource=62';

			} else if (caso == 4) {

				location.href = nivel + 'unidades/unidade2/coronariopatia/index.php?uni=2&caso=4&topico=0&resource=5';

			} else if (caso == 5) {

				location.href = nivel + 'unidades/unidade2/doencas_vasculares/index.php?uni=2&caso=5&topico=0&resource=15';

			} else if (caso == 6) {

				location.href = nivel + 'unidades/unidade2/insuficiencia_cardiaca/index.php?uni=2&caso=6&topico=0&resource=25';

			} else if (caso == 7) {

				location.href = nivel + 'unidades/unidade3/hanseniase/index.php?uni=3&caso=7&topico=0&resource=143';

			} else if (caso == 9) {

				location.href = nivel + 'unidades/unidade3/malaria/index.php?uni=3&caso=8&topico=0&resource=160';

			} else if (caso == 8) {

				location.href = nivel + 'unidades/unidade3/sida/index.php?uni=3&caso=9&topico=0&resource=151';

			} else if (caso == 10) {

				location.href = nivel + 'unidades/unidade3/tuberculose/index.php?uni=3&caso=10&topico=0&resource=167';

			} else if (caso == 11) {

				location.href = nivel + 'unidades/unidade4/itu/index.php?uni=4&caso=11&topico=0&resource=100';

			} else if (caso == 12) {

				location.href = nivel + 'unidades/unidade5/pos_ortopedia/index.php?uni=5&caso=12&topico=0&resource=43';

			} else if (caso == 13) {

				location.href = nivel + 'unidades/unidade5/osteomielite/index.php?uni=5&caso=13&topico=0&resource=34';

			} else if (caso == 14) {

				location.href = nivel + 'unidades/unidade6/trans_psiquiatricos/index.php?uni=6&caso=14&topico=0&resource=50';

			} else if (caso == 15) {

				location.href = nivel + 'unidades/unidade7/alcool/index.php?uni=7&caso=15&topico=0&resource=97';

			} else if (caso == 16) {

				location.href = nivel + 'unidades/unidade8/esclerose_lateral/index.php?uni=8&caso=17&topico=0&resource=131';

			} else if (caso == 17) {

				location.href = nivel + 'unidades/unidade8/esclerose_multipla/index.php?uni=8&caso=18&topico=0&resource=122';

			}
		}

		</script>


			<!-- Ranking-->
			<div style='display:none'>
				<div id="modal_ranking" style='padding:10px; background:#fff;'>
					
					<?php 
					//select ranking da sessao
						// $sql = mysql_query("SELECT * FROM mdl_gamefication where user_id='$user_id' ORDER BY total_pontuacao DESC") or die(mysql_error());
// 						
						// while ($resultado = mysql_fetch_array($sql)) {
// 							
							// $likesr_sessao = $resultado['likesRespostas'];
							// $likesp_sessao = $resultado['likesPerguntas'];
							// $exe_sessao = $resultado['exercicios'];
							// $prova_sessao = $resultado['prova'];
							// $total_sessao =$resultado['total_pontuacao'];
// 							
						// }	
						
						
						?>
					<h4 style="color: #fff;">Ranking</h4>



					<div id="colocacao">

					<?php  if($data_total['totalparticipantes'] < 999) {   ?>
						<p style="font-size: 55px;margin-top: 63px;text-align: center;font-weight: bold;">
							<?php echo $position.'º'; ?>

						<p style="width: 42px;margin-left: 109px;position: absolute;margin-top: -13px;font-size: 14px;font-weight: bold;"> 
							<?php echo ' / '.$data_total['totalparticipantes']; ?>
						</p>
						</p>
					<?php } else if($data_total['totalparticipantes'] > 999){  ?>

						<?php if($position > 999){ ?>

							<p style="font-size: 34px;margin-top: 63px;text-align: center;font-weight: bold;">
								<?php echo $position.'º'; ?>

						<?php } else { ?>

							<p style="font-size: 55px;margin-top: 63px;text-align: center;font-weight: bold;">
								<?php echo $position.'º'; ?>					

						<?php } ?>

						<p style="width: 42px;margin-left: 109px;position: absolute;margin-top: -21px;font-size: 12px;font-weight: bold;"> 
							<?php echo ' / '.$data_total['totalparticipantes']; ?>
						</p>
						</p>

					<?php } ?>
					
					</div>



					<div id="texto_pont">Sua pontuação:</div>
					 
					<div id="lista"> 
					<?php echo ' <img  src="' . $nivel . 'imagens/simb_4.png" title="Respostas curtidas no Fórum">'.'&nbsp;'.$likesr_sessao;?>&nbsp; &nbsp; &nbsp;
					<?php echo ' <img  src="' . $nivel . 'imagens/simb_5.png" title="Pontuação exercícios">'.'&nbsp;'.$exe_sessao;?> &nbsp; &nbsp;&nbsp;
					<?php echo ' <img  src="' . $nivel . 'imagens/simb_6.png" title="Nota da prova">'.'&nbsp;'.$prova_sessao;?>&nbsp; &nbsp; &nbsp;
					<?php echo ' <img  src="' . $nivel . 'imagens/simb_3.png" title="Perguntas curtidas no Fórum">'.'&nbsp;'.$likesp_sessao;?>&nbsp; &nbsp;&nbsp;
				
<!--<?php echo  " Exercicios corretos: ".$exe_sessao." <br> Perguntas curtidas: ".$likesp_sessao." <br> Respostas curtidas: ".$likesr_sessao." <br> Nota da prova: ".$prova_sessao."<br><b>Total de Pontos: ".$total_sessao."<b>"; ?>--></div>
					<?php echo '<img style="position: absolute; right: 86; top: 60;" src="' . $nivel . 'imagens/Obj_1.png">' ?>
					
					

					<br><br><strong style="position: absolute; top: 289; color: #fff;"><u></u></strong>

					<table class="table table-striped table-hover table-bordered" style="border: 1px solid #ccc; margin-top: 226px;">
				    <thead style="background-color: #333333; color: #ffffff;">
				        <tr>
				        	<th><?php echo ' <img src="' . $nivel . 'imagens/trophy_titulo.png"><br>Colocação';?></th>
				            <th style="width:150px;"><?php echo ' <img  src="' . $nivel . 'imagens/titulo_2.png"><br>Nome';?></th>
				            <th title="Respostas curtidas no Fórum"><?php echo ' <img  src="' . $nivel . 'imagens/titulo_4.png" ><br> Respostas';?></th>
				            <th title="Pontuação exercícios"><?php echo ' <img  src="' . $nivel . 'imagens/simb_5.png" ><br>Exercícios';?></th>
				            <th title="Nota da prova"><?php echo ' <img  src="' . $nivel . 'imagens/simb_6.png" ><br>Prova';?></th>
				            <th title="Perguntas curtidas no Fórum"><?php echo ' <img  src="' . $nivel . 'imagens/titulo_3.png" ><br> Perguntas';?></th>  	            
				            <th style="background-color:#CE8F22;"><?php echo ' <img  src="' . $nivel . 'imagens/titulo_7.png">';?></th>
				        </tr>
				    </thead>
				    <tbody>
				    	<?php //select ranking geral 
				    	rankingGeral();	?>	
				    </tbody>
				</table>

				
				<div class="feedAutorizacao alert-success" style="display:none; padding:15px;"></div>
				<!--<a class="btn" style="background-color: #333333;color: #fff;border-radius: 24px;position: absolute;right: 10;bottom: 90;background-image: none;width: 100px;"><strong>Permitir</strong></a> -->
				<?php verificaAutorizacao($user_id); ?>	 
				<script type="text/javascript">

					function autorizacaoPost(){
						
						$.post( '../modulos/gamefication/functions/post.php', 
								{ funcao: 'autorizacao'}, function(data){ alert(data);//location.reload();
								 } 
								 ).done(function() {
									    //$('.feedAutorizacao').append('Adicionado').fadeIn(2000);
									    $('.textoAutorizacao').slideToggle(500);
									    $('.btn_autorizacao').slideToggle(500);
									   $('.feedAutorizacao').append("Nome liberado com sucesso. Por favor atualize a página clicando aqui <a href='javascript:history.go(0);' onClick='history.go(0);'>Atualizar</a>").slideToggle(500); 
									  
									   
									   
								  });
					}
				</script>
				<br>
				
				</div>
			</div><!-- fim -->

			<!-- Certificacao-->
			<div style='display:none'>
				<div id="modal_certificacao" style='padding:10px; background:#fff;'>
					<h4>Certificação</h4>

					<br>

					Para que você receba a declaração de conclusão deste módulo, é necessário fazer a avaliação  final. Recomendamos que esta seja a última etapa, depois que você tiver concluído seus estudos.  <br><br>
					A avaliação vale de zero (0,0) a dez (10,0). Para receber a declaração de conclusão do módulo é necessário tirar, pelo menos, 7,0 (sete). São permitidas 3 (três tentativas). Caso você  não atinja nota mínima 7,0 (sete), será necessário fazer o curso novamente.   Após atingir nota igual ou superior a 7,0 (sete), você receberá, por e-mail, um link que  permitirá a impressão do certificado. O link será encaminhado em até 72 horas após a conclusão da avaliação.   <br><br>
					Clique <a href="http://www.sabertecnologias.com.br/moodle_interno/mod/quiz/view.php?id=302" target="_blank">aqui</a> para iniciar a avaliação final.

			
				<br>
				
				</div>
			</div><!-- fim -->

			<!-- Perguntas-->
			<div style='display:none'>
				<div id="modal_perguntas" style='padding:10px; background:#fff;'>
					<h4>Perguntas frequentes</h4>

					<br>

					<b>1. Tenho interesse de fazer este módulo. Onde encontro informações?</b><br />
					As informações sobre o módulo Abordagem domiciliar de situações clínicas comuns em adultos, bem como o link para o sistema de matrícula estão disponíveis em: <a href="http://www.unasus.gov.br/cursoAD" target="_blank">http://www.unasus.gov.br/cursoAD</a>
					<br /><br />
					<b>2. Não estou conseguindo receber a declaração de conclusão de curso. O que está havendo?</b><br />
Reforçamos que o conteúdo didático do curso tem acesso público e irrestrito. No entanto, a declaração de conclusão de curso é disponibilizada apenas para médicos e enfermeiros que atenderam aos requisitos para matrícula na versão do módulo com certificação. 
					<br /><br />
					<b>3. Fiz a avaliação final do módulo, tirei nota igual ou maior que sete, mas a declaração de conclusão de curso ainda não chegou. O que aconteceu?</b><br />
O link para acesso à declaração é enviado por e-mail no prazo de 72 horas após o término da avaliação final. Se já passou este período, recomendamos que você verifique qual foi o e-mail que cadastrou no Cadastro Nacional de Profissionais de Saúde (<a href="https://cnps.unasus.gov.br/cnps/Home.app" target="_blank">https://cnps.unasus.gov.br/cnps/Home.app</a>) e verifique, também, as caixas SPAM, lixo eletrônico ou quarentena do seu e-mail. 
					<br /><br />
					Você também pode consultar a declaração na Plataforma Arouca de acordo com as orientações abaixo:<br />
					1) Acesse o endereço: <a href="http://arouca.unasus.gov.br" target="_blank">http://arouca.unasus.gov.br</a><br>
					2) Clique em "Faça seu login" <br>
					3) Digite seu CPF e sua senha e clique em "Autenticar" <br>
					4) Clique em "Minha Página > Meus Certificados" ou "Minha Página > Meu Histórico". Localize a tabela na qual estão listados os cursos nos quais você participou com os respectivos certificados, caso tenha atendido aos requisitos para certificação.
					
					<br /><br />
					<b>4. É possível enviar minha declaração de conclusão de curso por e-mail?</b><br />
					Não. O que você receberá por e-mail é o link para ter acesso à declaração. Verifique o seu e-mail, lembrando que você deve acessar o e-mail que cadastrou no Cadastro Nacional de Profissionais de Saúde. Recomendamos, ainda, que você verifique as caixas SPAM, lixo eletrônico ou quarentena do seu e-mail.
					<br /><br />
					Você também pode consultar sua declaração na Plataforma Arouca de acordo com as orientações abaixo:<br>
					1) Acesse o endereço: <a href="http://arouca.unasus.gov.br" target="_blank">http://arouca.unasus.gov.br</a><br>
					2) Clique em "Faça seu login" <br>
					3) Digite seu CPF e sua senha e clique em "Autenticar" <br>
					4) Clique em "Minha Página > Meus Certificados" ou "Minha Página > Meu Histórico". Localize a tabela na qual estão listados os cursos nos quais você participou com os respectivos certificados, caso tenha atendido aos requisitos para certificação.
					
					<br /><br />
					<b>5. Gostaria de mais informações sobre o Programa Multicêntrico de Qualificação Profissional em Atenção Domiciliar a Distância. Onde encontro?</b><br />
					Você pode consultar as informações no link: <a href="http://www.unasus.gov.br/cursoAD" target="_blank">http://www.unasus.gov.br/cursoAD</a>

					<br /><br />			
					<b>6. Como posso ter acesso aos demais módulos do o Programa Multicêntrico de Qualificação Profissional em Atenção Domiciliar a Distância?</b><br />
					As informações estão disponíveis no link: <a href="http://www.unasus.gov.br/cursoAD" target="_blank">http://www.unasus.gov.br/cursoAD</a>
				<br>
				
				</div>
			</div><!-- fim -->

			<!-- Opiniao-->
			<div style='display:none'>
				<div id="modal_perfil" style='padding:10px; background:#fff;'>
					<h4>Perfil e opinião</h4>

					<br>
				Gostaríamos de conhecer o seu perfil tecnológico e a sua opinião sobre o curso.  <br>
				Clique <a href="http://www.sabertecnologias.com.br/moodle_interno/mod/resource/view.php?id=417" target="_blank">aqui</a> para responder ao questionário.


			
				<br>
				
				</div>
			</div><!-- fim -->

			<!-- Creditos-->
			<div style='display:none'>
				<div id="modal_creditos" style='padding:10px; background:#fff;' style="font-size: 14">
					<h4>Créditos</h4>

					<br>
						<b>© 2014. Ministério da Saúde. Secretaria de Atenção à Saúde.  Departamento de Atenção Básica. 
						<br>Coordenação Geral de Atenção Domiciliar.</b><br><br>
Todos os direitos reservados. É permitida a reprodução parcial ou total desta obra, desde que citada a  fonte e que não seja para venda ou qualquer fim comercial. A
responsabilidade pelos direitos autorais de textos e imagens desta obra é da equipe de produção.<br>
						<br>

				<div id="left">
				
					<b>Curso online Abordagem de situações clínicas comuns em adultos</b><br>
					Primeira oferta: dezembro de 2014 <br><br>

					<b>Elaboração, difusão e informações</b><br>
					Universidade Aberta do Sistema Único De Saúde (UNA-SUS)<br>
					Secretaria de Gestão do Trabalho e da Educação Na Saúde (SGTES) <br>
					Secretaria de Atenção à Saúde – Departamento de Atenção Básica – Coordenação Geral de Atenção Domiciliar<br><br>

					<b>UNA-SUS | Universidade Federal de Pernambuco </b><br>
					Coordenação Geral: Cristine Martins Gomes de Gusmão <br>
					Coordenação Técnica: Josiane Lemos Machiavelli <br>
					Coordenação de Educação a Distância: Sandra de Albuquerque Siebra <br>
					Gerente de Projetos: Júlio Venâncio de Menezes Júnior<br>
					<br>

					<i><b>Equipe de produção do curso</b></i><br><br>

					<b>Editora </b><br>
					Josiane Lemos Machiavelli (UNA-SUS UFPE)  <br><br>

					<b>Conteudistas</b><br>
					Isabel Brandão Correia (Secretaria de Saúde do Recife) <br>
					Marciana Batista da Silva (Secretaria Estadual de Saúde de Pernambuco)<br>
					Patrícia Pereira da Silva (UNA-SUS UFPE)  <br>
					<br>
					<!-- <b>Autora colaboradora</b><br>
					Patrícia Pereira da Silva (UNA-SUS UFPE)<br>
					<br> -->
					<b>Organizadora e Designer instrucional </b><br>
					Patrícia Pereira da Silva (UNA-SUS UFPE)  <br>
					<br>
					<b>Designers gráficos </b><br>
					Juliana Leal (UNA-SUS UFPE)  <br>
					Rodrigo Cavalcanti Lins (UNA-SUS UFPE)<br>
					<br />

					
					<!-- <br /> -->



				</div>
				

				<div id="right">

				<b>Ilustrações</b><br />
					Cleyton Nicollas de Oliveira Guimarães (UNA-SUS UFPE)<br>
					Juliana Leal da Cruz (UNA-SUS UFPE)<br />
					Silvânia Cosmo Esmerindo Vieira (UNA-SUS UFPE)<br/>
					Vinícius Haniere Saraiva Milfont (UNA-SUS UFPE)<br />
					

<b><br />
Revisão linguística</b><br />
Ângela Borges (UNA-SUS UFPE)<br>
Emanuel Cordeiro da Silva (UNA-SUS UFPE)<br />
Eveline Costa Lopes (UNA-SUS UFPE)<br/>
<br />

<b>Equipe de tecnologia da informação</b><br />
Adeilson Irineu de Souza Filho (UNA-SUS UFPE)<br />
Mirela Natali Vieira de Souza (UNA-SUS UFPE)<br />
Priscilla Batista Mendes (UNA-SUS UFPE) <br/>
Renato Rodrigues de Aquino (UNA-SUS UFPE) <br/>
Rodrigo Cavalcanti Lins (UNA-SUS UFPE)<br />
Wellton Thiago Machado Ferreira (UNA-SUS UFPE)<br />
<br />

<b>Equipe de ciência da informação </b><br />
Vildeane da Rocha Borba (UNA-SUS UFPE)<br />
Jacilene Adriana da Silva Correia (UNA-SUS UFPE)<br />
<br />

<b>Equipe de supervisão acadêmica </b><br />
Fabiana de Barros Lima (UNA-SUS UFPE)<br />
Geisa Ferreira da Silva (UNA-SUS UFPE)<br />
Isabella Maria Lorêto da Silva (UNA-SUS UFPE)<br />
Rosilândia Maria da Silva (UNA-SUS UFPE)<br />
<br />

<b>Colaboradores na validação externa do curso </b><br />
Leonardo Savassi (Secretaria Executiva da UNA-SUS)<br>
Lina Sandra Barreto Brasil (Secretaria Executiva da UNA-SUS)<br />
Luciana Guimarães N. De Paula (Coordenação Geral de Atenção Domiciliar)<br/>
Mariana Borges Dias (Coordenação Geral de Atenção Domiciliar)<br />

<br>
</div>
</div>
</div><!-- fim -->


			<style type="text/css">
				#right {
					width: 48%;
					float: right;
					/*font-size: 12px !important;*/
					margin-left: 10px;
				}

				#left {
					width: 46%;
					float: left;
					/*font-size: 12px !important;*/

				}

				#modal_creditos b {
					font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif;!important;
				}

			</style>
