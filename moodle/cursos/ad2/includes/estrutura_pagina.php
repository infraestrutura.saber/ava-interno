<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-br" xml:lang="pt-br">
<head>	
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	

		<link rel="stylesheet" type="text/css" href="../../../css/style.css">
		<script type="text/javascript" src="../../../lib/jquery.js"></script>
		<script type="text/javascript" src="../../../lib/colorbox/jquery.colorbox.js"></script>
		<script type="text/javascript" src="../../../lib/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="../../../scripts/apresentacao.js"></script>
		<script type="text/javascript" src="../../../scripts/script.js"></script>
		

		<link rel="stylesheet" type="text/css" href="../css/style.css">
		<link rel="stylesheet" type="text/css" href="../../../lib/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../../../lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="../../../lib/colorbox/colorbox.css">
		
		
		<title>Unidade <?php $unidade = $_GET['uni'];
							 $caso = $_GET['caso'];
							 echo $unidade;
		?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

			<body>

				<div id="mensagemConteudo" class="mensagemConteudo" style="display: none;">
				  	<div class="container">
				  	<div class="alert" style="height:60px;width:975px;">
					  <button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>Atenção!</strong>
					  As informações disponíveis neste curso têm caráter educativo. 
					  Os procedimentos de diagnóstico e manejo devem ser feitos por profissionais habilitados para tal.
					  <hr style="margin: 0px !important;">
					  <div style="float: right; margin-right: 15px;">
					  	<input style="margin-bottom: 6px; font-weight: bold;" type="checkbox" name="check_warn" id="check_warn" value=""> <span style="margin-top: 5px;">Não exibir na próxima vez</span>
					  </div>
					</div>
					</div>
				</div>

						<script type="text/javascript">
							$(document).ready(function() {
								$("#menu li a").mouseover(function() {
									var index = $("#menu li a").index(this);
									$("#menu li").eq(index).children("ul").slideDown(100);
									if ($(this).siblings('ul').size() > 0) {
										return false;
									}
								});
								$("#menu li").mouseleave(function() {
									var index = $("#menu li").index(this);
									$("#menu li").eq(index).children("ul").slideUp(100);
								});
							});


							$( "#check_warn" ).change(function() {
			 
							 	if($("#check_warn").is(":checked") == true){
							 		 localStorage.setItem('warn_block', 'on');
							 	}else{
							 		 localStorage.setItem('warn_block', 'off');
							 	}



							});


							function show_warn() {

								

									if (localStorage.getItem('warn_block') == null) {
										$("#mensagemConteudo").show();
									} else if (localStorage.getItem('warn_block') == 'off') {
										$("#mensagemConteudo").show();
									} 

								

							}

							show_warn();


		</script>
				
				<?php 
					require_once ('../../../../../config.php'); 
					require_once ('../../../functions/conection.php');
					require_once ('../../../functions/functions.php');
					session_start('config');
					//conectar();
					$id=$USER->id;		
					$cpf = $_SESSION['cpf'];
					$top_ex = array(0,0);
					$id_exercicios = array('vazio');
					$nome_caso = "";
					
					//redirecionar caso nao tenha permissao
					if ($id == "") {
						header('Location: http://ufpe.unasus.gov.br/moodle_interno/cursos/ad2/');
					}

					if (empty($_GET['pagina'])) {
						//se variavél pagina não existir
						$temp = '0';

					}



					function avatar_unidade($unidade, $caso, &$id_exercicios, &$top_ex, &$nome_caso) {
					

						if ($unidade == 1) {
							//Doenças respiratorias
						
							echo "<script type='text/javascript'>$('#avatar').html('<img src=\"../../../imagens/unidade_1/icone_unidade1.png\">');</script>";

							if($caso == 1){
								$nome_caso = "asma";
								$top_ex = array(8,181);
								$id_exercicios = array(4,1,2,3);
								$topicos = "../../../includes/indices/asma.php";
								echo "<script type='text/javascript'>$('.nome_caso').html('Asma');</script>";
								

								

							}else if($caso == 2){
								$nome_caso = "dpoc";
								$top_ex = array(7,182);
								$topicos = "../../../includes/indices/dpoc.php";
								$id_exercicios = array(5,6,7,8,9);
								echo "<script type='text/javascript'>$('.nome_caso').html('Doença Pulmonar Obstrutiva Crônica (DPOC)');</script>";								
							

							}else if($caso == 3){
								$nome_caso = "pneumonia";
								$top_ex = array(8,183);
								$topicos = "../../../includes/indices/pneumonia.php";
								$id_exercicios = array(10,11,12,14,13);
								echo "<script type='text/javascript'>$('.nome_caso').html('Pneumonia');</script>";	

							}

						} else if ($unidade == 2) {

							echo "<script type='text/javascript'>$('#avatar').html('<img src=\"../../../imagens/unidade_2/icone_unidade2.png\">');</script>";


							if($caso == 4){
								$nome_caso = "coronariopatia";
								$top_ex = array(9,184);
								$id_exercicios = array(15,16);
								echo "<script type='text/javascript'>$('.nome_caso').html('Coronariopatia');</script>";	
								$topicos = "../../../includes/indices/coronariopatia.php";

							}else if($caso == 5){
								$nome_caso = "doencas_vasculares";
								$top_ex = array(8,185);
								$id_exercicios = array(17,18);
								echo "<script type='text/javascript'>$('.nome_caso').html('Doença vascular periférica');</script>";
								$topicos = "../../../includes/indices/doencas_vasculares.php";	
 
							}else if($caso == 6){
								$nome_caso = "insuficiencia_cardiaca";
								$top_ex = array(8,186);
								$id_exercicios = array(19,20);
								echo "<script type='text/javascript'>$('.nome_caso').html('Insuficiência cardíaca');</script>";	
								$topicos = "../../../includes/indices/insuficiencia_cardiaca.php";

							}


						} else if ($unidade == 3) {
							echo "<script type='text/javascript'>$('#avatar').html('<img src=\"../../../imagens/unidade_3/icone_unidade3.png\">');</script>";


							if($caso == 7){
								$nome_caso = "hanseniase";
								$top_ex = array(7,180);
								$id_exercicios = array(21,22,23,24,25,26,27,28,61);
								echo "<script type='text/javascript'>$('.nome_caso').html('Hanseníase');</script>";	
								$topicos = "../../../includes/indices/hanseniase.php";

							}else if($caso == 8){
								$nome_caso = "malaria";
								$top_ex = array(7,193);
								$id_exercicios = array(29,30,31,32);
								echo "<script type='text/javascript'>$('.nome_caso').html('Malária');</script>";
								$topicos = "../../../includes/indices/malaria.php";
 
							}else if($caso == 9){
								$nome_caso = "sida";
								$top_ex = array(7,187);
								$id_exercicios = array(33,34,35,36);
								echo "<script type='text/javascript'>$('.nome_caso').html('Síndrome da Imunodeficiência Adquirida (SIDA) ');</script>";
								$topicos = "../../../includes/indices/sida.php";	

							}else if($caso == 10){
								$nome_caso = "tuberculose";
								$top_ex = array(7,192);
								$id_exercicios = array(38,37, 62, 39,40);
								echo "<script type='text/javascript'>$('.nome_caso').html('Tuberculose');</script>";
								$topicos = "../../../includes/indices/tuberculose.php";	

							}

						} else if ($unidade == 4) {
							echo "<script type='text/javascript'>$('#avatar').html('<img src=\"../../../imagens/unidade_4/icone_unidade4.png\">');</script>";


							if($caso == 11){
								$nome_caso = "itu";
								$top_ex = array(8,191);
								$id_exercicios = array(41,42,43,44);
								echo "<script type='text/javascript'>$('.nome_caso').html('Infecção do trato urinário');</script>";	
								$topicos = "../../../includes/indices/itu.php";

							}

						} else if ($unidade == 5) {
							echo "<script type='text/javascript'>$('#avatar').html('<img src=\"../../../imagens/unidade_5/icone_unidade5.png\">');</script>";



							if($caso == 13){
								$nome_caso = "osteomielite";
								$top_ex = array(8,188);
								$id_exercicios = array(46,47,48);
								echo "<script type='text/javascript'>$('.nome_caso').html('Osteomielite');</script>";	
								$topicos = "../../../includes/indices/osteomielite.php";

							}else if($caso == 12){
								$nome_caso = "pos_ortopedia";
								$top_ex = array(7,197);
								$id_exercicios = array(45);
								echo "<script type='text/javascript'>$('.nome_caso').html('Pós-operatório em ortopedia');</script>";	
								$topicos = "../../../includes/indices/pos_ortopedia.php";

							}

						} else if ($unidade == 6) {
							echo "<script type='text/javascript'>$('#avatar').html('<img src=\"../../../imagens/unidade_6/icone_unidade6.png\">');</script>";


							if($caso == 14){
								$nome_caso = "trans_psiquiatricos";
								$top_ex = array(6,198);
								$id_exercicios = array(49,50);
								echo "<script type='text/javascript'>$('.nome_caso').html('Transtornos psiquiátricos');</script>";	
								$topicos = "../../../includes/indices/trans_psiquiatricos.php";

							}


						} else if ($unidade == 7) {
							echo "<script type='text/javascript'>$('#avatar').html('<img src=\"../../../imagens/unidade_7/icone_unidade7.png\">');</script>";


							if($caso == 15){
								$nome_caso = "alcool";
								$top_ex = array(8,107);
								$id_exercicios = array(51,52,53);
								echo "<script type='text/javascript'>$('.nome_caso').html('Álcool e outras drogas');</script>";	
								$topicos = "../../../includes/indices/alcool.php";

							}

						} else if ($unidade == 8) {
							echo "<script type='text/javascript'>$('#avatar').html('<img src=\"../../../imagens/unidade_8/icone_unidade8.png\">');</script>";


							if($caso == 17){
								$nome_caso = "esclerose_lateral";
								$top_ex = array(8,195);
								$id_exercicios = array(54,55,56);
								echo "<script type='text/javascript'>$('.nome_caso').html('Esclerose lateral amiotrófica');</script>";	
								$topicos = "../../../includes/indices/esclerose_lateral.php";

							}else if($caso == 18){
								$nome_caso = "esclerose_multipla";
								$top_ex = array(7,194);
								$id_exercicios = array(57,58,59,60);
								echo "<script type='text/javascript'>$('.nome_caso').html('Esclerose múltipla');</script>";
								$topicos = "../../../includes/indices/esclerose_multipla.php";	

							}

						}

						return $topicos;
					}

					function gerarExercicios($id,$unidade,&$nome_caso,$caso,&$id_exercicios,&$top_ex){

						echo "<div style='display:none' id='temp'>";
						
						$exercicios = count($id_exercicios);
						$link = "../../../unidades/unidade".$unidade."/".$nome_caso."/index.php?uni=".$unidade."&caso=".$caso."&topico=".$top_ex[0]."&resource=".$top_ex[1];
						
						//CHAMADA PARA VERIFICACAO DOS EXERCICIOS DEVEM SER FEITAS DENTRO DO FOR 
						for ($i=0; $i < $exercicios ; $i++) { 
							echo "<a href='".$link."' title='Exercício ".($i+1)."'>";
							echo " ".verificaExercicio($id, $id_exercicios[$i]);
							echo " </a>";

						 }

						 

						echo "</div>";
						echo "<script>$('#container_ex').html($('#temp').html())</script>";
					}

							

				?>



				<div id="geral">
					<div id="fundo">
							<div id="barra_topo">
								<?php
								include ('../../../includes/menu.php');
								?>
							  
							</div>

					<div id="disciplina">
						<?php //$nome_unidade = $_GET['nome'];
						//$unidade = $_GET['uni'];

						echo "<div id='avatar'>";
						echo "</div>";
						echo "<p class='nome_caso'>";
						echo "</p>";
						?>
						<!-- <p>Asma</p> -->
						<div class="info topo_info"><img src="../../../imagens/info_white.png" title="Título da situação clínica que compõe a unidade didática." alt="Título da situação clínica que compõe a unidade didática."></div>
					</div>
					
					<!-- <div id="usuario">
							  		<p>Olá, <b><span style="margin-right: 8px;">Fulano <?php //echo($USER->firstname . "</span></b>" . "   |  "); ?></p>
					</div> -->

					

			<div id="barra_evolucao">
				<div id="evolucao">
					<p class="descricao">Evolução<img src="../../../imagens/info.png" title="Contabiliza a quantidade de páginas visualizadas. A proposta é lhe dar 
a visão do percentual de conteúdos já acessados." alt="Contabiliza a quantidade de páginas visualizadas. A proposta é lhe dar 
a visão do percentual de conteúdos já acessados."></p>

	


				<!-- Tooltip exercícios-->
				<script type="text/javascript">
					$(document).ready(function(){
					    $("#btn_ex").popover({
					        placement : 'left',
					        html: true,
					         content: function() {
          						return $('#lista_ex').html();
        					}
					    });
					});
				</script>

				<p class="descricao" style="position: absolute;right: 90;">Exercícios <img src="../../../imagens/info.png" title="Acesso aos exerícios." alt="Acesso aos exercícios."></p>

				<div id="caixa_botao">
					<button type="button" id="btn_ex" class="btn">
						<img src="../../../imagens/icn_exer.png">
					</button>
				</div>

				<div id="lista_ex" class="popover fade right in">
					<div class="arrow"></div>
					<div class="popover-content">

						<!--chamar as funcoes-->
						<div id="container_ex"></div>

					</div>
				</div>

				

				<div class="navbar">
			            <div class="progress evolucao">
					
			                 <div class="bar" style="width: <?php porcentagem($caso,$cpf); ?> "></div>
			    </div>
			          
			    <script type="text/javascript">
					$('html').on('mouseup', function(e) {
				    if(!$(e.target).closest('.popover').length) {
				        $('.popover').each(function(){
				            $(this.previousSibling).popover('hide');
				        });
				    }
				});
				</script>
				

			      </div>
				</div>
					
	
					
				</div>

				


			</div> <!--daf-->


			<div id="central">
				
				<div id="conteudo">

				
				<?php

					//TRATAMENTO DE URL. CASO ALGUEM QUEIRA BURLAR A URL O MESMO SERA BARRADO.
					$url = $_SERVER['REQUEST_URI'];
					//echo "url: ".$url."<br>";

					$pos = strpos($url, '&');
					//$pos2 = strpos($url, '?');
					
					if ($pos == true) {
						//echo "existem todos os parametros";
					}else{
						echo "<script> alert('Algo errado. Alguns parametros foram removidos.'); </script>";

					}
				?>
					
				<?php 

 					//conectar();

 					//IMPRIME O CONTEUDO DA UNIDADE
					$resource = $_GET['resource'];
					if (empty($resource)) {
						//echo "sem id do conteudo";
					}else{
						conteudo($resource);	
					}
					

					
				// AQUI VAI FICAR A CHAMADA DA FUNCAO QUE VAI CARREGAR OS
				//CONTEUDOS DAS UNIDADES

					// $id_caso = $_GET['caso'];
					// $topico_caso = $_GET['topico'];
					// $id = 7;	
					// echo "TOP: ".$topico_caso;
					// echo "CASO: ".$id_caso."<br>";													

					//verificarTopico($id, $topico_caso, $id_caso);
					
					?>
				</div>

				<!--Funcao com o botao de apagar toda a evolução do aluno-->
				

				<div id="indice">
					<div id="titulo">
						<h3>Índice
						<?php //session_start('unidades');
						//echo 'valor'. $_SESSION['unidades']; 

						session_start();						
$ids =$_SESSION['unidades']; 
//echo 'aqui'.$_SESSION['unidades'];
//echo "resource".$_GET['resource'];
foreach($ids as $chave => $valor){ 
        //if($valor != '')
        if( $valor == $_GET['resource']){  
            
            echo 'indice: '.$chave.' valor: '.$valor;
        }
    }?>
						</h3>
						<div class="info"><img src="../../../imagens/info.png" title="Acesso ao(s) conteúdos(s) disponível(is) nesta unidade didática." alt="Acesso ao(s) conteúdos(s) disponível(is) nesta unidade didática.">
						</div>
					</div>
					<ul>
					<?php
					conectar();

					//echo "u:".$id;
					include (avatar_unidade($unidade, $caso, $id_exercicios, $top_ex, $nome_caso));
					gerarExercicios($id,$unidade,$nome_caso, $caso,$id_exercicios,$top_ex);

					$id = $USER->id;
					
					$id_caso = $_GET['caso'];
					$caso = $_GET['caso'];
					$topico_caso = $_GET['topico'];
													
					//verificarTopico($id, $topico_caso, $id_caso);
					
					if ($_GET['caso']) {
						$id = $USER->id;
						$id_caso = $_GET['caso'];
						$topico_caso = $_GET['topico'];
					//	cadastrarTopico($id, $id_caso, $topico_caso);
					}
					

					
					?>
					</ul>

					<div id="apagar_tudo">
						<div style="display:none;">
						<div id="texto_color" >

							<h4>Confirmação</h4> <br> <b> <?php echo $USER->firstname.', ';?></b>você tem certeza que deseja recomeçar os estudos desta unidade? Se confirmar a opção, todos os registros de navegação pelas páginas desta unidade serão eliminados e você não saberá quais páginas já visitou.
							<br><br><?php echo '<a class="btn inline" style="float:right;" href="#texto_color" onClick="apagar_confirmar('.$caso.');">Confirmar</a>'; ?>


						</div></div>

					<a class="btn-block btn btn-info btn_continuar" href="">Continuar os estudos  <img src="../../../imagens/info_white.png" title="Clicando aqui você dá continuidade aos estudos a partir da ultima página visitada nesta situação clínica." alt="Unidade didática."></a>
					<?php //ultimoAcesso($id, 2); ?>
					<a class="btn-block btn btn-danger btn_recomecar inline" href="#texto_color" onClick="apagar();">Recomeçar os estudos <img src="../../../imagens/info_white.png" title="Clicando aqui você limpa o histórico de navegação desta situação clínica." alt="Unidade didática."></a>
				


					<script type="text/javascript">
						function apagar(){															
						//$("#texto_color").html("");
						$(".inline").colorbox({inline:true, width:"50%",onClosed:function(){}});
					}


					function apagar_confirmar(num){
						//alert("caso " + num);
						$.post( "../../../functions/apagar_tudo.php", { caso: num }, function() {
							
						location.reload();
						});

					}
					</script>

					<?php 
						$caso = $_GET['caso'];
						//deletarEvolucao($id, $caso);
					?>
						
					
					</div>
				</div>

			</div>


		</div>

		

		<!-- Modal: html do modal só aparece nessa página. O texto que será exibido no feedback das questões
	deve ser colocado nas divs feedback + name de cada questão, dentro do seu arquivo.
 -->
<!-- <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      	<h3 class="titulo_ex">Exercício</h3>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <br>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div> -->

<!-- <div class="modal fade" id="modalEx" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      	<h3 class="titulo_ex">Exercício</h3>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <br>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
 -->

	</body>

	</html>
