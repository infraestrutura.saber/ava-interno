<!DOCTYPE html!>
<html lang="pt-br">
	<meta charset="utf-8">
	<head>
		
		<link rel="stylesheet" type="text/css" href="../css/apresentacao.css">
		<script type="text/javascript" src="../lib/jquery.js"></script>
		<script type="text/javascript" src="../lib/cycle.js"></script>
		<script type="text/javascript" src="../scripts/apresentacao.js"></script>
		<script type="text/javascript" src="../lib/bootstrap/js/bootstrap.js"></script>
		<script type="text/javascript" src="../lib/colorbox/jquery.colorbox.js"></script>
		<script type="text/javascript" src="../lib/jqfloat.js"></script>
		<link rel="stylesheet" type="text/css" href="../lib/colorbox/colorbox.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap-responsive.css">
	</head>

	<body>
		<div id="barra_topo">
		<div id="menu">
			<?php 

				$caso = $_GET['caso'];
			
			 require_once("../../../config.php");
			 $id=$USER->id;
			 $nome_user=$USER->firstname;
			 $cpf=$USER->username;

			// echo "<script> alert($id); </script>";
			//echo "<script> alert('$nome_user'); </script>";
			// echo "<script> alert('$cpf'); </script>";


			require ("../includes/menu.php");
			require ('../functions/conection.php');
			conectar();

			require_once ("../functions/functions.php"); 


			require_once ("../modulos/gamefication/functions/startGamefication.php");
			// include '../functions/functions.php'; ERRO NESTA CHAMADA

			startGamefication($id); 
			validar_usuario($nome_user,$cpf);
			
			//verificarProva($id);  //funcao que insere a nota final (finalgrade) do aluno

			//ESTA FUNCAO APRESENTA A NOTA DO ALUNO NORMALIZADA..
			verificarProva($tbGrades,$itemId,$cursoId,$tbItens,$tbUser,$tbQuizAtt,$tentativa,$cursoId,$userid,$tbCourse,$qntQuestoesProva);
			

			?>
			<script type="text/javascript">

				//document.getElementById("icone_home").src="../imagens/home.png";
				//document.getElementById("icone_sair").src="../imagens/sair.png";

							$(document).ready(function() {
								$("#menu li a").mouseover(function() {
									var index = $("#menu li a").index(this);
									$("#menu li").eq(index).children("ul").slideDown(100);
									if ($(this).siblings('ul').size() > 0) {
										return false;
									}
								});
								$("#menu li").mouseleave(function() {
									var index = $("#menu li").index(this);
									$("#menu li").eq(index).children("ul").slideUp(100);
								});
							});

							
		</script>

		</div></div>
		<div id="ceu">
			 
				
				
		</div>
		<div id="background_cidade"></div>
		<div id="pagina">
				 <div id="nuvem" style="/* left: 511.41419999999994px; */">
					<img src="../imagens/apresentacao/nuvem2.png" alt="tour">
				</div>
				<div id="nuvem2" style="/* left: 815.078px; */">
					<img src="../imagens/apresentacao/nuvem2.png" alt="tour" style="width: 65%;">
				</div>
				<div id="nuvem3" style="/* left: 815.078px; */">
					<img src="../imagens/apresentacao/nuvem2.png" alt="tour">
				</div>
				<div id="nuvem4" >
					<img src="../imagens/apresentacao/nuvem2.png" alt="tour" style="width: 66%;">
				</div>  
			<div id="aviao">
					<!-- <img src="../imagens/apresentacao/tour_aviao.png" alt="tour"> -->
					<center><img style="margin-left: 4px;" src="../imagens/apresentacao/Balao.png">
					<a class="inline" href="#introducao" onclick="abreModalIntro();"><img src="../imagens/apresentacao/faixa-1.png"></a>
					<img src="../imagens/apresentacao/faixa-2.png"></center>
				</div>

				
			
			<div style="display: none;"> <div id="introducao" ><h4>Apresentação</h4>A Atenção Domiciliar é uma modalidade de cuidado que visualiza o domicílio como mais um 

espaço terapêutico que amplia o conceito de atendimento e cuidado de situações clínicas, 

por meio de uma atenção humanizada que preserva os vínculos familiares e traz conforto ao 

paciente. Este módulo pretende diferenciar as principais situações clínicas comuns em adultos, 

considerando a importância da avaliação e diagnóstico, além de reconhecer o cuidador como 

parte fundamental da equipe de cuidado.</div></div>
			<div id="passaro">
						
			</div>

			<script type="text/javascript">

					if(localStorage.getItem("block_balao") != "on"){
										$("#passaro").html('<img id="fechar_balao" src="../imagens/sair.png"><img src="../imagens/apresentacao/indicador.png">');
					}
			

			</script>

			<div id="cenario">
				<img src="../imagens/apresentacao/base.png" alt="cenário" />

				<div id="grupo_geral">
					<div id="ps1"> 
						<a class="inline" href="#modal_unidade"  onclick="abreModalUnidade('cenario');">
							<img src="../imagens/apresentacao/Personagem/p1.png" onMouseOver="this.src='../imagens/apresentacao/Personagem/silhuetas/ps1.png'" onMouseOut="this.src='../imagens/apresentacao/Personagem/p1.png'" alt="cenário" /></a>

					</div>
					<div id="ps2" data-toggle="modal" >	
						<a class="inline" href="#modal_unidade" onclick="abreModalUnidade('cenario');">
							<img src="../imagens/apresentacao/Personagem/p2.png" onMouseOver="this.src='../imagens/apresentacao/Personagem/silhuetas/ps2.png'" onMouseOut="this.src='../imagens/apresentacao/Personagem/p2.png'" alt="cenário" /></a>
					</div>
					<div id="ps3"> 
						<a class="inline" href="#modal_unidade" onclick="abreModalUnidade('cenario');">
							<img src="../imagens/apresentacao/Personagem/p3.png" onMouseOver="this.src='../imagens/apresentacao/Personagem/silhuetas/ps3.png'" onMouseOut="this.src='../imagens/apresentacao/Personagem/p3.png'" alt="cenário" /></a>
					</div>
					<div id="ps4"> 
						<a class="inline" href="#modal_unidade" onclick="abreModalUnidade('cenario');">
							<img src="../imagens/apresentacao/Personagem/p4.png" onMouseOver="this.src='../imagens/apresentacao/Personagem/silhuetas/ps4.png'" onMouseOut="this.src='../imagens/apresentacao/Personagem/p4.png'" alt="cenário" /></a>

					</div>
					<div id="ps5"> 
						<a class="inline" href="#modal_unidade" onclick="abreModalUnidade('cenario');">
							<img src="../imagens/apresentacao/Personagem/p5.png" onMouseOver="this.src='../imagens/apresentacao/Personagem/silhuetas/ps5.png'" onMouseOut="this.src='../imagens/apresentacao/Personagem/p5.png'" alt="cenário" /></a>

					</div>
					<div id="ps6"> 
						<a class="inline" href="#modal_unidade" onclick="abreModalUnidade('cenario');">
							<img src="../imagens/apresentacao/Personagem/p6.png" onMouseOver="this.src='../imagens/apresentacao/Personagem/silhuetas/ps6.png'" onMouseOut="this.src='../imagens/apresentacao/Personagem/p6.png'" alt="cenário" /></a>

					</div>
					<div id="ps7"> 
						<a class="inline" href="#modal_unidade" onclick="abreModalUnidade('cenario');">
							<img src="../imagens/apresentacao/Personagem/p7.png" onMouseOver="this.src='../imagens/apresentacao/Personagem/silhuetas/ps7.png'" onMouseOut="this.src='../imagens/apresentacao/Personagem/p7.png'" alt="cenário" /></a>

					</div>
					<div id="ps8"> 
						<a class="inline" href="#modal_unidade" onclick="abreModalUnidade('cenario');">
							<img src="../imagens/apresentacao/Personagem/p8.png" onMouseOver="this.src='../imagens/apresentacao/Personagem/silhuetas/ps8.png'" onMouseOut="this.src='../imagens/apresentacao/Personagem/p8.png'" alt="cenário" /></a>

					</div>
					
					<div id="ps9"> 
						<a class="inline" href="#modal_unidade" onclick="abreModalUnidade('cenario');">
							<img src="../imagens/apresentacao/Personagem/p9.png" onMouseOver="this.src='../imagens/apresentacao/Personagem/silhuetas/ps9.png'" onMouseOut="this.src='../imagens/apresentacao/Personagem/p9.png'" alt="cenário" /></a>

					</div>
					
					<div id="ps10"> 
						<a class="inline" href="#modal_unidade" onclick="abreModalUnidade('cenario');">
							<img src="../imagens/apresentacao/Personagem/p10.png" onMouseOver="this.src='../imagens/apresentacao/Personagem/silhuetas/ps10.png'" onMouseOut="this.src='../imagens/apresentacao/Personagem/p10.png'" alt="cenário" /></a>

					</div>
					
					<div id="ps11"> 
						<a class="inline" href="#modal_unidade" onclick="abreModalUnidade('cenario');">
							<img src="../imagens/apresentacao/Personagem/p11.png" onMouseOver="this.src='../imagens/apresentacao/Personagem/silhuetas/ps11.png'" onMouseOut="this.src='../imagens/apresentacao/Personagem/p11.png'" alt="cenário" /></a>

					</div>
					
					<!-- <div id="ps12"> 
						<a class="inline" href="#modal_unidade" onclick="abreModalUnidade('cenario');">
							<img src="../imagens/apresentacao/Personagem/p12.png" onMouseOver="this.src='../imagens/apresentacao/Personagem/silhuetas/ps12.png'" onMouseOut="this.src='../imagens/apresentacao/Personagem/p12.png'" alt="cenário" /></a>

					</div> -->
					
					<div id="ps13"> 
						<a class="inline" href="#modal_unidade" onclick="abreModalUnidade('cenario');">
							<img src="../imagens/apresentacao/Personagem/p13.png" onMouseOver="this.src='../imagens/apresentacao/Personagem/silhuetas/ps13.png'" onMouseOut="this.src='../imagens/apresentacao/Personagem/p13.png'" alt="cenário" /></a>

					</div>
					
					<div id="ps14"> 
						<a class="inline" href="#modal_unidade" onclick="abreModalUnidade('cenario');">
							<img src="../imagens/apresentacao/Personagem/p14.png" onMouseOver="this.src='../imagens/apresentacao/Personagem/silhuetas/ps14.png'" onMouseOut="this.src='../imagens/apresentacao/Personagem/p14.png'" alt="cenário" /></a>

					</div>
					
					<div id="ps15"> 
						<a class="inline" href="#modal_unidade" onclick="abreModalUnidade('cenario');">
							<img src="../imagens/apresentacao/Personagem/p15.png" onMouseOver="this.src='../imagens/apresentacao/Personagem/silhuetas/ps15.png'" onMouseOut="this.src='../imagens/apresentacao/Personagem/p15.png'" alt="cenário" /></a>

					</div>
					
					<div id="ps16"> 
						<a class="inline" href="#modal_unidade" onclick="abreModalUnidade('cenario');">
							<img src="../imagens/apresentacao/Personagem/p16.png" onMouseOver="this.src='../imagens/apresentacao/Personagem/silhuetas/ps16.png'" onMouseOut="this.src='../imagens/apresentacao/Personagem/p16.png'" alt="cenário" /></a>

					</div>
					
					<div id="ps17"> 
						<a class="inline" href="#modal_unidade" onclick="abreModalUnidade('cenario');">
							<img src="../imagens/apresentacao/Personagem/p17.png" onMouseOver="this.src='../imagens/apresentacao/Personagem/silhuetas/ps17.png'" onMouseOut="this.src='../imagens/apresentacao/Personagem/p17.png'" alt="cenário" /></a>

					</div>
					
					<div id="ps18"> 
						<a class="inline" href="#modal_unidade" onclick="abreModalUnidade('cenario');">
							<img src="../imagens/apresentacao/Personagem/p18.png" onMouseOver="this.src='../imagens/apresentacao/Personagem/silhuetas/ps18.png'" onMouseOut="this.src='../imagens/apresentacao/Personagem/p18.png'" alt="cenário" /></a>

					</div>
					
					
					
				</div>
			</div>
			 
			<?php include ("../includes/modal_unidades.php"); ?>

		<div id="marcas">
			<center><!--<img src="../imagens/apresentacao/rodape.png
				">--></center>
		</div>

		</div>

		

		</div>

<script type="text/javascript">
	function abreModalIntro () {
						$(".inline").colorbox({inline:true, width:"650px"});
					}
</script>


<script type="text/javascript">
$(document).ready(function() {


		var animateTruck = function () {

    $('#nuvem').css('left','0px')
               .animate({'left':$(window).width()},
                        50000,
                        'linear',
                        function(){
                            animateTruck();
                            animateTruck2();
                        });
};

var animateTruck2 = function () {

    $('#nuvem2').css('left','1280px')
               .animate({"left":"-100px"},
                        60000,
                        'linear',
                        function(){
                            animateTruck();
                            animateTruck2();
                        });
};

// animateTruck();
// animateTruck2();
});


$(document).ready(function() {
	function antiGrav(ele) { 
	var distance = 7;
	$(ele).animate({
		'top': "+=" + distance + "px"
	},1500,"swing",function(){
		$(ele).animate({				
            'top': "-=" + distance + "px"
		},1300,"swing",function(){
			antiGrav(ele);
        });
	});
}


    antiGrav('#aviao'); 

});
</script>

	</body>
</html>
