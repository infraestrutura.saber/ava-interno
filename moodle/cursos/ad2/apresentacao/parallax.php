<!DOCTYPE html>

<head>

	<meta charset="utf-8" />
	<title>jInvertScroll Demo Page</title>
    <link rel="stylesheet" href="parallax/css/example.css" />
    <link rel="stylesheet" type="text/css" href="../lib/colorbox/colorbox.css">
    <link rel="stylesheet" type="text/css" href="../css/apresentacao.css">
    <link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap-responsive.css">


     <script type="text/javascript" src="parallax/libs/jquery.min.js"></script>
    <script type="text/javascript" src="parallax/libs/jquery.jInvertScroll.js"></script>
    <script type="text/javascript" src="../lib/colorbox/jquery.colorbox.js"></script>
	<script type="text/javascript" src="../scripts/apresentacao.js"></script>
    
</head>
<body>
    <h1></h1>

    <?php
         require_once("../../../config.php");
			 $id=$USER->id;
			 $nome_user=$USER->firstname;
			 $cpf=$USER->username;

			// echo "<script> alert($id); </script>";
			//echo "<script> alert('$nome_user'); </script>";
			// echo "<script> alert('$cpf'); </script>";


			
			require ('../functions/conection.php');
			conectar();

			require_once ("../functions/functions.php"); 
			include ("../includes/modal_unidades.php");
    
    
    ?>


<div style="position: fixed;z-index: 5000; top: 73px;left: 14px;">
	<p style="float: right;font-size: 10px;color: #2c2c2c;line-height:14px;margin-left: 5px;">Este site é melhor visualizado<br> no Google Chrome</p>
	<img src="parallax/images/chrome.svg" style="width: 27px;height: 27px;float: right;">
	</div>

    <div id="painel_curso"> 
	<div class="titulo">Abordagem domiciliar de situações clínicas comuns em adultos</div>
	
	<div class="descricao">A atenção domiciliar é uma modalidade de cuidado que visualiza o domicílio
como mais um espaço terapêutico, ampliando o conceito e as possibilidades
de atendimento às situações clínicas. Deve-se primar pela atenção
humanizada e pela preservação dos vínculos familiares, pois isso traz
conforto ao paciente.<br><br>
Este módulo apresenta as principais situações clínicas comuns em adultos,
descrevendo o manejo dos pacientes na atenção domiciliar e reforçando o
papel do cuidador como parte fundamental da equipe de cuidado.</div>
  <br><a id="acesso_link" href="https://www.youtube.com/embed/liw-lvmPw-Q" title="Guia de uso"><button style="height: 31px;width: 170px;margin-left: 0px; float: left;" class="btn btn-inverse" id="acesso">Guia de uso</button></a>  </div>

	<script>
	$(document).ready(function(){
	 $("#acesso_link").colorbox({iframe:true, innerWidth:1000, innerHeight:563});
	 $("#cboxClose").hide();
	});
	</script>

    <div id="miniaturas">
        <ul> 

<div id="ps3"> 
                <a class="inline" href="#modal_unidade" onclick="abreModal();">
            <li title="Abordagem domiciliar ao paciente com insuficiência cardíaca"><img src="parallax/images/miniaturas/14.svg"></li>
</a></div>

<div id="ps2"> 
                <a class="inline" href="#modal_unidade" onclick="abreModal();">
            <li title="Abordagem ao paciente com doença vascular periférica no domicílio"><img src="parallax/images/miniaturas/8.svg"></li>
</a></div>

 <div id="ps1"> 
                <a class="inline" href="#modal_unidade" onclick="abreModal();">
            <li title="Abordagem ao paciente com cardiopatia isquêmica crônica no domicílio"><img src="parallax/images/miniaturas/2.svg"></li>
</a></div>

<div id="ps5"> 
                <a class="inline" target="_parent" href="#modal_unidade" onclick="abreModal();">
                     <li title="Abordagem ao paciente com pneumonia no domicílio" style="margin-right: 29px;"><img src="parallax/images/miniaturas/1.svg"></li>
                </a>
            </div>

<div id="ps7"> 
                <a class="inline" href="#modal_unidade" onclick="abreModal();">
            <li title="Abordagem ao paciente com Doença Pulmonar Obstrutiva Crônica no domicílio" >
		<img src="parallax/images/miniaturas/7.svg"></li>
</a></div>


<div id="ps6"> 
                <a class="inline" href="#modal_unidade" onclick="abreModal();">
            <li title="Abordagem ao paciente com asma no domicílio"><img src="parallax/images/miniaturas/13.svg"></li>
</a></div>

<!--Linha 2 -->

<div id="ps8"> 
                <a class="inline" href="#modal_unidade" onclick="abreModal();">
            <li title="Abordagem ao paciente com transtornos psiquiátricos no domicílio"><img src="parallax/images/miniaturas/6.svg"></li>
</a></div>

 


 <div id="ps18"> 
                <a class="inline" href="#modal_unidade" onclick="abreModal();">

           <li title="Abordagem ao paciente com tuberculose no domicílio" style="margin-right: 17px;">
		<img src="parallax/images/miniaturas/4.svg"></li>
</a></div>


<div id="ps17"> 
                <a class="inline" href="#modal_unidade" onclick="abreModal();">
            <li title="Abordagem ao paciente com malária"><img src="parallax/images/miniaturas/9.svg"></li>

</a></div>


<div id="ps15"> 
                <a class="inline" href="#modal_unidade" onclick="abreModal();">
            <li title="Abordagem domiciliar ao paciente com Síndrome da Imunodeficiência Adquirida (SIDA)"><img src="parallax/images/miniaturas/10.svg"></li>
</a></div>


<div id="ps16"> 
                <a class="inline" href="#modal_unidade" onclick="abreModal();">
            <li title="Abordagem ao paciente com hanseníase no domicílio"><img src="parallax/images/miniaturas/15.svg"></li>
</a></div>

<div id="ps4"> 
                <a class="inline" target="_parent" href="#modal_unidade" onclick="abreModal();">
            <li title="Abordagem ao paciente com infecção do trato urinário no domicílio" style="margin-right: 15px;"><img src="parallax/images/miniaturas/3.svg"></li>

</a></div>



<!-- Linha 3 -->

<div id="ps14"> 
                <a class="inline" href="#modal_unidade" onclick="abreModal();">
            <li title="Abordagem ao paciente com Esclerose Lateral Amiotrófica no domicílio"><img src="parallax/images/miniaturas/11.svg"></li>
</a></div>

<div id="ps13"> 
                <a class="inline" href="#modal_unidade" onclick="abreModal();">
            <li title="Abordagem ao paciente com Esclerose Múltipla"><img src="parallax/images/miniaturas/17.svg"></li>
</a></div>



<div id="ps11"> 
                <a class="inline" href="#modal_unidade" onclick="abreModal();">
           <li title="
Abordagem ao paciente usuário de álcool e crack no domicílio" style="margin-right: 26px;">
		<img src="parallax/images/miniaturas/18.svg"><img src="parallax/images/miniaturas/12.svg"></li>
</a></div>


<div id="ps10"> 
                <a class="inline" href="#modal_unidade" onclick="abreModal();">
           <li title="Abordagem ao paciente pós-operatório de problemas ortopédicos no domicílio" style="margin-right: 33px;">
		<img src="parallax/images/miniaturas/5.svg"></li>
</a></div>

<div id="ps9"> 
                <a class="inline" href="#modal_unidade" onclick="abreModal();">
            <li title="Abordagem ao paciente com osteomielite no domicílio" ><img src="parallax/images/miniaturas/16.svg"></li>
</a></div>




<!--<div id="ps11"> 
                <a class="inline" target="_parent" href="#modal_unidade" onclick="abreModal();">
            <li><img src="parallax/images/miniaturas/12.svg"></li>
</a></div>-->











        </ul>



    </div>

   <div id="controles">
        <div id="forward" title="Avançar" class="botao_controle"><img src="parallax/images/botoes/next.svg" width="30px"> </div>
       	<div id="play" title="Iniciar" class="botao_controle"><img src="parallax/images/botoes/play.svg" width="30px"></div>
        <div id="pause" title="Pausar e prossguir manualmente" class="botao_controle"><img src="parallax/images/botoes/pause.svg" width="30px"></div>
        <div id="rewind" title="Voltar" class="botao_controle"><img src="parallax/images/botoes/previous.svg" width="30px"></div>
    </div>
    
    <div class="horizon scroll">
        <img width="5999px"  src="parallax/images/horizon.svg" alt="" />
    </div>
    <div class="middle scroll">
        <img width="6290px" src="parallax/images/middle.svg" alt="" />
    </div>
    <div class="front scroll">
        <img style="margin-left: -24px;" width="12492px" src="parallax/images/front.svg" alt="" />
    </div>
    
   
    <script type="text/javascript">
    jQuery.fx.interval = 1;
   


	$('#iframe_home').contents().find('#painel_curso').fadeOut();

        function abreModal() {
		
	//$('#iframe_home').contents().find('.topo_modal').css('cssText', 'background-color: #e8d09d !important');

            $(".inline").colorbox({
                inline : true,
                width : "1050px"
            });

            if (localStorage.getItem('modal_block') == 'on') {
                if (document.getElementById('check_modal') != null)
                    document.getElementById('check_modal').checked = true;
            } else {
                if (document.getElementById('check_modal') != null)
                    document.getElementById('check_modal').checked = false;
            }
        }



        $("#play").click(function() {

            $("#play").hide();
            $("#pause").css('background','#800080').show();


           
            animation2();
                


        });   

         $("#pause").click(function() {

            $("#pause").hide();
            $("#play").show();

            $('html,body').stop();

           
             
        });

        $("#rewind").click(function(){
            $('html,body').stop();
            $("#miniaturas").fadeOut(100); 
            $('html,body').animate({ scrollTop: 1 }, 1000, function(){
            $("#pause").hide();
            $("#play").show();
            $('html,body').stop();
        });
         });

         $("#forward").click(function(){
            $('html,body').stop();
            $('html,body').animate({ scrollTop: 8950 }, 1000, function(){
            $("#play").show();
            $("#play").css('cursor') == 'default';
            $("#pause").hide();
            $("#miniaturas").fadeIn();
            });
          });
             
      
    $(window).on('scroll', function() {
		
	  
           var y = document.body.scrollTop;
           //console.log (y);

            
                    $('#iframe_home').contents().find('#painel_curso').hide();


                    if($('#miniaturas').css('display') == 'none'){ 

                             if( y > 8450){
                             //  $("#miniaturas").fadeIn();
                              
                             }

                    } else { 

                             if( y < 8450){
                             //   $("#miniaturas").fadeOut();
                                
                            }
                   
                    }


		     if($('#painel_curso').css('display') != 'block'){ 


                             if( y < 10){
                               //$("#painel_curso").fadeIn();
				//$('#iframe_home').contents().find('#painel_curso').fadeIn();
                              
                             }

                    } else { 

                             if( y > 100){
			//	 $("#painel_curso").fadeOut();
			//	$('#iframe_home').contents().find('#painel_curso').fadeIn();
                                
                            }
                   
                    }

                      
    });
    
    function animation2(){

        var y= document.body.scrollTop;
        $('html,body').animate({ scrollTop: 8950 }, 77400,'linear', function(){
                  $("#pause").css('background','#333333').show();
		 $("#play").show();
           	 $("#play").css('cursor') == 'default';
           	 $("#pause").hide();
		 $("#miniaturas").fadeIn();
        });   


    }


    function animation(){

    var y= document.body.scrollTop;
       
        if(y==0){
            $('html,body').animate({ scrollTop: 8950 }, 47400, function(){
                 $("#miniaturas").fadeIn();
            });   

        }
    }

    //setInterval(function(){animation()}, 3000);
   
    (function($) {
        var elem = $.jInvertScroll(['.scroll'],        // an array containing the selector(s) for the elements you want to animate
            {
                         // optional: define the height the user can scroll, otherwise the overall length will be taken as scrollable height
            onScroll: function(percent) {   //optional: callback function that will be called when the user scrolls down, useful for animating other things on the page
              
            }
        });

        $(window).resize(function() {
          if ($(window).width() <= 768) {
            elem.destroy();
          }
          else {
            elem.reinitialize();
          }
        });
    }(jQuery));

    </script>

</body>
</html>
