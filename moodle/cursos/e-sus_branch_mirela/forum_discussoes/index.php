﻿<!DOCTYPE html!>
<meta charset="utf-8">
<html lang="pt-br">
<head>

		
		<!--<link rel="stylesheet" type="text/css" href="../apresentacao/css/style.css">-->
		<link rel="stylesheet" type="text/css" href="../css/style.css">
		
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap-responsive.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap-responsive.min.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="forum/css/style.css">

		<script type="text/javascript" src="../lib/bootstrap/js/bootstrap.js"></script>
		<script type="text/javascript" src="../lib/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="../lib/bootstrap/js/bootstrap-dropdown.js"></script>
		<script type="text/javascript" src="../lib/bootstrap/js/jquery-1.10.1.min.js"></script>
		<script type="text/javascript" src="../lib/jquery.js"></script>
		<link rel="stylesheet" type="text/css" href="../lib/colorbox/colorbox.css"> 


		<script type="text/javascript" src="../lib/colorbox/jquery.colorbox.js"></script>

		<script type="text/javascript" src="forum/ckeditor/ckeditor.js"></script> 

		<title>Fórum</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>




	
			<body>
						<script type="text/javascript">
			$(document).ready(function(){
			 $("#menu li a").mouseover(function(){
			 var index = $("#menu li a").index(this);
			 $("#menu li").eq(index).children("ul").slideDown(100);
			 if($(this).siblings('ul').size() > 0){
			 return false;
			 }
			 });
			 $("#menu li").mouseleave(function(){
			 var index = $("#menu li").index(this);
			 $("#menu li").eq(index).children("ul").slideUp(100);
			 });
			});


		</script>
				<?php 
				//dando um 'require_once' eu preciso escapar os niveis da mesma forma como
				//eh feito em hmtl, pois o php entende que o arquivo em questao nao esta na raiz.
				require_once('../../../config.php');
				require_once('../functions/conection.php'); 
				require_once('../functions/funcoes.php');

				
				$id=$USER->id;
				$username=$USER->username;

				//redirecionar caso nao tenha permissao
				if($id==""){
					header('Location: http://ufpe.unasus.gov.br/testes/homl_moodle_unasus/cursos/ad2/index.php'); //redireciona se o usuario nao tiver permissão
					//header('Location: http://ufpe.unasus.gov.br/adteste/login/index.php'); //redirencionar
					
				}

				if(empty($_GET['pagina'])){
					//se variavél pagina não existir 
					$temp = '0';			

				}

				if ($_GET['pagina']==1){ 
					$temp = 1;

					if($_GET['id'] > 0){
						$id_user = $_GET['id'];
					}
					
				}

				if ($_GET['pagina']==2){ 
					$temp = 2;

					if($_GET['id'] > 0){
						$id_user = $_GET['id'];
					}
					
				}

				if ($_GET['pagina']==3){ 
					$temp = 3;

					if($_GET['id'] > 0){
						$id_user = $_GET['id'];
					}
					
				}


				
				?>
				<div id="geral">
					<div id="fundo">
							<div id="barra_topo">
							
								<?php
										include('../includes/menu.php'); 

								?>
					</div>

					<div id="disciplina">
						<p>Comunidade de Práticas</p>
						<div class="info"><img src="../imagens/info.png" title="Fórum" alt="Fórum"></div>
					</div>
					
					<div id="usuario">
							  		<p>Olá, <b><span style="margin-right: 8px;"><?php echo ($USER->firstname ."</span></b>"."   |  "); ?></p>  
					</div>
			<div id="central">		
				<div id="barra_total">
						<?php
							if($temp==0){
								include('0.php'); 
							}else if($temp == '1'){
								include('forum_discussoes.php'); 
							}else if($temp == '2'){
								include('insertLike.php'); 
							}else if($temp == '3'){
								include('insertLikeAnswer.php'); 
							}
						?>
		
						
				</div>
			</div>

			</div> 

		</div>

	</body>

	</html>