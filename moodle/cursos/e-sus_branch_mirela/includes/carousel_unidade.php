<html>
<head>

	<meta charset="utf-8" />
	<!-- <html lang="pt-br" manifest="unasus_ufpe.manifest"> -->
	
	<link rel="stylesheet" type="text/css" href="../lib/rs_carousel/main/css/demo.css" media="all" />
	<link rel="stylesheet" type="text/css" href="../lib/rs_carousel/main/css/base.css" media="all" />
	<link rel="stylesheet" type="text/css" href="../lib/rs_carousel/dist/css/jquery.rs.carousel.css" media="all" />
	
	<!--Chamada da lib LABJS independente-->
	<script type="text/javascript" src="../lib/LAB.js"></script>

	


</head>
<body class="no-js">
	<div id="carousel">
		
		
		<form class="module" >
			
			<div class="actions" style="display: none;">
				<ul class="action-set base">
					<li class="action">
						<input type="button" id="init" name="init" value="init" />
					</li>
					<li class="action">
						<input type="button" id="destroy" name="destroy" value="destroy" />
					</li>
				</ul>
			</div>
		</form>


		<?php

			$view = $_GET['view'];
			$topico = $_GET['topico'];
			$id = $_GET['id_user'];

			
			include '../functions/conectar.php';
			conectar ();
			include '../functions/functions.php';


			if($view == 'unidade1'){
				$topico = $_GET['topico'];
				$uni = 1;
		?>
		<div class="testeTop" style="display:none"><?php echo $topico; ?></div>
		<div id="rs-carousel" class="module bg" style="display:none;">
		<!--	<img onclick="anterior(0);" class="seta_left" src="../imagens/conteudo/set_esq_u1.png">
			<img onclick="proximo(0);" class="seta_right" src="../imagens/conteudo/set_dir_u1.png">-->
			<ul>
				<li class="color-2">
					<a title="Apresentação" href="index.php?view=unidade1&topico=0&resource=208">1</a></li>
				<li class="color-2">
					<a title="Entenda o que é o SISAB e o e-SUS" href="index.php?view=unidade1&topico=1&resource=209">2</a></li>
				<li class="color-2">
					<a title="SIAB e SISAB" href="index.php?view=unidade1&topico=2&resource=210">3</a></li>
				<li class="color-2">
					<a title="Usuários do sistema" href="index.php?view=unidade1&topico=3&resource=211">4</a></li>
				<li class="color-2">
					<a title="Como implantar o e-SUS AB no município?" href="index.php?view=unidade1&topico=4&resource=212">5</a></li>
				<li class="color-2">
					<a title="Cenários de implantação do e-SUS AB" href="index.php?view=unidade1&topico=5&resource=213">6</a></li>
				<li class="color-2">
					<a title="O que é CDS e PEC?" href="index.php?view=unidade1&topico=6&resource=214">7</a></li>
				<li class="color-2">
					<a title="Suporte técnico" href="index.php?view=unidade1&topico=7&resource=215">8</a></li>
				<li class="color-2">
					<a title="Exercícios" href="index.php?view=unidade1&topico=8&exercicio=1">9</a></li>
				<li class="color-2">
					<a title="Referências" href="index.php?view=unidade1&topico=9&resource=217">10</a></li>
			</ul>
		</div>

			<div id="getTop0" style="display:none;"><?php verificarTopico($id, 0, $uni) ?></div>
			<div id="getTop1" style="display:none;"><?php verificarTopico($id, 1, $uni) ?></div>
			<div id="getTop2" style="display:none;"><?php verificarTopico($id, 2, $uni) ?></div>
			<div id="getTop3" style="display:none;"><?php verificarTopico($id, 3, $uni) ?></div>
			<div id="getTop4" style="display:none;"><?php verificarTopico($id, 4, $uni) ?></div>
			<div id="getTop5" style="display:none;"><?php verificarTopico($id, 5, $uni) ?></div>
			<div id="getTop6" style="display:none;"><?php verificarTopico($id, 6, $uni) ?></div>
			<div id="getTop7" style="display:none;"><?php verificarTopico($id, 7, $uni) ?></div>
			<div id="getTop8" style="display:none;"><?php verificarTopico($id, 8, $uni) ?></div>
			<div id="getTop9" style="display:none;"><?php verificarTopico($id, 9, $uni) ?></div>

			<script>

				var topico = $(".testeTop").html();
				$( "#rs-carousel ul li a:eq("+ topico +")" ).css( "border-bottom", "3px solid #8d55a1" );

			</script>

		<?php

				for ($i=0; $i <10 ; $i++) { 
					//echo "<div id='getTop' style='display:none;'>".verificarTopico($id, $i, $uni)."</div>";
					//echo "<script>alert($('#getTop').html(););</script>";
					echo "<script>
						var a = document.getElementById('getTop".$i."').innerHTML;
						if(a != ''){
							$( '#rs-carousel ul li:eq(".$i.")' ).css( 'background-color', 'd1bbd9', 'important' );
						}

					</script>";
					
				}

			}else if($view == 'unidade2'){
					$uni = 2;
		?>

		<div class="testeTop" style="display:none"><?php echo $topico; ?></div>
		<div id="rs-carousel" class="module bg" style="display:none;">
			<ul>
				<li class="color-2">
					<a title="Apresentação" href="index.php?view=unidade2&topico=0&resource=218">1</a></li>
				<li class="color-2">
					<a title="O que é o CDS?" href="index.php?view=unidade2&topico=1&resource=219">2</a></li>
				<li class="color-2">
					<a title="Cenários de implantação" href="index.php?view=unidade2&topico=2&resource=220">3</a></li>
				<li class="color-2">
					<a title="Exercícios" href="index.php?view=unidade2&topico=3&exercicio=2">4</a></li>
				<li class="color-2">
					<a title="Referências" href="index.php?view=unidade2&topico=4&resource=222">5</a></li>
			</ul>
		</div>

			<div id="getTop0" style="display:none;"><?php verificarTopico($id, 0, $uni) ?></div>
			<div id="getTop1" style="display:none;"><?php verificarTopico($id, 1, $uni) ?></div>
			<div id="getTop2" style="display:none;"><?php verificarTopico($id, 2, $uni) ?></div>
			<div id="getTop3" style="display:none;"><?php verificarTopico($id, 3, $uni) ?></div>
			<div id="getTop4" style="display:none;"><?php verificarTopico($id, 4, $uni) ?></div>
		

			<script>

				var topico = $(".testeTop").html();
				$( "#rs-carousel ul li a:eq("+ topico +")" ).css( "border-bottom", "3px solid #1e76ba" );

			</script>

		<?php


			for ($i=0; $i <5 ; $i++) { 
				//echo "<div id='getTop' style='display:none;'>".verificarTopico($id, $i, $uni)."</div>";
				//echo "<script>alert($('#getTop').html(););</script>";
				echo "<script>
					var a = document.getElementById('getTop".$i."').innerHTML;
					if(a != ''){
						$( '#rs-carousel ul li:eq(".$i.")' ).css( 'background-color', '78add6', 'important' );
					}

				</script>";
				
			}

			}else{
				$uni = 3;
		?>

		<div class="testeTop" style="display:none"><?php echo $topico; ?></div>
		<div id="rs-carousel" class="module bg" style="display:none;">
			<ul>
				
				<li class="color-2"><a title="Apresentação" href="index.php?view=unidade3&topico=0&resource=223">1</a></li>
				
				<li class="color-2"><a title="O que é o Prontuário Eletrônico do Cidadão (PEC)?" href="index.php?view=unidade3&topico=1&resource=224">2</a></li>
				
				<li class="color-2"><a title="O que é o Prontuário Eletrônico do Cidadão (PEC) – Centralizador?" href="index.php?view=unidade3&topico=2&resource=225">3</a></li>
				
				<li class="color-2"><a title="Cenários para implantação" href="index.php?view=unidade3&topico=3&resource=227">4</a></li>
				
				<li class="color-2"><a title="Protocolos SOAP e CIAP-2" href="index.php?view=unidade3&topico=4&resource=228">5</a></li>
				
				<li class="color-2"><a title="Exercícios" href="index.php?view=unidade3&topico=5&exercicio=3">6</a></li>
				
				<li class="color-2"><a title="Referências" href="index.php?view=unidade3&topico=6&resource=230">7</a></li>
				
			</ul>
		</div>

		<div id="getTop0" style="display:none;"><?php verificarTopico($id, 0, $uni) ?></div>
			<div id="getTop1" style="display:none;"><?php verificarTopico($id, 1, $uni) ?></div>
			<div id="getTop2" style="display:none;"><?php verificarTopico($id, 2, $uni) ?></div>
			<div id="getTop3" style="display:none;"><?php verificarTopico($id, 3, $uni) ?></div>
			<div id="getTop4" style="display:none;"><?php verificarTopico($id, 4, $uni) ?></div>
			<div id="getTop5" style="display:none;"><?php verificarTopico($id, 5, $uni) ?></div>
			<div id="getTop6" style="display:none;"><?php verificarTopico($id, 6, $uni) ?></div>

			<script>

				var topico = $(".testeTop").html();
				$( "#rs-carousel ul li a:eq("+ topico +")" ).css( "border-bottom", "3px solid #009797" );

			</script>

		<?php


				for ($i=0; $i <7 ; $i++) { 
					//echo "<div id='getTop' style='display:none;'>".verificarTopico($id, $i, $uni)."</div>";
					//echo "<script>alert($('#getTop').html(););</script>";
					echo "<script>
						var a = document.getElementById('getTop".$i."').innerHTML;
						if(a != ''){
							$( '#rs-carousel ul li:eq(".$i.")' ).css( 'background-color', '99d5d5', 'important' );
						}

					</script>";
					
				}


			}	

			
		?>


	</div>

	<!--Chamada de todos os .js que serão utilizados-->
	<script type="text/javascript">

		$LAB.setOptions({AlwaysPreserveOrder:true}) //garante que serão liberados sequencialmente
		.script("../lib/jquery.js")
		.script("../lib/rs_carousel/vendor/modernizr.3dtransforms.touch.js")
		.script('../lib/rs_carousel/vendor/jquery.ui.widget.js')
		.script("../lib/rs_carousel/vendor/jquery.event.drag.js")
		.script("../lib/rs_carousel/vendor/jquery.translate3d.js")
		.script("../lib/rs_carousel/dist/js/jquery.rs.carousel.js")
		.script("../lib/rs_carousel/dist/js/jquery.rs.carousel-autoscroll.js")
		.script("../lib/rs_carousel/dist/js/jquery.rs.carousel-continuous.js")
		.script("../lib/rs_carousel/dist/js/jquery.rs.carousel-touch.js")
		.script("../lib/rs_carousel/main/js/demo.js").wait(function() {
			//após a chamada do ultimo arquivo .js , a inicialização dos objetos deve ser feito imediatamente após
			try{
				demo.init($('#carousel'));
				$('#rs-carousel').show();
				//Pega o tópico atual para ser marcado no carousel
				var topico = $(".testeTop").html();

				//modifica as setas de navegação - só pegou assim
				$(".rs-carousel-action.rs-carousel-action-prev").html($(".seta_hide_left").html());
				$(".rs-carousel-action.rs-carousel-action-prev.rs-carousel-action-active").html($(".seta_hide_left").html());
				$(".rs-carousel-action.rs-carousel-action-next").html($(".seta_hide_right").html());
				$(".rs-carousel-action.rs-carousel-action-next.rs-carousel-action-active").html($(".seta_hide_right").html());
				$('#rs-carousel').carousel('goToItem', topico); //move o carousel para o item atual

			}catch(err){
				alert(err);      
				
			}
			
			}
		);

		


	</script>

</body>
</html>
