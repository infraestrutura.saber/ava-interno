<!doctype html>
<html>
<head>

	<meta charset="utf-8" />
	
	<link rel="stylesheet" type="text/css" href="../lib/rs_carousel/main/css/demo_ex.css" media="all" />
	<link rel="stylesheet" type="text/css" href="../lib/rs_carousel/main/css/base.css" media="all" />
	<link rel="stylesheet" type="text/css" href="../lib/rs_carousel/dist/css/jquery.rs.carousel.css" media="all" />

	<!--Chamada da lib LABJS independente-->
	<script type="text/javascript" src="../lib/LAB.js"></script>


	
</head>
<body class="no-js">
	<div id="carousel-ex">
		
		
		<form class="module" >
			
			<div class="actions" style="display: none;">
				<ul class="action-set base">
					<li class="action">
						<input type="button" id="init" name="init" value="init" />
					</li>
					<li class="action">
						<input type="button" id="destroy" name="destroy" value="destroy" />
					</li>
				</ul>
			</div>
		</form>

		<?php

			$view = $_GET['view'];

			if($view == 'unidade1'){
		?>

		<div id="rs-carousel-ex" class="module bg" style="display:none;">
			<ul>
				<li class="color-2">
					<a href="index.php?view=unidade1&topico=8&exercicio=1&ex=1"><img src="../imagens/conteudo/icn_exer.png"></a></li>
				<li class="color-2">
					<a  href="index.php?view=unidade1&topico=8&exercicio=1&ex=2"><img src="../imagens/conteudo/icn_exer.png"></a></li>
				<li class="color-2">
					<a  href="index.php?view=unidade1&topico=8&exercicio=1&ex=3"><img src="../imagens/conteudo/icn_exer.png"></a></li>
			</ul>
		</div>

		<?php
			}else if($view == 'unidade2'){
		?>


		<div id="rs-carousel-ex" class="module bg" style="display:none;">
			<ul>
				<li class="color-2">
					<a href="index.php?view=unidade2&topico=3&exercicio=2&ex=1"><img src="../imagens/conteudo/icn_exer.png"></a></li>
				<li class="color-2">
					<a href="index.php?view=unidade2&topico=3&exercicio=2&ex=2"><img src="../imagens/conteudo/icn_exer.png"></a></li>
				<li class="color-2">
					<a href="index.php?view=unidade2&topico=3&exercicio=2&ex=3"><img src="../imagens/conteudo/icn_exer.png"></a></li>
				</ul>
		</div>

		<?php
			}else{

		?>


		<div id="rs-carousel-ex" class="module bg" style="display:none;">
			<ul>
				<li class="color-2">
					<a href="index.php?view=unidade3&topico=3&exercicio=3&ex=1"><img src="../imagens/conteudo/icn_exer.png"></a></li>
				<li class="color-2">
					<a href="index.php?view=unidade3&topico=3&exercicio=3&ex=2"><img src="../imagens/conteudo/icn_exer.png"></li>
				<li class="color-2">
					<a href="index.php?view=unidade3&topico=3&exercicio=3&ex=3"><img src="../imagens/conteudo/icn_exer.png"></li>
				
			</ul>
		</div>

		<?php
			}
		?>




	</div>

	<!--Chamada de todos os .js que serão utilizados-->
	<script type="text/javascript">

		$LAB.setOptions({AlwaysPreserveOrder:true})//garante que serão liberados sequencialmente
		.script("../lib/jquery.js")
		.script("../lib/rs_carousel/vendor/modernizr.3dtransforms.touch.js")
		.script('../lib/rs_carousel/vendor/jquery.ui.widget.js')
		.script("../lib/rs_carousel/vendor/jquery.event.drag.js")
		.script("../lib/rs_carousel/vendor/jquery.translate3d.js")
		.script("../lib/rs_carousel/dist/js/jquery.rs.carousel.js")
		.script("../lib/rs_carousel/dist/js/jquery.rs.carousel-autoscroll.js")
		.script("../lib/rs_carousel/dist/js/jquery.rs.carousel-continuous.js")
		.script("../lib/rs_carousel/dist/js/jquery.rs.carousel-touch.js")
		.script("../lib/rs_carousel/main/js/demo_ex.js").wait(function() {
		//após a chamada do ultimo arquivo .js , a inicialização dos objetos deve ser feito imediatamente após
	
			try{
				demoEx.init($('#carousel-ex'));
				$('#rs-carousel-ex').show();
				$(".rs-carousel-action.rs-carousel-action-prev").html($(".seta_hide_left").html());
				$(".rs-carousel-action.rs-carousel-action-prev.rs-carousel-action-active").html($(".seta_hide_left").html());
				$(".rs-carousel-action.rs-carousel-action-next").html($(".seta_hide_right").html());
				$(".rs-carousel-action.rs-carousel-action-next.rs-carousel-action-active").html($(".seta_hide_right").html());
				
			}catch(err){
				alert(err);
				
			}
			
		});

		
		
	</script>

</body>
</html>
