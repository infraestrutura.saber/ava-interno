<?php
include '../../../config.php';
include '../functions/conectar.php';
conectar();
include '../functions/functions.php'; //funcoes de conteudo, cadastro de topicos...


$unidade = $_GET ['view'];
$topico = $_GET ['topico'];
$id =$USER->id;
$cpf = $USER->username;
session_start('config');
//verificando autenticacao
if($_SESSION['logado'] != 1){
	header('Location: http://ufpe.unasus.gov.br/moodle_interno/cursos/e-sus_branch_mirela');
}

$_SESSION['firstname'] = $USER->firstname;
$_SESSION['id'] = $id;
$_SESSION['cpf'] = $cpf;
$_SESSION['topico'] = $topico;
$_SESSION['unidade'] = $unidade;
validar_usuario($cpf,$id);

?>

<div id="menu" class="visible-desktop">
	<ul>
		<li>
			 	<?php
					
					if (end ( explode ( "/", getcwd () ) ) == "apresentacao") {
						
						$nivel = "../";
					} else {
						
						$nivel = "../../../";
					}
					
					// echo '<a href="'.$nivel.'apresentacao/index.php"><img id="icone_home" src="../../../imagens/home.png"> Início</a>';
					echo '<a href="index.php"><img id="icone_home" src="../imagens/home.png"> Início</a>';
					?>

			 </li>
		<li><a>Unidades Didáticas <b class="caret"></b></a>
			<ul>
				<!--NAVEGAÇÃO-->
				<li><a href="index.php?view=menu">Acesso às unidades</b></a>
					<!--UNIDADE 1-->
				
				<li><a href="index.php?view=unidade1&topico=0&resource=208">Unidade 1</b></a>

				</li>
				<!--UNIDADE 2-->
				<li><a href="index.php?view=unidade2&topico=0&resource=218">Unidade 2</b></a>

				</li>
				<!--UNIDADE 3-->
				<li><a href="index.php?view=unidade3&topico=0&resource=223">Unidade 3</b></a>

				</li>
				<!--UNIDADE 4-->


			</ul></li>
		<li><a href="index.php?view=forum">Fórum de discussões<?php alert_postage($id,'desktop');?> </a></li>
		<li><a href="index.php?view=certificacao">Certificação</a></li>
		<li><a href="index.php?view=perguntas">Perguntas frequentes</a></li>
		<li><a href="index.php?view=opiniao">Perfil e opinião</a></li>
		<li><a href="index.php?view=creditos">Créditos</a></li>
	</ul>
	<div id="sair"><?php echo "Olá <b>".$_SESSION['firstname']."</b>! | ";?>Sair</div>
	<div id="fechar">
		<p>
			<?php
			include ('sair.php');
			?>
			<img id="icone_sair" src="../imagens/icn_sair.png"></a>
		</p>
	</div>

</div>


<script type="text/javascript">

							  		function abreModal () {
							  			
							  			 $(".inline").colorbox({inline:true, width:"60%"});
							  		}

							  </script>

<!-- Certificacao-->
<div style='display: none'>
	<div id="modal_certificacao" style='padding: 10px; background: #fff;'>
		<h4>Certificação</h4>

		<br> Para que você receba a declaração de conclusão deste módulo, é
		necessário fazer a avaliação final. Recomendamos que esta seja a
		última etapa, depois que você tiver concluído seus estudos. <br> <br>
		A avaliação vale de zero (0,0) a dez (10,0). Para receber a declaração
		de conclusão do módulo é necessário tirar, pelo menos, 7,0 (sete). São
		permitidas 3 (três tentativas). Caso você não atinja nota mínima 7,0
		(sete), será necessário fazer o curso novamente. Após atingir nota
		igual ou superior a 7,0 (sete), você receberá, por e-mail, um link que
		permitirá a impressão do certificado. O link será encaminhado em até
		72 horas após a conclusão da avaliação. <br> <br> Clique <a
			href="http://ufpe.unasus.gov.br/moodle_unasus/mod/quiz/view.php?id=21"
			target="_blank">aqui</a> para iniciar a avaliação final. <br>

	</div>
</div>
<!-- fim -->

<!-- Perguntas-->
<div style='display: none'>
	<div id="modal_perguntas" style='padding: 10px; background: #fff;'>
		<h4>Perguntas frequentes</h4>

		<br> Página em construção. <br>

	</div>
</div>
<!-- fim -->

<!-- Opiniao-->
<div style='display: none'>
	<div id="modal_perfil" style='padding: 10px; background: #fff;'>
		<h4>Perfil e opinião</h4>

		<br> Gostaríamos de conhecer o seu perfil tecnológico e a sua opinião
		sobre o curso. <br> Clique <a
			href=" http://ufpe.unasus.gov.br/moodle_unasus/mod/feedback/view.php?id=20"
			target="_blank">aqui</a> para responder ao questionário. <br>

	</div>
</div>
<!-- fim -->

<!-- Creditos-->
<div style='display: none'>
	<div id="modal_creditos" style='padding: 10px; background: #fff;'
		style="font-size: 14">
		<h4>Créditos</h4>
		<br> <b>© 2014. Ministério da Saúde. Secretaria de Atenção à Saúde.
			Departamento de Atenção Básica. <br>Coordenação Geral de Atenção
			Domiciliar.
		</b><br> <br> Todos os direitos reservados. É permitida a reprodução
		parcial ou total desta obra, desde que citada a fonte e que não seja
		para venda ou qualquer fim comercial. A responsabilidade pelos
		direitos autorais de textos e imagens desta obra é de responsabilidade
		da área técnica.<br> <br>

		<div id="left">

			<b>Curso online Princípios para o cuidado domiciliar 2</b><br>
			Primeira oferta: março de 2014 <br> <br> <b>Elaboração, difusão e
				informações</b><br> Universidade Aberta do Sistema Único De Saúde
			(UNA-SUS)<br> Secretaria de Gestão do Trabalho e da Educação Na Saúde
			(SGTES) <br> Secretaria de Atenção à Saúde – Departamento de Atenção
			Básica – Coordenação Geral de Atenção Domiciliar<br> <br> <b>UNA-SUS
				| Universidade Federal de Pernambuco </b><br> Coordenação Geral:
			Cristine Martins Gomes de Gusmão <br> Coordenação Técnica: Josiane
			Lemos Machiavelli <br> Coordenação de Educação a Distância: Sandra de
			Albuquerque Siebra <br> Gerente de Projetos: Júlio Venâncio de
			Menezes Júnior<br> <br> <i><b>Equipe de produção do curso</b></i><br>
			<br> <b>Coordenadora de produção </b><br> Josiane Lemos Machiavelli
			(UNA-SUS UFPE) <br> <br> <b>Autoras</b><br>Geruza Pereira Gomes
			Almeida<br> Luzia Maria dos Santos <br> Vanuza Regina Lommez De
			Oliveira<br> <br> <b>Autora colaboradora</b><br> Patrícia Pereira da
			Silva (UNA-SUS UFPE)<br> <br> <b>Designer instrucional </b><br>
			Patrícia Pereira da Silva (UNA-SUS UFPE) <br> <br> <b>Projeto gráfico
			</b><br> Juliana Leal (UNA-SUS UFPE) <br> Rodrigo Cavalcanti Lins
			(UNA-SUS UFPE)<br>



		</div>


		<div id="right">
			<b>Ilustrações</b><br /> Juliana Leal (UNA-SUS UFPE)<br /> Vinícius
			Haniere Saraiva Milfont (UNA-SUS UFPE)<br /> <br /> <b>Produção
				audiovisual</b><br /> Amanda Rosineide da Silva (UFPE)<br> Caio Lira
			(UFPE)<br> Carolina Barbosa Rangel (UNA-SUS UFPE) <br> Geraldo Luiz
			Monteiro do Nascimento (UNA-SUS UFPE) <br> Jamilly Muniz de Oliveira
			(UFPE)<br> Luiz Miguel Picelli Sanches (UNA-SUS UFPE)<br> Patrícia
			Pereira da Silva (UNA-SUS UFPE)<br> Rodrigo Luis da Silveira Silva
			(UFPE)<br> <b><br /> Revisão linguística</b><br /> Emanuel Cordeiro
			da Silva (UNA-SUS UFPE)<br /> <br /> <b>Equipe de tecnologia da
				informação</b><br /> Lucy do Nascimento Cavalcante (UNA-SUS UFPE)<br />
			Mirela Natali Vieira de Souza (UNA-SUS UFPE) <br /> Rodrigo
			Cavalcanti Lins (UNA-SUS UFPE) <br /> Wellton Thiago Machado Ferreira
			(UNA-SUS UFPE) <br /> <br /> <b>Equipe de ciência da informação </b><br />
			Vildeane da Rocha Borba (UNA-SUS UFPE) <br /> Jacilene Adriana da
			Silva Correia (UNA-SUS UFPE) <br /> <br /> <b>Equipe de supervisão
				acadêmica </b><br /> Fabiana de Barros Lima (UNA-SUS UFPE) <br />
			Geisa Ferreira da Silva (UNA-SUS UFPE) <br /> Isabella Maria Lorêto
			da Silva (UNA-SUS UFPE) <br /> Rosilândia Maria da Silva (UNA-SUS
			UFPE) <br /> <br /> <b>Colaboradores na validação externa do curso </b><br />
			Leonardo Savassi (Secretaria Executiva da UNA-SUS) <br /> Mariana
			Borges Dias (Coordenação Geral de Atenção Domiciliar) <br /> Mara
			Lúcia Renostro Zachi (Secretaria Executiva da UNA-SUS)


		</div>



		<br>

	</div>
</div>
<!-- fim -->
<div class="saudacao hidden-desktop">
	Olá, <b><?php echo $USER->firstname; ?></b> !
</div>
<div id="menu_responsivo" class="hidden-desktop">

	<!-- <div class="dropdown">
		<img class="dropdown-toggle" data-toggle="dropdown" src="../imagens/icn_menu_responsivo.svg" />
		
		
		<ul class="dropdown-menu">
			<li>
				<a tabindex="-1" href="#">Ação</a>
			</li>
			<li>
				<a tabindex="-1" href="#">Outra ação</a>
			</li>
			<li>
				<a tabindex="-1" href="#">Algo a mais aqui</a>
			</li>
			<li class="divider"></li>
			<li>
				<a tabindex="-1" href="#">Link separador</a>
			</li>
		</ul>
	</div> -->


	<img class="icon_menu" src="../imagens/icn_menu_responsivo.svg" />
	<!--<div class="menu_reponsive_container">
  	
</div>-->
</div>

<style type="text/css">
#right {
	width: 48%;
	float: right;
	font-size: 12px !important;
	margin-left: 10px;
}

#left {
	width: 46%;
	float: left;
	font-size: 12px !important;
}

#modal_creditos b {
	font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans",
		"Helvetica Neue", Arial, sans-serif;
	!
	important;
}
</style>
<script>
				$('.dropdown-toggle').dropdown();
			</script>
<script type="text/javascript">
							$(document).ready(function() {
								$("#menu li a").mouseover(function() {
									var index = $("#menu li a").index(this);
									$("#menu li").eq(index).children("ul").slideDown(100);
									if ($(this).siblings('ul').size() > 0) {
										return false;
									}
								});
								$("#menu li").mouseleave(function() {
									var index = $("#menu li").index(this);
									$("#menu li").eq(index).children("ul").slideUp(100);
								});
							});

		</script>
