<!DOCTYPE html!>
<html lang="pt-br">
<!-- <html lang="pt-br" manifest="../includes/unasus_ufpe.manifest"> -->
<meta charset="utf-8"> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<head>
		<title>e-SUS</title>
		<link rel="stylesheet" type="text/css" href="../lib/slidebars/slidebars.css">
		
		<link rel="stylesheet" type="text/css" href="../css/apresentacao.css">
		<link rel="stylesheet" type="text/css" href="../css/navegacao.css">
		<link rel="stylesheet" type="text/css" href="../css/style.css">
		<link rel="stylesheet" type="text/css" href="../css/forum.css">
		<script type="text/javascript" src="../lib/jquery.js"></script>
		<script type="text/javascript" src="../lib/cycle.js"></script>
		<script type="text/javascript" src="../scripts/apresentacao.js"></script>
		<script type="text/javascript" src="../scripts/script.js"></script>

		<script type="text/javascript" src="../lib/bootstrap/js/bootstrap.js"></script>
		<script type="text/javascript" src="../lib/colorbox/jquery.colorbox.js"></script>
		<link rel="stylesheet" type="text/css" href="../lib/colorbox/colorbox.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap-responsive.css">
		<script src="../lib/ckeditor/ckeditor.js"></script>
	</head>

	<body>
	<div id="sb-site">
		<div id="barra_topo">
			
			<div class="container">	
		<?php
		include '../includes/menu.php';
		?>
			</div>
		</div>
		<div id="background_conteudo">
		<!-- <div class="container">	 -->
		
		<?php
		
		$view = $_GET['view'];
 		tratamentoPagina($view);
 		//include ('forum.php'); 
		?>					


		<!-- </div> -->
		</div>
		<div id="footer">
			<img src="../imagens/logo_ppe.png" />
			<img src="../imagens/logo_ufpe.png" />
			<img src="../imagens/logo_saber.png" />
			<img src="../imagens/logo_una_sus.png" />
			<img src="../imagens/logo_ms.png" />
			<img src="../imagens/logo_gf.png" />
		</div>
		<!--fim slidebar-->
		</div>
		
		<div class="sb-slidebar sb-right">
		<nav>
		<ul class="sb-menu">
			<li class="sb-close"><a class="marginLiMenu" href="index.php">Inicio</a></li>
			<li class="sb-close"><a class="marginLiMenu" href="index.php?view=menu">Acesso às unidades</a></li>			
			<li class="sb-close"><a class="marginLiMenu" href="index.php?view=unidade1&topico=0&resource=208">Unidade 1</a></li>
			<li class="sb-close"><a class="marginLiMenu" href="index.php?view=unidade2&topico=0&resource=218">Unidade 2</a></li>
			<li class="sb-close"><a class="marginLiMenu" href="index.php?view=unidade3&topico=0&resource=223">Unidade 3</a></li>
			<li class="sb-close"><a class="marginLiMenu" href="index.php?view=forum"><?php alert_postage($id,'smarth');?>Fórum de discussões</a></li>
			<li class="sb-close"><a class="marginLiMenu" href="index.php?view=certificacao">Certificação</a></li>
			<li class="sb-close"><a class="marginLiMenu" href="index.php?view=perguntas">Perguntas Frequentes</a></li>
			<li class="sb-close"><a class="marginLiMenu" href="index.php?view=opiniao">Perfil/Opinião</a></li>
			<li class="sb-close"><a class="marginLiMenu" href="index.php?view=creditos">Créditos</a></li>
		</ul>
	</nav>
      <!-- Your right Slidebar content. -->
    	</div>
    	<!-- jQuery -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <!-- Slidebars -->
 <script type="text/javascript" src="../lib/slidebars/slidebars.min.js"></script>
    <script>
  (function($) {
    $(document).ready(function() {
      var mySlidebars = new $.slidebars();
      $('.my-button').on('click', function() {
        mySlidebars.slidebars.open('left');
      });
      $('.icon_menu').on('click', function() {
        mySlidebars.slidebars.toggle('right');
      });
      $('.my-third-button').click(mySlidebars.slidebars.close);
    });
  }) (jQuery);
</script>
	</body>
</html>
