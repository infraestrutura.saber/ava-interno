<?php
// include '../../../config.php';


// //include '../functions/conectar.php';
// //conectar ();
//include '../functions/functions.php';

// $id =$USER->id;
// $cpf = $USER->username;
// session_start('config');
// $cpf=
$id=$_SESSION['id'];
?>
	<div class="row-fluid marginTop60">

	<div class="span12 no_left botoesMenu">

		<div class="span4">
			<div id="sisab" class="bg1 icone_unidade">
				<center><img src="../imagens/navegacao/Icn_U1.png"/></center>
				<div class="porc_menu"><?php 
				
				porcentagem(1,$cpf);?></div>
			</div>
			<div class="bg1 titulo_menu">
				Apresentação do SISAB 
			</div>
			<a href="index.php?view=unidade1&topico=0&resource=208">
			<div class="item_menu bg1_1">
				Apresentação <?php echo verificarTopico($id, 0, 1); ?>
			</div></a>
			<a href="index.php?view=unidade1&topico=1&resource=209">
			<div class="item_menu bg1_2">
				Entenda o que é o SISAB e o e-SUS <?php verificarTopico($id, 1, 1); ?>
			</div></a>
			<a href="index.php?view=unidade1&topico=2&resource=210">
			<div class="item_menu bg1_1">
				SIAB e SISAB, o que mudou? <?php verificarTopico($id, 2, 1); ?>
			</div></a>
			<a href="index.php?view=unidade1&topico=3&resource=211">
			<div class="item_menu bg1_2">
				Usuários do sistema	<?php verificarTopico($id, 3, 1); ?>		
			</div></a>
			<a href="index.php?view=unidade1&topico=4&resource=212">
			<div class="item_menu bg1_1">
				Como implantar o e-SUS AB no município? <?php verificarTopico($id, 4, 1); ?>
			</div></a>
			<a href="index.php?view=unidade1&topico=5&resource=213">
			<div class="item_menu bg1_2">
				Cenários de implantação do e-SUS AB	<?php verificarTopico($id, 5, 1); ?>		
			</div></a>
			<a href="index.php?view=unidade1&topico=6&resource=214">
			<div class="item_menu bg1_1">
				O que é CDS e PEC? <?php verificarTopico($id, 6, 1); ?>		
			</div></a>
			<a href="index.php?view=unidade1&topico=7&resource=215">
			<div class="item_menu bg1_2">
				Suporte técnico <?php verificarTopico($id, 7, 1); ?>
			</div></a>
			<a href="index.php?view=unidade1&topico=8&exercicio=1">
			<div class="item_menu bg1_1">
				Exercícios <?php verificarTopico($id, 8, 1); ?>			
			</div></a>
			<a href="index.php?view=unidade1&topico=9&resource=217">
			<div class="item_menu bg1_2">
				Referências	<?php verificarTopico($id, 9, 1); ?>		
			</div></a>
			
		</div>

		<div class="span4">
			<div id="cds" class="bg2 icone_unidade">
				<center><img src="../imagens/navegacao/Icn_U2.png"/></center>
				<div class="porc_menu"><?php porcentagem(2,$cpf); ?></div>
			</div>


			<div class="bg2 titulo_menu">
Implantação da Coleta
de Dados Simplificada (CDS)			</div>
			<a href="index.php?view=unidade2&topico=0&resource=218">
			<div class="item_menu bg2_1">
				Apresentação <?php verificarTopico($id, 0, 2); ?>
			</div></a>
			<a href="index.php?view=unidade2&topico=1&resource=219">
			<div class="item_menu bg2_2">
				O que é o CDS? <?php verificarTopico($id, 1, 2); ?>			
			</div></a>
			<a href="index.php?view=unidade2&topico=2&resource=220">
			<div class="item_menu bg2_1">
				Cenários de implantação <?php verificarTopico($id, 2, 2); ?>
			</div></a>
			<a href="index.php?view=unidade2&topico=3&exercicio=2">
			<div class="item_menu bg2_2">
				Exercícios <?php verificarTopico($id, 3, 2); ?>		
			</div></a>
			<a href="index.php?view=unidade2&topico=4&resource=222">
			<div class="item_menu bg2_1">
				Referências	<?php verificarTopico($id, 4, 2); ?>	
			</div></a>

		</div>

		<div class="span4">
			<div id="pec" class="bg3 icone_unidade">
				<center><img src="../imagens/navegacao/Icn_U3.png"/></center>
				<div class="porc_menu"><?php porcentagem(3,$cpf); ?></div>
			</div>

			<div class="bg3 titulo_menu">
Prontuário Eletrônico do
Cidadão (PEC)		</div>
			<a href="index.php?view=unidade3&topico=0&resource=223">
			<div class="item_menu bg3_1">
				Apresentação <?php verificarTopico($id, 0, 3); ?>
			</div></a>
			<a href="index.php?view=unidade3&topico=1&resource=224">
			<div class="item_menu bg3_2">
				O que é o Prontuário Eletrônico do
Cidadão (PEC)?	<?php verificarTopico($id, 1, 3); ?>	
			</div></a>
			<a href="index.php?view=unidade3&topico=2&resource=225">
			<div class="item_menu bg3_1">
				O que é o Prontuário Eletrônico do Cidadão (PEC) – Centralizador? <?php verificarTopico($id, 2, 3); ?>		
			</div></a>
			<a href="index.php?view=unidade3&topico=3&resource=227">
			<div class="item_menu bg3_2">
				Cenários para implantação <?php verificarTopico($id, 3, 3); ?>
			</div></a>
			<a href="index.php?view=unidade3&topico=4&resource=228">
			<div class="item_menu bg3_1">
				Protocolos SOAP e CIAP-2 <?php verificarTopico($id, 4, 3); ?>
			</div></a>
			<a href="index.php?view=unidade3&topico=5&exercicio=3">
			<div class="item_menu bg3_2">
				Exercícios <?php verificarTopico($id, 5, 3); ?>
			</div></a>

			<a href="index.php?view=unidade3&topico=6&resource=230">
			<div class="item_menu bg3_1">
				Referências	<?php verificarTopico($id, 6, 3); ?>		
			</div></a>


		</div>

	</div>

</div>







</html>