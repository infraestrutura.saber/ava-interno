
	<div class="row-fluid marginTop60">

	<div class="span12 no_left">

		<div class="span4">
			<div id="sisab" class="bg1 icone_unidade">
				<center><img src="../imagens/navegacao/Icn_U1.png"/></center>
				<div class="porc_menu"><?php $cpf = 'rodrigo'; porcentagem(1,$cpf);?></div>
			</div>
			<div class="bg1 titulo_menu">
				Apresentação do SISAB
			</div>

			<div class="item_menu bg1_1">
				<a href="index.php?view=unidade1&topico=0&resource=208">Apresentação</a>
			</div>

			<div class="item_menu bg1_2">
				<a href="index.php?view=unidade1&topico=1&resource=209">Entenda o que é o SISAB e o e-SUS</a>
			</div>

			<div class="item_menu bg1_1">
				<a href="index.php?view=unidade1&topico=2&resource=210">SIAB e SISAB</a>
			</div>

			<div class="item_menu bg1_2">
				<a href="index.php?view=unidade1&topico=3&resource=211">Usuários do sistema</a>			
			</div>

			<div class="item_menu bg1_1">
				<a href="index.php?view=unidade1&topico=4&resource=212">Como implementar o e-SUS</a>
			</div>

			<div class="item_menu bg1_2">
				<a href="index.php?view=unidade1&topico=5&resource=213">Cenários de implementação</a>			
			</div>

			<div class="item_menu bg1_1">
				<a href="index.php?view=unidade1&topico=6&resource=214">O que é CDS e PEC?</a>			
			</div>

			<div class="item_menu bg1_2">
				<a href="index.php?view=unidade1&topico=7&resource=215">Suporte técnico</a>	

			</div>

			<div class="item_menu bg1_1">
				<a href="index.php?view=unidade1&topico8=&resource=216">Exercícios</a>			
			</div>
			
		</div>

		<div class="span4">
			<div id="cds" class="bg2 icone_unidade">
				<center><img src="../imagens/navegacao/Icn_U2.png"/></center>
				<div class="porc_menu"><?php $cpf = 'rodrigo'; porcentagem(2,$cpf); ?></div>
			</div>


			<div class="bg2 titulo_menu">
Implantação da Coleta
de Dados Simplificada (CDS)			</div>

			<div class="item_menu bg2_1">
				<a href="index.php?view=unidade2&topico=0&resource=218">Apresentação</a>
			</div>

			<div class="item_menu bg2_2">
				<a href="index.php?view=unidade2&topico=1&resource=219">O que é o CDS?</a>			
			</div>

			<div class="item_menu bg2_1">
				<a href="index.php?view=unidade2&topico=2&resource=220">Cenários de implantação</a>
			</div>

			<div class="item_menu bg2_2">
				<a href="index.php?view=unidade2&topico=3&resource=221">Exercícios</a>		
			</div>

			<div class="item_menu bg2_1">
				<a href="index.php?view=unidade2&topico=4&resource=217">Referências</a>		
			</div>

		</div>

		<div class="span4">
			<div id="pec" class="bg3 icone_unidade">
				<center><img src="../imagens/navegacao/Icn_U3.png"/></center>
				<div class="porc_menu"><?php $cpf = 'rodrigo'; porcentagem(3,$cpf); ?></div>
			</div>

			<div class="bg3 titulo_menu">
Prontuário Eletrônico do
Cidadão (PEC)		</div>

			<div class="item_menu bg3_1">
				<a href="index.php?view=unidade3&topico=0&resource=223">Apresentação</a>
			</div>

			<div class="item_menu bg3_2">
				<a href="index.php?view=unidade3&topico=1&resource=224">O que é o Prontuário Eletrônico do
Cidadão (PEC)?</a>		
			</div>

			<div class="item_menu bg3_1">
				<a href="index.php?view=unidade3&topico=2&resource=225">O que é o Prontuário Eletrônico do Cidadão (PEC) – Centralizador??	</a>	
			</div>

			
			<div class="item_menu bg3_2">
				<a href="index.php?view=unidade3&topico=3&resource=226">Vantagens de implantação</a>
			</div>

			<div class="item_menu bg3_1">
				<a href="index.php?view=unidade3&topico=4&resource=227">Cenários para implantação</a>
			</div>

			<div class="item_menu bg3_2">
				<a href="index.php?view=unidade3&topico=5&resource=228">Protocolos SOAP e CIAP-2</a>
			</div>

			<div class="item_menu bg3_1">
				<a href="index.php?view=unidade3&topico=6&resource=229">Exercícios</a>
			</div>


			<div class="item_menu bg3_2">
				<a href="index.php?view=unidade3&topico=7&resource=230">Referências		</a>	
			</div>


		</div>

	</div>

</div>







</html>