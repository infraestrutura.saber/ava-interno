<h4>Exercicios</h4>
<br><br>
<table style="border: 0px; width: 100%;"><tbody><tr style="background-color: rgb(30, 118, 186);"><td style="padding-left: 20px; height: 48px;"><b><font color="#ffffff" size="4" style="position: relative; bottom: 10px;"><img src="http://ava.unasusufpe.com.br/moodle_interno/file.php/6/icones_caixas/icn_atencao_u2.png" alt="Atenção" title="Atenção" style="position: relative; top: 12px;" /> Atenção</font></b></td></tr><tr style="background-color: rgb(165, 200, 227);"><td style="padding: 10px 10px 10px 20px;"><span style="background-color: rgb(165, 200, 227);"><span style="color: rgb(34, 34, 34); font-size: small;">Antes de iniciar os exercícios, conheça as fichas e os campos utilizados pela CDS. Clique </span><span style="color: rgb(34, 34, 34); font-size: small;"><a href="http://189.28.128.100/dab/docs/portaldab/documentos/manual_CDS_ESUS.pdf" style="font-weight: bold; text-decoration: underline;">aqui</a> </span><span style="color: rgb(34, 34, 34); font-size: small;">(BRASIL, 2014a).</span></span></td></tr></tbody></table>
<br>
<h5>Caso 1</h5>
<!-- inicio exercicio 1 -->
<center><img src="../unidades/unidade2/imagens/estudo_de_caso01_u2.jpg" class="visible-desktop"></center><br><br>
<center><img src="../unidades/unidade2/imagens/estudo_de_caso01_u2.jpg" style="width: 100%;" class="hidden-desktop"></center><br><br>
<p class="MsoNormal" style="text-align: left;">
	Maria Ângela, 69 anos, casada, fumante, trabalha como auxiliar de escritório e chega à Unidade Básica 
	de Saúde com queixa de dor de cabeça, principalmente ao fim do dia. É acolhida pela enfermeira, que 
	verifica que a pressão arterial (PA) de Maria Ângela se encontra limítrofe (140x90 mmHg). O médico da 
	unidade atende Dona Maria Ângela e, ao verificar o histórico, constata que ela já apresentou, em outros
	 momentos, alteração na PA. Ela se queixa de alta carga de trabalho e do relacionamento instável com o 
	 marido, que é usuário de drogas. Isso gera estresses constantes. Após exame físico, prescrição de 
	 medicação e orientações, é agendada uma consulta de retorno para abordar melhor as questões de trabalho 
	 e familiares.<br><br>
	<b>1)  Para essa situação, qual é o tipo de ficha que deve ser preenchida?</span></b><br><br>
	


<div id="unid2_ex1"></div>
<script src="../unidades/unidade2/script.js" type="text/javascript"></script>
<script src="../unidades/unidade2/unid2_ex1.js" type="text/javascript"></script>
<br /><br />
<div class="box" id="boxunid2_ex1"></div>
<div class="feedback feedbackunid2_ex1">
	<p class="MsoNormal"></p>
	<div class="feed">
		 Esta alternativa está errada. A ficha A é uma ficha extinta com a chegada do e-SUS, que era preenchida nas primeiras visitas que o Agente Comunitário de Saúde (ACS) fazia às famílias de sua comunidade, sendo uma ficha por família.
		<br />
	</div>
	<div class="feed">
		 Esta alternativa está errada. A ficha de procedimentos é para a coleta de dados sobre a realização de procedimentos e pequenas cirurgias ambulatoriais (BRASIL, 2014a).
		<br />
	</div>
	<div class="feed">
		Esta é a alternativa correta. A ficha de atendimento individual é um instrumento de coleta de dados dos atendimentos realizados em cada turno, por determinado profissional. O preenchimento poderá ser realizado pelos profissionais de nível superior da equipe de Atenção Básica, do NASF (Núcleo de Apoio à Saúde da Família) e do Consultório na Rua (CnR), com exceção do cirurgião-dentista, cuja ficha será própria(BRASIL, 
		<br />
	</div>
	<div class="feed">
		Esta alternativa está errada. O atendimento inicial foi apenas com a Dona Maria Ângela, logo deve ser utilizada a ficha de atendimento individual, que é um instrumento de coleta de dados dos atendimentos realizados em cada turno, por determinado profissional(BRASIL, 2014a).
	</div>
</div>


<!-- inicio exercicio 2 -->
<b>2) Qual tipo de atendimento a enfermeira deve marcar na ficha de atendimento?</b>
<br><br>



<div id="unid2_ex2"></div>

<script src="../unidades/unidade2/unid2_ex2.js" type="text/javascript"></script>
<br /><br />
<div class="box" id="boxunid2_ex2"></div>
<div class="feedback feedbackunid2_ex2">
	<p class="MsoNormal"></p>
	<div class="feed">
		Esta alternativa está errada. A dentista realizou procedimentos para minimizar a dor do paciente. A urgência nesse caso foi configurada pela necessidade de atendimento, impreterivelmente, no mesmo dia, com prioridades quanto à condição física e/ou vulnerabilidade psicossocial do paciente. Logo, o campo a ser marcado na ficha é o de “atendimento de urgência”.
		<br />
	</div>
	<div class="feed">
		Esta alternativa está errada. A dentista realizou procedimentos para minimizar a dor do paciente. A urgência nesse caso foi configurada pela necessidade de atendimento, impreterivelmente, no mesmo dia, com prioridades quanto à condição física e/ou vulnerabilidade psicossocial do paciente. Logo, o campo a ser marcado na ficha é o de “atendimento de urgência”.
		<br />
	</div>
	<div class="feed">
		Esta alternativa está correta. A dentista realizou procedimentos para minimizar a dor do paciente. A urgência nesse caso foi configurada pela necessidade de atendimento, impreterivelmente, no mesmo dia, com prioridades quanto à condição física e/ou vulnerabilidade psicossocial do paciente. Logo, o campo a ser marcado na ficha é o de “atendimento de urgência”.
		<br />
	</div>
	<div class="feed">
		Falta texto, Poeta!
	</div>
</div>

<!-- inicio exercicio 3 -->
<b>3)  Em relação ao campo “procedimentos/quantidade realizada”, quais campos a dentista deveria marcar?

</b><br><br>
1. Articular e viabilizar equipes de suporte à informatização nos estabelecimentos e elaborar estratégia de implantação, contemplando o dimensionamento da quantidade de usuários vinculados às equipes da AB, para cadastro dos usuários.<br>
2. Garantir a integração do planejamento local ao plano de ação regionale viabilizar a qualificação de profissionais para capacitar os profissionais de saúde e de tecnologia da informação do município.<br>
3. Identificar profissionais habilitados para a coordenação da implantação do sistema e realizar diagnóstico situacional da capacidade dos recursos humanos existentes nos estados e municípios.<br>
4. Realizar o levantamento da capacidade tecnológica disponível em cada Unidade Básica de Saúde (UBS), nas Secretarias Municipais de Saúde (SMS) e na Secretaria de Estado de Saúde (SES);da necessidade de computadores e de impressoras, de acordo com a quantidade de ambientes e conectividade à internet.<br>
</p><p></p>

<div id="unid2_ex3"></div>

<script src="../unidades/unidade2/unid2_ex3.js" type="text/javascript"></script>
<br /><br />
<div class="box" id="boxunid2_ex3"></div>
<div class="feedback feedbackunid2_ex3">
	<p class="MsoNormal"></p>
	<div class="feed">
		Esta alternativa está correta. Na descrição do caso, a dentista realiza um acesso à polpa dentária e medicação (por dente) e um curativo de demora c/ ou s/ preparo biomecânico.
		<br />
	</div>
	<div class="feed">
		 Esta alternativa está errada. Na descrição do caso,a dentista realiza um acesso à polpa dentária e medicação (por dente) e um curativo de demora c/ ou s/ preparo biomecânico.
		<br />
	</div>
	<div class="feed">
		 Esta alternativa está errada. Na descrição do caso,a dentista realiza um acesso à polpa dentária e medicação (por dente) e um curativo de demora c/ ou s/ preparo biomecânico.
		<br />
	</div>
	<div class="feed">
		 Esta alternativa está errada. Na descrição do caso,a dentista realiza um acesso à polpa dentária e medicação (por dente) e um curativo de demora c/ ou s/ preparo biomecânico.
	</div>
</div>

<!-- inicio exercicio 4 -->
<b>4) Em relação ao campo “problema/condição avaliada”, quais campos o médico deveria marcar?

</b><br><br>


<div id="unid2_ex4"></div>

<script src="../unidades/unidade2/unid2_ex4.js" type="text/javascript"></script>
<br /><br />
<div class="box" id="boxunid2_ex4"></div>
<div class="feedback feedbackunid2_ex4">
	<p class="MsoNormal"></p>
	<div class="feed">
		Esta alternativa está errada. Dona Maria Ângela é tabagista e apresentava-se com a pressão arterial limítrofe e constantes dores de cabeça. Ela tem indicativos de hipertensão arterial. Os campos que deveriam ser marcados são hipertensão arterial e tabagismo. Apenas o marido da Dona Maria Ângela é usuário de drogas.
		<br />
	</div>
	<div class="feed">
		 Esta alternativa está errada. Dona Maria Ângela é tabagista e apresentava-se com a pressão arterial limítrofe e constantes dores de cabeça. Ela tem indicativos de hipertensão arterial. Os campos que deveriam ser marcados são hipertensão arterial e tabagismo.
		<br />
	</div>
	<div class="feed">
		 Esta alternativa está errada. Dona Maria Ângela é tabagista e apresentava-se com a pressão arterial limítrofe e constantes dores de cabeça. Ela tem indicativos de hipertensão arterial. Os campos que deveriam ser marcados são hipertensão arterial e tabagismo.
		<br />
	</div>
	<div class="feed">
		  Esta alternativa está correta. Dona Maria Ângela é tabagista e apresentava-se com a pressão arterial limítrofe e constantes dores de cabeça. Ela tem indicativos de hipertensão arterial. Os campos que deveriam ser marcados são hipertensão arterial e tabagismo.
	</div>
</div>
<br><br>
<h5>Caso 2</h5>
<!-- inicio exercicio 5 -->
<center><img src="../unidades/unidade2/imagens/estudo_de_caso02_u2.jpg" class="visible-desktop"></center>
<center><img src="../unidades/unidade2/imagens/estudo_de_caso02_u2.jpg" style="width: 100%;" class="hidden-desktop"></center><br><br>
Arthur, 25 anos, usuário de cadeira de rodas por possuir distrofia muscular, chega à Unidade Básica de Saúde relatando dor em dente incisivo central superior direito (elemento 11). O paciente foi acolhido na unidade de saúde pela enfermeira Marcela e direcionado para atendimento imediato pela dentista da equipe, Mariana.
Durante o atendimento de Arthur, a dentista realizou os seguintes procedimentos: 01 acesso à polpa dentária, 01 curativo de demora e o fornecimento de 01 escova e creme dental.
Após avaliação e atendimento supracitados, Arthur foi encaminhado para um atendimento com endodontista no Centro Especializado em Odontologia.
<br><br><b>1) Em relação ao campo “problema/condição avaliada”, quais campos o médico deveria marcar?

</b><br><br>


<div id="unid2_ex5"></div>

<script src="../unidades/unidade2/unid2_ex5.js" type="text/javascript"></script>
<br /><br />
<div class="box" id="boxunid2_ex5"></div>
<div class="feedback feedbackunid2_ex5">
	<p class="MsoNormal"></p>
	<div class="feed">
		Esta é a alternativa correta. A ficha de atendimento odontológico individual é uma ficha de coleta de dados que visa ao registro das informações do atendimento realizado pela equipe de Saúde Bucal na Atenção Básica (BRASIL, 2014a).
		<br />
	</div>
	<div class="feed">
		 Esta alternativa está errada. O nome correto da ficha é “ficha de procedimentos”, e não de procedimentos odontológicos. A resposta correta seria “ficha de atendimento odontológico individual”, que é uma ficha de coleta de dados que visa ao registro das informações do atendimento realizado pela equipe de Saúde Bucal na Atenção Básica (BRASIL, 2014a).
		<br />
	</div>
	<div class="feed">
		 Esta alternativa está errada. A resposta correta seria “ficha de atendimento odontológico individual”, que é uma ficha de coleta de dados que visa ao registro das informações do atendimento realizado pela equipe de Saúde Bucal na Atenção Básica (BRASIL, 2014a).
		<br />
	</div>
	<div class="feed">
		  Esta alternativa está errada. A resposta correta seria “ficha de atendimento odontológico individual”, que é uma ficha de coleta de dados que visa ao registro das informações do atendimento realizado pela equipe de Saúde Bucal na Atenção Básica (BRASIL, 2014a).
	</div>
</div>
<!-- inicio exercicio 6 -->
<b>2) Qual tipo de atendimento a enfermeira deve marcar na ficha de atendimento?

</b><br><br>


<div id="unid2_ex6"></div>

<script src="../unidades/unidade2/unid2_ex6.js" type="text/javascript"></script>
<br /><br />
<div class="box" id="boxunid2_ex6"></div>
<div class="feedback feedbackunid2_ex6">
	<p class="MsoNormal"></p>
	<div class="feed">
		Esta é a alternativa correta. A ficha de atendimento odontológico individual é uma ficha de coleta de dados que visa ao registro das informações do atendimento realizado pela equipe de Saúde Bucal na Atenção Básica (BRASIL, 2014a).
		<br />
	</div>
	<div class="feed">
		 Esta alternativa está errada. A dentista realizou procedimentos para minimizar a dor do paciente. A urgência nesse caso foi configurada pela necessidade de atendimento, impreterivelmente, no mesmo dia, com prioridades quanto à condição física e/ou vulnerabilidade psicossocial do paciente. Logo, o campo a ser marcado na ficha é o de “atendimento de urgência”.
		<br />
	</div>
	<div class="feed">
		  Esta alternativa está correta. A dentista realizou procedimentos para minimizar a dor do paciente. A urgência nesse caso foi configurada pela necessidade de atendimento, impreterivelmente, no mesmo dia, com prioridades quanto à condição física e/ou vulnerabilidade psicossocial do paciente. Logo, o campo a ser marcado na ficha é o de “atendimento de urgência”.
		<br />
	</div>
	<div class="feed">
		  Esta alternativa está errada. A dentista realizou procedimentos para minimizar a dor do paciente. A urgência nesse caso foi configurada pela necessidade de atendimento, impreterivelmente, no mesmo dia, com prioridades quanto à condição física e/ou vulnerabilidade psicossocial do paciente. Logo, o campo a ser marcado na ficha é o de “atendimento de urgência”.
	</div>
</div>
<!-- inicio exercicio 7 -->
<b>2) Qual tipo de atendimento a enfermeira deve marcar na ficha de atendimento?

</b><br><br>


<div id="unid2_ex7"></div>

<script src="../unidades/unidade2/unid2_ex7.js" type="text/javascript"></script>
<br /><br />
<div class="box" id="boxunid2_ex7"></div>
<div class="feedback feedbackunid2_ex7">
	<p class="MsoNormal"></p>
	<div class="feed">
		Esta alternativa está correta. Na descrição do caso, a dentista realiza um acesso à polpa dentária e medicação (por dente) e um curativo de demora c/ ou s/ preparo biomecânico.
		<br />
	</div>
	<div class="feed">
		 Esta alternativa está errada. Na descrição do caso,a dentista realiza um acesso à polpa dentária e medicação (por dente) e um curativo de demora c/ ou s/ preparo biomecânico.
		<br />
	</div>
	<div class="feed">
		  Esta alternativa está errada. Na descrição do caso,a dentista realiza um acesso à polpa dentária e medicação (por dente) e um curativo de demora c/ ou s/ preparo biomecânico.
		<br />
	</div>
	<div class="feed">
		  Esta alternativa está errada. Na descrição do caso,a dentista realiza um acesso à polpa dentária e medicação (por dente) e um curativo de demora c/ ou s/ preparo biomecânico.
	</div>
</div>
<br><br>
<table style="border: 0px; width: 100%;"><tbody><tr style="background-color: rgb(30, 118, 186);"><td style="padding-left: 20px; height: 48px;"><b><font color="#ffffff" size="4" style="position: relative; bottom: 10px;"><img src="http://ava.unasusufpe.com.br/moodle_interno/file.php/6/icones_caixas/icn_mais_u2.png" alt="Saiba mais" title="Saiba mais" style="position: relative; top: 12px;" /> Saiba mais</font></b></td></tr><tr style="background-color: rgb(165, 200, 227);"><td style="padding: 10px 10px 10px 20px;"><span style="background-color: rgb(165, 200, 227);"><font size="2">Quer estudar mais sobre o uso das fichas? Recomendamos que assista ao vídeo a seguir.<br /></font><font size="2"><a href="http://www.youtube.com/watch?v=IamMjgdimAY">http://www.youtube.com/watch?v=IamMjgdimAY</a> (GAETE, 2014).</font></span></td></tr></tbody></table>
<br><br>