<div id="ex1"></div><span style="color: #009797; font-weight: bold; font-size: 20px;">Exercícios</span><br><br>
<!-- inicio exercicio 1 -->

	<b>1) De acordo com o que você aprendeu na unidade, marque a alternativa que apresenta uma das características do PEC:</b><br>

<br/>
<div id="unid3_ex1"></div>
<script src="../unidades/unidade3/script.js" type="text/javascript"></script>
<script src="../unidades/unidade3/unid3_ex1.js" type="text/javascript"></script>
<br />
<div class="box" id="boxunid3_ex1"></div>
<div class="feedback feedbackunid3_ex1">
	<p class="MsoNormal"></p>
	<div class="feed">
		Esta alternativa está errada. Para a utilização do PEC, há sim a necessidade do acesso à internet. Apenas por meio do CDS, é possível a descrição utilizando formulários impressos.
		<br />
	</div>
	<div class="feed">
		Esta alternativa está correta.O PEC permite visualizar informações sobre o paciente, inclusive de atendimentos anteriores, caso o mesmo já esteja cadastrado.
		<br />
	</div>
	<div class="feed">
		Esta alternativa está errada. Com o uso do PEC, há necessidade de acesso à internet, e também há interação entre áreas e profissionais responsáveis pelo atendimento ao usuário.
		<br />
	</div>
	<div class="feed">
		 Esta alternativa está errada. É criado um PEC para cada profissional responsável pelo atendimento ao usuário, e ocorre sima integração total entre eles.
	</div>
</div>

<div id="ex2"></div>
<!-- inicio exercicio 2 -->

	<b>2) Em relação às diferenças entre SIAB e SISAB, analise a primeira coluna e, em seguida, na segunda coluna, selecione à qual sistema ela corresponde.</b>

<br><br>
a) Por meio deste sistema o tipo de registro é individualizado, e não consolidado.<br>
b) Por meio deste sistema, apenas os profissionais da Estratégia de Saúde da Família (ESF) e Equipes de Atenção Básica (participantes do PMAQ) podem realizar a alimentação dos dados. Outros profissionais de programas como Consultório na Rua, Atenção Domiciliar, NASF e Academia da Saúde não têm permissão para uso do sistema.<br>
c) O acompanhamento do território, neste sistema, é realizado por domicílio, núcleos familiares e indivíduos.<br>
d) Por meio deste sistema, os indicadores são fornecidos a partir da situação de saúde do território, atendimentos e acompanhamento dos indivíduos do território.<br>
e) Por meio deste sistema, os relatórios gerenciais são limitados aos dados consolidados, e não ocorrem de forma dinâmica.<br>
</p><p></p>

<div id="unid1_ex2"></div>

<!-- <script src="../unidades/unidade1/unid1_ex2.js" type="text/javascript"></script> -->
<br /><br />
<div class="box" id="boxunid1_ex2"></div>
<div class="feedback feedbackunid1_ex2">
	<p class="MsoNormal"></p>
	<div class="feed">
		Esta alternativa está errada. As afirmativas I, II e III estão corretas.
		<br />
	</div>
	<div class="feed">
		Esta alternativa está errada. As afirmativas I, II e III estão corretas.
		<br />
	</div>
	<div class="feed">
		Esta alternativa está errada. As afirmativas I, II e III estão corretas.
		<br />
	</div>
	<div class="feed">
		Esta é a alternativa correta. As afirmativas I, II e III estão corretas.
	</div>
</div>
<div id="ex3"></div>
<!-- inicio exercicio 3 -->

	<b>3) Sobre o uso do protocolo SOAP e CIAP-2 no PEC, analise se sentenças são (V) verdadeiras ou (F) falsas:

</b><br><br>
I. O PEC utiliza o protocolo SOAP como um componente importante do Registro Clínico Orientado por Problemas (RCOP), permitindo a padronização das notas clínicas e potencializando o trabalho e a comunicação em equipe de saúde. Ele visa a simplificar o registro das informações, sistematizando o processo de coleta de dados.<br>
II. A Classificação Internacional e Atenção Primária (CIAP-2) consiste em um sistema de classificação de problemas que substitui a necessidade do CID-10 como indicador de morbimortalidade, possuindo a vantagem de poder ser utilizado por toda a equipe de saúde.<br>
III. A CIAP-2 aparece no e-SUS AB como uma ferramenta importante que permite classificar não só os problemas diagnosticados pelos profissionais de saúde, mas principalmente os motivos das consultas e as intervenções que foram acordadas utilizando o protocolo SOAP para sistematização dos registros.<br>
</p><p></p><br><br>
Agora marque a alternativa que apresenta a sequência <b>correta</b>:
<br><br/>
<div id="unid1_ex3"></div>

<script src="../unidades/unidade1/unid1_ex3.js" type="text/javascript"></script>
<br /><br />
<div class="box" id="boxunid1_ex3"></div>
<div class="feedback feedbackunid1_ex3">
	<p class="MsoNormal"></p>
	<div class="feed">
		Esta alternativa está errada. O item II é falso. A Classificação Internacional e Atenção Primária (CIAP-2) consiste em um sistema de classificação de problemas (e não de doenças), que pode ser utilizada por todos os profissionais da Atenção Básica. Faz o melhor atendimento da incerteza, porém não substitui o CID-10, que continua sendo importante indicador de morbimortalidade.
		<br />
	</div>
	<div class="feed">
		Esta alternativa está errada. Os itens I e III são verdadeiros e o II é falso.<br><br>
I) O PEC tem como objetivo simplificar o registro das informações, sistematizando o processo de coleta de dados dos profissionais de saúde. E, utiliza o protocolo SOAP como um componente importante do Registro Clínico Orientado por Problemas (RCOP), originalmente criado para o ambiente hospitalar, mas que pode ser adaptado e utilizado por todos os profissionais da Atenção Básica, incluindo os ACS, permitindo a padronização das notas clínicas e potencializando o trabalho e a comunicação em equipe.<br>
II) A Classificação Internacional e Atenção Primária (CIAP-2) consiste em um sistema de classificação de problemas (e não de doenças), que pode ser utilizada por todos os profissionais da Atenção Básica. Faz o melhor atendimento da incerteza, porém não substitui o CID-10, que continua sendo importante indicador de morbimortalidade.<br>
III) A Classificação Internacional e Atenção Primária (CIAP-2) consiste em um sistema de classificação de problemas (e não de doenças), que pode ser utilizada por todos os profissionais da Atenção Básica. Faz o melhor atendimento da incerteza, porém não substitui o CID-10, que continua sendo importante indicador de morbimortalidade.<br>
	
	</div>
	<div class="feed">
		Esta alternativa está correta. Apenas o item II é falso. A Classificação Internacional e Atenção Primária (CIAP-2) consiste em um sistema de classificação de problemas (e não de doenças), que pode ser utilizada por todos os profissionais da Atenção Básica. Faz o melhor atendimento da incerteza, porém não substitui o CID-10, que continua sendo importante indicador de morbimortalidade.
		<br />
	</div>
	<div class="feed">
		Esta alternativa está errada. Os itens I e III são verdadeiros e o item II é falso.<br><br>
I) O PEC tem como objetivo simplificar o registro das informações, sistematizando o processo de coleta de dados dos profissionais de saúde. E, utiliza o protocolo SOAP como um componente importante do Registro Clínico Orientado por Problemas (RCOP), originalmente criado para o ambiente hospitalar, mas que pode ser adaptado e utilizado por todos os profissionais da Atenção Básica, incluindo os ACS, permitindo a padronização das notas clínicas e potencializando o trabalho e a comunicação em equipe.<br>
II) A Classificação Internacional e Atenção Primária (CIAP-2) consiste em um sistema de classificação de problemas (e não de doenças), que pode ser utilizada por todos os profissionais da Atenção Básica. Faz o melhor atendimento da incerteza, porém não substitui o CID-10, que continua sendo importante indicador de morbimortalidade.<br>
III) A Classificação Internacional e Atenção Primária (CIAP-2) consiste em um sistema de classificação de problemas (e não de doenças), que pode ser utilizada por todos os profissionais da Atenção Básica. Faz o melhor atendimento da incerteza, porém não substitui o CID-10, que continua sendo importante indicador de morbimortalidade. <br>
	</div>
</div>

<?php
	
	$ex = $_GET['ex'];

	if($ex == 1){

		echo '<script>$("html,body").animate({scrollTop: $("#ex1").offset().top}, 2500);</script>' ;


	}else if($ex == 2){

		echo '<script>$("html,body").animate({scrollTop: $("#ex2").offset().top}, 2500);</script>' ;

	}else if($ex == 3){
		echo '<script>$("html,body").animate({scrollTop: $("#ex3").offset().top}, 2500);</script>' ;
	}

?>
