var alternativas = new Array();
alternativas[0] = "Para a utilização do PEC, não há necessidade do acesso à internet. A descrição dos dados é realizada por meio de formulários impressos.";
alternativas[1] = "O PEC fornece possibilidade de visualização de informações sobre o paciente, inclusive de atendimentos anteriores, caso o mesmo já esteja cadastrado.";
alternativas[2] = "Há necessidade de acesso à internet, porém não há interação entre áreas e profissionais responsáveis pelo atendimento ao usuário.";
alternativas[3] = "Quando opta-se pelo uso do PEC, é criado um PEC para cada profissional responsável pelo atendimento ao usuário, não ocorrendo integração entre eles.";
var name = "unid3_ex1";gerarinputmultipla(alternativas, name, 1);