<div id="ex1"></div>

<span style="color: #8d55a1; font-weight: bold; font-size: 20px;">Exercícios</span>
<br><br>

<!-- inicio exercicio 1 -->
<b>1) Sobre o e-SUS e SISAB, analise as sentenças a seguir e marque (V) verdadeiro ou falso (F):</span></b><br><br>
	I. O novo Sistema de Informação em Saúde da Atenção Básica (SISAB) modernizou a plataforma tecnológica por meio do software e-SUS Atenção Básica (e-SUS AB). A grande mudança foi aprimorar o detalhamento das informações, permitindo o acompanhamento individualizado do usuário e as ações desenvolvidas pelos profissionais da equipe de saúde.<br>
II. O nome e-SUS faz referência a um Sistema Único de Saúde (SUS) eletrônico, que foi idealizado para permitir a modernização do sistema de gerenciamento de informações na área de saúde a fim de contribuir para a organização do trabalho dos profissionais de saúde, facilitando-a, e melhorar a qualidade da atenção à saúde prestada à população.<br>
III. O SISAB permite a interação dos diversos programas, contemplando os dados das equipes da AB, das equipes de Núcleos de Apoio à Saúde da Família (NASF), Consultórios na Rua (CnR), Programa Saúde na Escola (PSE), Programa Melhor em Casa e Academias da Saúde, além de outras modalidades de equipes e programas que porventura sejam incluídos na AB.<br>
</b>
<br>
<div id="asmaex1"></div>
<script src="../unidades/unidade1/script.js" type="text/javascript"></script>
<script src="../unidades/unidade1/unid1_ex1.js" type="text/javascript"></script>
<br /><br />
<div class="box" id="boxunid1_ex1"></div>
<div class="feedback feedbackunid1_ex1">
	<p class="MsoNormal"></p>
	<div class="feed">
		Esta alternativa está errada. As afirmativas I, II e III estão corretas.
		<br />
	</div>
	<div class="feed">
		Esta alternativa está errada. As afirmativas I, II e III estão corretas.
		<br />
	</div>
	<div class="feed">
		Esta alternativa está errada. As afirmativas I, II e III estão corretas.
		<br />
	</div>
	<div class="feed">
		Esta é a alternativa correta. As afirmativas I, II e III estão corretas.
	</div>
</div>

<div id="ex2"></div>
<!-- inicio exercicio 2 -->
<b>2) Em relação às diferenças entre SIAB e SISAB, analise a primeira coluna e, em seguida, na segunda coluna, selecione à qual sistema ela corresponde.
</b><br><br>
a) Por meio deste sistema o tipo de registro é individualizado, e não consolidado.<br>
b) Por meio deste sistema, apenas os profissionais da Estratégia de Saúde da Família (ESF) e Equipes de Atenção Básica (participantes do PMAQ) podem realizar a alimentação dos dados. Outros profissionais de programas como Consultório na Rua, Atenção Domiciliar, NASF e Academia da Saúde não têm permissão para uso do sistema.<br>
c) O acompanhamento do território, neste sistema, é realizado por domicílio, núcleos familiares e indivíduos.<br>
d) Por meio deste sistema, os indicadores são fornecidos a partir da situação de saúde do território, atendimentos e acompanhamento dos indivíduos do território.<br>
e) Por meio deste sistema, os relatórios gerenciais são limitados aos dados consolidados, e não ocorrem de forma dinâmica.<br>
</p><p></p>
<br>
<div id="unid1_ex2"></div>

<script src="../unidades/unidade1/unid1_ex2.js" type="text/javascript"></script>
<br /><br />
<div class="box" id="boxunid1_ex2"></div>
<div class="feedback feedbackunid1_ex2">
	<p class="MsoNormal"></p>
	<div class="feed">
		Esta alternativa está errada. As afirmativas I, II e III estão corretas.
		<br />
	</div>
	<div class="feed">
		Esta alternativa está errada. As afirmativas I, II e III estão corretas.
		<br />
	</div>
	<div class="feed">
		Esta alternativa está errada. As afirmativas I, II e III estão corretas.
		<br />
	</div>
	<div class="feed">
		Esta é a alternativa correta. As afirmativas I, II e III estão corretas.
	</div>
</div>

<div id="ex3"></div>
<!-- inicio exercicio 3 -->
<b>3) Quais os passos necessários para a implantação do e-SUS AB no seu município? Marque a alternativa que apresenta a sequência correta.
</b><br><br>
1. Articular e viabilizar equipes de suporte à informatização nos estabelecimentos e elaborar estratégia de implantação, contemplando o dimensionamento da quantidade de usuários vinculados às equipes da AB, para cadastro dos usuários.<br>
2. Garantir a integração do planejamento local ao plano de ação regionale viabilizar a qualificação de profissionais para capacitar os profissionais de saúde e de tecnologia da informação do município.<br>
3. Identificar profissionais habilitados para a coordenação da implantação do sistema e realizar diagnóstico situacional da capacidade dos recursos humanos existentes nos estados e municípios.<br>
4. Realizar o levantamento da capacidade tecnológica disponível em cada Unidade Básica de Saúde (UBS), nas Secretarias Municipais de Saúde (SMS) e na Secretaria de Estado de Saúde (SES);da necessidade de computadores e de impressoras, de acordo com a quantidade de ambientes e conectividade à internet.<br>
</p><p></p>
<br>
<div id="unid1_ex3"></div>

<script src="../unidades/unidade1/unid1_ex3.js" type="text/javascript"></script>
<br /><br />
<div class="box" id="boxunid1_ex3"></div>
<div class="feedback feedbackunid1_ex3">
	<p class="MsoNormal"></p>
	<div class="feed">
		Esta alternativa está errada. A sequência correta é 4, 3, 1, 2.
		<br />
	</div>
	<div class="feed">
		Esta é a alternativa correta. A sequência correta é 4, 3, 1, 2.
		<br />
	</div>
	<div class="feed">
		Esta alternativa está errada. A sequência correta é 4, 3, 1, 2.
		<br />
	</div>
	<div class="feed">
		Esta alternativa está errada. A sequência correta é 4, 3, 1, 2.
	</div>
</div>
<?php
	
	$ex = $_GET['ex'];

	if($ex == 1){

		echo '<script>$("html,body").animate({scrollTop: $("#ex1").offset().top}, 2500);</script>' ;


	}else if($ex == 2){

		echo '<script>$("html,body").animate({scrollTop: $("#ex2").offset().top}, 2500);</script>' ;

	}else if($ex == 3){
		echo '<script>$("html,body").animate({scrollTop: $("#ex3").offset().top}, 2500);</script>' ;
	}

?>

