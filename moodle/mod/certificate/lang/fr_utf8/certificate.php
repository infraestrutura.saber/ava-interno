<?PHP // $Id: certificate.php,v 3.1.9 2008/01/20

//General functions
$string['modulename'] = 'Certificat';
$string['modulenameplural'] = 'Certificats';
$string['certificatename'] = 'Nom du Certificat';
$string['certificate:view'] = 'Voir Certificats';
$string['certificate:manage'] = 'G�rer les certificats';
$string['certificate:printteacher'] = 'Imprimer Professeur';
$string['certificate:student'] = 'Obtenir Certificats';

//Adding an instance
$string['intro'] = 'Introduction';
$string['addlinklabel'] = 'Ajouter un autre lien d\'option d\'activit�';
$string['addlinktitle'] = 'Cliquer pour ajouter un autre lien d\'option d\'activit�';
$string['issueoptions'] = 'Options des questions';
$string['textoptions'] = 'Options de texte';
$string['designoptions'] = 'Options graphiques';
$string['lockingoptions'] = 'Options de verrouillage';
$string['certificatetype'] = 'Type de certificat';
$string['emailteachers'] = 'Courriels des professeurs';
$string['emailothers'] = 'Autres courriels';
$string['savecertificate'] = 'Enregistrement de certificat';
$string['deliver'] = 'Livraison';
$string['download'] = 'T�l�chargement obligatoire';
$string['openbrowser'] = 'Ouvrir dans une nouvelle fen�tre';
$string['emailcertificate'] = 'Courriel (Doit �galement choisir enregistrer!)';
$string['border'] = 'Bordure';
$string['borderstyle'] = 'Bordure d\'image';
$string['borderlines'] = 'Lignes';
$string['bordercolor'] = 'Bordure de lignes';
$string['borderblack'] = 'Noir';
$string['borderbrown'] = 'Brun';
$string['borderblue'] = 'Bleu';
$string['bordergreen'] = 'Vert';
$string['printwmark'] = 'Filigrane d\'image';
$string['datehelp'] = 'Date';
$string['dateformat'] = 'Format date';
$string['userdateformat'] = 'Format de date';
$string['receiveddate'] = 'Date de r�ception';
$string['courseenddate'] = 'Date de fin du cours (doit �tre d�fini!)';
$string['gradedate'] = 'Date de la note';
$string['printcode'] = 'Impression de code';
$string['printgrade'] = 'Impression de note';
$string['printoutcome'] = 'R�sultats d\'impression';
$string['nogrades'] = 'Aucune note disponible';
$string['gradeformat'] = 'Format de note';
$string['gradepercent'] = 'Note en pourecentage';
$string['gradepoints'] = 'Note en points';
$string['gradeletter'] = 'Note en lettre';
$string['printhours'] = 'Imprimer heure(s) cr�dit�e(s)';
$string['printsignature'] = 'Image de la signature';
$string['sigline'] = 'ligne';
$string['printteacher'] = 'Imprimer le(s) nom(s) du(des) professeur(s)';
$string['customtext'] = 'Texte personnalis�';
$string['printdate'] = 'Date d\'impression';
$string['printseal'] = 'Cachet ou Logo';
$string['lockgrade'] = 'Verrouiller par note';
$string['requiredgrade'] = 'Note de cours requise';
$string['coursetime'] = 'Temps requis pour le cours ';
$string['linkedactivity'] = 'Lien d\'activit�';
$string['minimumgrade'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Note requise';
$string['activitylocklabel'] = 'Lien d\'activit�/Note minimum %';
$string['coursetimedependency'] = 'Minutes minimum requises en cours';
$string['activitydependencies'] = 'Charge d\'activit�';

//Strings for verification block
$string['entercode'] = 'Entrer le code de v�rification du certificat :';
$string['validate'] = 'V�rifier';
$string['certificate'] = 'V�rification du code de certificat:';
$string['verifycertificate'] = 'V�rifier le certificat';
$string['notfound'] = 'Le num�ro du certificat n\'a  pas pu �tre valid�.';
$string['back'] = 'Retour';
$string['to'] = 'Attribu� �';
$string['course'] = 'Pour';
$string['date'] = 'Au';
$string['grade'] = 'Niveau';

//Certificate view, index, report strings
$string['incompletemessage'] = 'Afin de t�l�charger votre certificat, vous devez d\'abord remplir tous les champs '.'activit�s. Pri�re de revenir au cours pour compl�ter vos travaux.';
$string['awardedto'] = 'Attribu� �';
$string['issued'] = 'Publi�';
$string['notissued'] = 'Non publi�';
$string['notissuedyet'] = 'Pas encore publi�';
$string['notreceived'] = 'Vous n\'avez pas re�u ce certificat';
$string['getcertificate'] = 'Obtenez votre certificat';
$string['report'] = 'Rapport';
$string['code'] = 'Code';
$string['viewed'] = 'Vous avez re�u ce certificat pour:';
$string['viewcertificateviews'] = 'Visualiser $a certificat(s) �mis';
$string['reviewcertificate'] = 'Revoir votre certificat d\'examen';
$string['openwindow'] = 'Cliquer sur le bouton ci-dessous pour ouvrir votre certificat dans une nouvelle fen�tre.';
$string['opendownload'] = 'Cliquer sur le bouton ci-dessous pour enregistrer votre certificat sur votre ordinateur.';
$string['openemail'] = 'Cliquer sur le bouton ci-dessous et votre certificat sera envoy� en pi�ce jointe.';
$string['receivedcerts'] = 'Certificats re�us';
$string['errorlockgrade'] = 'Votre niveau actuel sur $a->mod ($a->current%%) est inf�rieur au niveau requis ($a->needed%%) pour recevoir votre certificat.';
$string['errorlocksurvey'] = 'Vous devez compl�ter le questionnaire avant de recevoir votre certificat.';
$string['errorlockgradecourse'] = 'Votre note de cours actuelle ($a->current%%) est inf�rieure � la note requise ($a->needed%%) pour recevoir votre certificat.';
$string['errorlocktime'] = 'Vous devez d\'abord r�pondre aux exigences de temps pass� � travailler dans ce cours avant de recevoir votre certificat.';
$string['errorlockmod'] = 'Vous devez d\'abord r�pondre aux exigences de note de toutes les activit�s de cours avant de recevoir votre certificat.';

//Email text
$string['emailstudenttext'] = 'Ci-joint votre certificat de $a->course.';
$string['awarded'] = 'D�cern�';
$string['emailteachermail'] = '$a->l\'�tudiant(e) a re�u son certificat: \'$a->certificat\'pour $a->course.
Vous pouvez le revoir ici: $a->url';
$string['emailteachermailhtml'] = '$a->l\'�tudiant(e) a re�u son certificat: \'<i>$a->certificat(s)</i>\'pour $a->course.
Vous pouvez le revoir ici:<a href=\"$a->url\">Rapport de certificats</a>.';

//Names of type folders
$string['typeportrait'] = 'Portrait';
$string['typeletter_portrait'] = 'Portrait (lettre)';
$string['typelandscape'] = 'Paysage';
$string['typeletter_landscape'] = 'Paysage (lettre)';
$string['typeunicode_landscape'] = 'Unicode (paysage)';
$string['typeunicode_portrait'] = 'Unicode (portrait)';

//Print to certificate strings
$string['grade'] = 'Note';
$string['coursegrade'] = 'Note de cours';
$string['credithours'] = 'Heures cr�dit�es';

$string['titlelandscape'] = 'CERTIFICAT de R�USSITE';
$string['introlandscape'] = 'Ceci est pour certifier que';
$string['statementlandscape'] = 'a termin� le cours';

$string['titleletterlandscape'] = 'CERTIFICAT de R�USSITE';
$string['introletterlandscape'] = 'Ceci est pour certifier que';
$string['statementletterlandscape'] = 'a termin� le cours';

$string['titleportrait'] = 'CERTIFICAT de R�USSITE';
$string['introportrait'] = 'Ceci est pour certifier que';
$string['statementportrait'] = 'a termin� le cours';
$string['ondayportrait'] = 'ce jour';

$string['titleletterportrait'] = 'CERTIFICAT de R�USSITE';
$string['introletterportrait'] = 'a termin� le cours';
$string['statementletterportrait'] = 'a termin� le cours';

//Certificate transcript strings
$string['notapplicable'] = 'S/O';
$string['certificatesfor'] = 'Certificats pour';
$string['coursename'] = 'Cours';
$string['viewtranscript'] = 'Afficher les certificats';
$string['mycertificates'] = 'Mes Certificats';
$string['nocertificatesreceived'] = 'n\'a pas de certificat de cours re�us.';
$string['notissued'] = 'Non �mis';
$string['reportcertificate'] = 'Rapport de certificats';
$string['certificatereport'] = 'Rapport de certificats';
$string['printerfriendly'] = 'Page imprimable';
?>