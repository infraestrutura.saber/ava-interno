<?php
require_once('../../config.php');
require_once($CFG->libdir.'/dmllib.php');
global $CFG, $USER, $COURSE;

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from view.php in mod/tracker
}

$usergrade = get_field_select("grade_grades","finalgrade","userid=$USER->id and itemid=(SELECT id FROM mdl_grade_items where courseid=$COURSE->id and itemtype='course')");

if ($usergrade < 7) {
    print_error('O aluno não atingiu a nota mínima para a obtenção do certificado.');
}

//Print the html text

$mes_extenso = array(
        'Jan' => 'janeiro',
        'Feb' => 'fevereiro',
        'Mar' => 'março',
        'Apr' => 'abril',
        'May' => 'maio',
        'Jun' => 'junho',
        'Jul' => 'julho',
        'Aug' => 'agosto',
        'Nov' => 'novembro',
        'Sep' => 'setembro',
        'Oct' => 'outubro',
        'Dec' => 'dezembro'
    );

/*$customtext ='
    <p>Declaramos, para os devidos fins, que Tomas Vitor de Souza Gama Queiroz Teixeira, portador(a) do CPF <p/><p>013.473.774-17, concluiu o curso <i>Capacitação para Implantação do e-SUS na Atenção Básica'.''.',</i> com <p/><p>carga horária de 30 horas, ofertado na modalidade a distância pela Universidade Aberta do Sistema <p/><p>Único de Saúde | Universidade Federal de Pernambuco em parceria com a Secretaria Estadual de <p/><p>Saúde de Pernambuco.</p>
    <p><p/>';*/

$customtext ='
        Declaramos, para os devidos fins, que '.$USER->firstname.' '.$USER->lastname.', portador(a) do CPF '.substr($USER->username, 0,3).'.'.substr($USER->username, 3,3).'.'.substr($USER->username, 6,3).'-'.substr($USER->username, 9,2).
        ', concluiu o curso <i> Capacitação para Implantação do e-SUS na Atenção Básica, </i> com carga horária de 30 horas, ofertado na modalidade a distância pela Universidade Aberta do Sistema Único de Saúde | Universidade Federal de Pernambuco em parceria com a Secretaria Estadual de Saúde de Pernambuco.'
    ;

$paragraphedText = '<p>';
$lineLength = 0;

foreach (explode(' ', $customtext) as $word) {
    $paragraphedText .= $word.' ';
    if (!($word == '<i>' || $word == '</i>'))
        $lineLength += strlen($word) + 1;
    if ($lineLength >= 92)
    {
        $lineLength = 0;
        $paragraphedText .= '</p><p>';
    }
}

$customtext = utf8_decode("<div>".str_replace('i> ', 'i>', $paragraphedText)."</p></div>");

//Add pdf page    
    $pdf = new PDF("P", 'pt', 'A4');
    $pdf->SetProtection(array('print'));
    $pdf->AddPage();
    if(ini_get('magic_quotes_gpc')=='1')
        $customtext=stripslashes($customtext);

    cert_printtext(45, 150, 'C', 'Helvetica', 'B', 14, utf8_decode('Capacitação para Implantação do e-SUS na Atenção Básica'));
    cert_printtext(45, 190, 'C', 'Helvetica', 'B', 13, utf8_decode('D E C L A R A Ç Ã O'));

    $pdf->SetTextColor(0,0,0);      

    cert_printtext(45, 375, 'C', 'Helvetica', '', 10, 'Recife (PE), '.date('d').' de '.$mes_extenso[date('M')].' de '.date('Y'));    
    cert_printtext(45, 400, 'C', 'Helvetica', '', 10, 'Atenciosamente,');      

    cert_printtext(58, 220, '', '', '', '', '');
    $pdf->SetLeftMargin(65);
    $pdf->WriteHTML($customtext);    
    
    $pdf->setLineWidth(2);
    $pdf->setDrawColor(54, 95, 145);    
    $pdf->Line(0, 80, 1800, 80);   
   
    cert_printtext(43, 450, 'C', 'times', '', 10, '_______________________________________                   _______________________________________ ');
    cert_printtext(45, 465, 'C', 'times', 'B', 10, utf8_decode('Cristine Martins Gomes Gusmão                                       Maria Francisca Santos de Carvalho'));
    cert_printtext(40, 480, 'C', 'times', '', 10, utf8_decode('Coordenação Geral da UNA-SUS UFPE                                   Superintendência da Atenção Primária'));    

    $pdf->SetFont('helvetica', '', 8, '', 'false');
    $pdf->SetTextColor(79, 129, 189);  

    cert_printtext(45, 760, 'C', 'Helvetica', '', 10, utf8_decode('Centro de Convenções da UFPE, Espaço EAD | Avenida dos Reitores, s/n, Cidade Universitária'));
    cert_printtext(45, 775, 'C', 'Helvetica', '', 10, utf8_decode('Recife - PE,CEP 50741-000 | e-mail: unasus.ufpe@gmail.com| Telefone: +55 81 2126-8426'));    
?>