<?php
require_once("config.php");
require_once($CFG->libdir.'/dmllib.php');
global $USER, $CFG;

require_login($courseid);

$title = 'Registro CNES';

$navigation = build_navigation(array(array('name'=>'Registro CNES', 'link'=>null, 'type'=>'misc')));
print_header('Registro CNES', 'Registro CNES', $navigation, "", "", false, "&nbsp;", user_login_string());

if(!is_siteadmin($USER->id))
	print_error('badpermissions');

if (!empty($_POST['searchuser'])) { //Busca de Usuário
	
	if (isset($_POST['btnSearch_cnes'])){ //buscando a informação na base mdl_retrato_cnes		
    	$searchresult = get_records_sql('select c.name, cpf, coalesce(email,"-") as email, coalesce(case lastaccess when 0 then \'nunca\' 
    		else DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(lastaccess),\'+00:00\',\'-03:00\'), \'%d/%m/%y - %k:%i\') end,"-") 
    	as lastaccess from mdl_retrato_cnes c left join mdl_user u on c.cpf=u.username where name like "%'.$_POST["searchuser"].'%" or cpf="'.$_POST["searchuser"].'"');     	    	
	}
	else if (isset($_POST['btnSearch_moodle'])){ //buscando a informação na base mdl_user			    
	    $searchresult = get_records_sql('select concat(firstname," ",lastname) as name, username as cpf, email, case lastaccess when 0 then \'nunca\' else DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(lastaccess),\'+00:00\',\'-03:00\'), \'%d/%m/%y - %k:%i\') end as lastaccess from mdl_user where concat(firstname," ",lastname) like "%'.$_POST["searchuser"].'%" or username="'.$_POST["searchuser"].'"'); 	   	    
	}	
	
	echo '<div id="searchresult" style="margin-top:40px">';
	if (!empty($searchresult))
	{	
		echo '<table class="table table-striped"  cellpadding="3"><thead style="background-color:#ededed;"><th>Nome</th><th>CPF</th><th>E-mail</th><th>Último Acesso</th></thead><tbody>';			
		foreach ($searchresult as $result) {
			echo "<tr><td>".$result->name."</td><td>".$result->cpf."</td><td>".$result->email."</td><td>".$result->lastaccess."</td><tr/>";
		}		
		echo '</tbody></table>';
	}
	else
		echo "<strong>Nenhum registro encontrado</strong><br/>";	

	echo '</div>';
	echo "<br/><br/><button onclick=\"window.history.go(-1)\">Voltar</button>";
}

elseif (!empty($_POST['searchemail'])) {  //Busca email usuario
	
	if (isset($_POST['btnSearch_email'])) {
		$searchemail = get_records_sql('select i.itemname, g.finalgrade, u.email, us.cpf as cpf_aluno, us.certificado, u.firstname, u.id, u.email as mail, u.firstname as nome, g.finalgrade as nota
			FROM mdl_grade_items i
			INNER JOIN mdl_grade_grades g ON i.id = g.itemid
			INNER JOIN mdl_user u ON u.id = g.userid
			INNER JOIN mdl_usuario_ad2 us ON u.username =us.cpf
			WHERE i.courseid =2
			AND g.itemid =24
            AND us.cpf="'.$_POST['searchemail'].'" ');


		// 'select i.itemname, g.finalgrade, u.email, us.cpf as cpf_aluno, us.certificado, u.firstname, u.id, u.email as mail, u.firstname as nome, g.finalgrade as nota
		// 	FROM mdl_grade_items i
		// 	INNER JOIN mdl_grade_grades g ON i.id = g.itemid
		// 	INNER JOIN mdl_user u ON u.id = g.userid
		// 	INNER JOIN mdl_usuario_ad2 us ON u.username =us.cpf
		// 	WHERE i.courseid =2
		// 	AND g.userid =65
		// 	AND g.itemid =24
  //           AND us.cpf="'.$_POST['searchemail'].'" '
	}
	if (!empty($searchemail)) {
		foreach ($searchemail as $result) {
			//$result->nota >= 7
			if ($result->nota >= 70) {
				echo "aluno esta na media!!!";
				echo "nome: " .$result->nome ." Nota: " .$result->nota  ."Email:". $result->mail;	

				$email = $result->mail;
				

				$headers = "MIME-Version: 1.1\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1\n";
				

				$headers .= "From: UNASUS UFPE <email-smtp.us-east-1.amazonaws.com>"."\n"; // remetente
				$headers .= "Return-Path: UNASUS UFPE <email-smtp.us-east-1.amazonaws.com>"."\n"; // return-path
				


				$message2 = "<a href=''>clique aqui</a>";
				$message = '<p>Parabéns, '.$result->nome.' </p>';
				$message .= '<p>Você receberá seu certificado do curso e-SUS! </p>';
				$message .= "<a href='http://ava.unasusufpe.com.br/moodle_unasus/cursos/e-sus/'>clique aqui</a>";
				$message .= "para imprimir seu certificado.";
				$envio = mail($email, "Assunto - Certificado e-SUS ", $message, $headers);
				//$envio = mail("mirelanatali@gmail.com", "Assunto - Certificado e-SUS ", $message, $headers, "-r"."mirelanatali@gmail.com");
				 
				if($envio){
					//echo "Email OK enviada com sucesso";
					echo "<script>alert('Mensagem enviada.')</script>";
					
				}else{
					echo "<script>alert('Mensagem Nao enviada.')</script>";				
				}

			}else{
				echo $USER->firstname.  ', o aluno <b>'.$result->nome.' </b> de CPF: <b>'.$result->cpf_aluno.'</b>, não atingiu a média do curso!';

				echo "<br>";
				echo "<a class='btn' href='http://www.sabertecnologias.com.br/moodle_interno/cnes.php'>voltar</a>";

				
			}
			
		}
	}
}
elseif (isset($_POST['btnSearch_emailAll'])) {

		$searchemailAll = get_records_sql('select i.itemname, g.finalgrade, u.email, us.cpf, us.certificado, u.firstname, u.id, u.email as mail, u.firstname as nome, g.finalgrade as nota
			FROM mdl_grade_items i
			INNER JOIN mdl_grade_grades g ON i.id = g.itemid
			INNER JOIN mdl_user u ON u.id = g.userid
			INNER JOIN mdl_usuario_ad2 us ON u.username =us.cpf
			WHERE i.courseid = 2
			AND g.finalgrade >70
			
			 ');

	
	if (!empty($searchemailAll)) {
		foreach ($searchemailAll as $result) {
			echo "E-mail: " .$result->mail  ."Nome: ". $result->nome;

							
				$email = $result->mail;
				//$email = "mirelanatali@gmail.com";

				$headers = "MIME-Version: 1.1\n";
				$headers .= "Content-type: text/html; charset=iso-8859-1\n";
				

				$headers .= "From: UNASUS UFPE <email-smtp.us-east-1.amazonaws.com>"."\n"; // remetente
				$headers .= "Return-Path: UNASUS UFPE <email-smtp.us-east-1.amazonaws.com>"."\n"; // return-path
				
				$message2 = "<a href=''>clique aqui</a>";
				$message = '<p>Parabéns, '.$result->nome.' </p>';
				$message .= '<p>Você receberá seu certificado do curso e-SUS! </p>';
				$message .= "<a href='http://ava.unasusufpe.com.br/moodle_unasus/cursos/e-sus/'>clique aqui</a>";
				$message .= "para imprimir seu certificado.";
				$envio = mail($email, "Assunto - Certificado e-SUS ", $message, $headers);
				//$envio = mail("mirelanatali@gmail.com", "Assunto - Certificado e-SUS ", $message, $headers, "-r"."mirelanatali@gmail.com");
				 
				if($envio){
					//echo "Email OK enviada com sucesso";
					echo "<script>alert('Mensagem enviada a todos os alunos!.')</script>";
					
				}else{
					echo "<script>alert('Mensagem Nao pode ser enviada.')</script>";				
				}


		}
	}

	
}

else if (!empty($_POST['name'])) { //Registro de Usuário
	$record = new stdClass();
	$record->name         = $_POST["name"]; 
	$record->cpf = $_POST["cpf"];
	$record->justification = $_POST["justification"];
	$record->occupation = $_POST["occupation"];
	$record->creatorid = $USER->id;
	$record->created = date('Y-m-d H:i:s');	
	
	if (testaCPF($record->cpf))
	{ 
		if (insert_record('retrato_cnes', $record))
		{
			add_to_log(1, 'cnes', 'user registered', 'cnes.php', $record->cpf);
			echo '<p style="margin-top:3.5em">Usu&aacute;rio inserido com sucesso.</p><br/>';		
			redirect($CFG->wwwroot.'/cnes.php');	
		}
		else	
		{
			echo '<p style="margin-top:3.5em">O usu&aacute;rio não p&ocirc;de ser inserido.</p><br/>';	
			redirect($CFG->wwwroot.'/cnes.php');
		}	
	}		
}
else if (!empty($_POST['deleteuser'])) { //Remoção de Usuário
	$userToDelete = get_field_select('retrato_cnes','name', 'cpf='.$_POST['deleteuser']);
	if (!$userToDelete) 
		echo "<p style=\"margin-top:3.5em\">Não foi encontrado usuário com o CPF informado.</p><br/><br/><button onclick=\"window.history.go(-1)\">Voltar</button>";			
	else
	{
		delete_records_select('retrato_cnes', 'cpf='.$_POST['deleteuser']);
		add_to_log(1, 'cnes', 'user deleted', 'cnes.php', $_POST['deleteuser']);
		echo '<p style="margin-top:3.5em">Usu&aacute;rio removido com sucesso.</p><br/>';		
		redirect($CFG->wwwroot.'/cnes.php');
	}
}
else {
?>
	<form id="cnes_form_register" action=<?php echo '"'.$CFG->wwwroot.'/cnes.php"';?> method="post" style="margin-top:40px">
		<fieldset style="border:3px groove;padding:20px;">
			<legend>Registro de Usu&aacute;rio</legend>
			<label for="name">Nome:</label>
			<input type="text" id="name" name="name"><br/>
			<label for="cpf">CPF:</label>
			<input type="text" id="cpf" name="cpf" /><br/>
			<label for="justification">Justificativa:</label>
			<textarea rows="4" cols="50" id="justification" name="justification"></textarea><br/><br/>		
			<label for="occupation">Ocupa&ccedil;&atilde;o:</label>
			<select id="occupation" name="occupation">
				<option value=''>Selecione uma ocupação...</option>

<?php
	$occupations = array('Agentes Comunitários de Saúde','Agentes de Combate às Endemias','Assistentes sociais','Auxiliares de Enfermagem','Auxiliares em Saúde Bucal','Biólogos','Biomédicos','Enfermeiros','Farmacêuticos',
						 'Fisioterapeutas','Fonoaudiólogos','Gestores estaduais','Gestores municipais', 'Médicos','Médicos veterinários','Nutricionistas','Odontólogos','Outros','Profissionais de educação física','Profissionais de informática','Psicólogos','Técnicos de Enfermagem','Técnicos em Patologia Clínica',
						 'Técnicos em Radiologia','Técnicos em Saúde Bucal','Técnicos em Segurança do Trabalho','Terapeutas ocupacionais');

	foreach ($occupations as $occ) {
		echo "<option value='$occ'>$occ</option>'";	
	}
?>
			</select><br/>	
			<input type="submit" id="btnSave" value="Salvar"/>
		</fieldset>
	</form>
	<br/><br/><br/>
	<form id="cnes_form_search" action=<?php echo '"'.$CFG->wwwroot.'/cnes.php"';?> method="post">
		<fieldset style="border:3px groove;padding:20px;">
			<legend>Busca de Usu&aacute;rio</legend>			
			<label for="searchuser">Nome ou CPF:</label>
			<input type="text" id="searchuser" name="searchuser"/><br/><br/>
			<input type="submit" id="btnSearch_cnes" name="btnSearch_cnes" value="Buscar na Base CNES" />
			<input type="submit" id="btnSearch_moodle" name="btnSearch_moodle" value="Buscar na Base Moodle" />
		</fieldset>
	</form>
	<br/><br/><br/>
	<form id="cnes_form_delete" action=<?php echo '"'.$CFG->wwwroot.'/cnes.php"';?> method="post">
		<fieldset style="border:3px groove;padding:20px;">
			<legend>Remoção de Usu&aacute;rio</legend>			
			<label for="deleteuser">CPF do usuário a ser removido:</label>
			<input type="text" id="deleteuser" name="deleteuser"/><br/><br/>
			<input type="submit" style="display:none" id="btnSearch" class="deletar_usuario" value="Remover Usu&aacute;rio" />
			<input type="button" class="remover" value="Remover Usu&aacute;rio" />
			
		</fieldset>
	</form>
		<!-- modal -->
	<div class="modal" style="display: none" id="myModal" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">×</button>
			<h3 id="myModalLabel">Remoção de Usuário</h3>
		</div>
		<div class="modal-body">
			<p><?php echo $USER->firstname ?>, deseja realmente excluir o usuário de CPF <span class="registro"></span>?</p>
		</div>
		<div class="modal-footer">
			<a href="" class="btn nao" data-dismiss="modal" aria-hidden="true">Não</a>
			<a href="" class="btn sim" data-dismiss="modal" aria-hidden="true">Sim</a>

		</div>
	</div>
	<br/><br/><br/>
	<form id="cnes_form_search" action=<?php echo '"'.$CFG->wwwroot.'/cnes.php"';?> method="post">
		<fieldset style="border:3px groove;padding:20px;">
			<legend>Enviar Certificado e-SUS</legend>			
			<label for="searchemail">Nome ou CPF:</label>
			<input type="text" id="searchemail" name="searchemail"/><br/><br/>
			<input type="submit" id="btnSearch_email" name="btnSearch_email" value="Enviar para aluno" />
			<input type="submit" id="btnSearch_emailAll" name="btnSearch_emailAll" value="Enviar para todos os alunos" />
		</fieldset>
	</form>


	<!-- fim modal -->
	<script type="text/javascript">

	$('.remover').click(function(){
		$('.registro').html($("#deleteuser").val());	
		$('.modal').modal('show');
		
	});
	$('.sim').click(function(){
		$('.deletar_usuario').click();
	});
	$('.nao').click(function(){
		$('.modal').modal('hide');
	});
	</script>
<?php 
}
	print_footer('none');

	/**Funções auxiliares**/

	function testaCPF($cpf) {
	
		$error;
		//O CPF deve ser válido
		if (!validateCPF($cpf)) {
			$error = 'CPF inválido';
		}

		//O CPF deve ser único na tabela
		if (get_field_select('retrato_cnes','name', 'cpf='.$cpf)) 
			$error = 'J&aacute; existe usu&aacute;rio com o CPF informado.';
		
		if (isset($error))
			echo "<p style=\"margin-top:3.5em\">$error</p><br/><br/><button onclick=\"window.history.go(-1)\">Voltar</button>";			

		return empty($error);
	}

	function validateCPF($cpf) {		
		
		$soma = 0;
		$resto;		
		
		// verifica se possui 11 números
		if(!(is_numeric($cpf) && strlen($cpf) == 11)) {
			return false;
		}		

		// verifica se está usando a repetição de um numero
		if( ($cpf == '11111111111') || ($cpf == '22222222222') || ($cpf == '33333333333') || ($cpf == '44444444444') || 
			($cpf == '55555555555') || ($cpf == '66666666666') || ($cpf == '77777777777') || ($cpf == '88888888888') || 
			($cpf == '99999999999') || ($cpf == '00000000000') ) {
			return false;
		}
		 
		//PEGA O DIGITO VERIFICADOR
		$dv_informado = substr($cpf, 9,2);

		for($i=0; $i<=8; $i++) {
			$digito[$i] = substr($cpf, $i,1);
		}

		//CALCULA O VALOR DO 10º DIGITO DE VERIFICAÇÂO
		$posicao = 10;
		$soma = 0;

		for($i=0; $i<=8; $i++) {
			$soma = $soma + $digito[$i] * $posicao;
			$posicao = $posicao - 1;
		}

		$digito[9] = $soma % 11;

		if($digito[9] < 2) {
			$digito[9] = 0;
		} 
		else {
			$digito[9] = 11 - $digito[9];
		}

		//CALCULA O VALOR DO 11º DIGITO DE VERIFICAÇÃO
		$posicao = 11;
		$soma = 0;

		for ($i=0; $i<=9; $i++) {
			$soma = $soma + $digito[$i] * $posicao;
			$posicao = $posicao - 1;
		}

		$digito[10] = $soma % 11;

		if ($digito[10] < 2) {
			$digito[10] = 0;
		}
		else {
			$digito[10] = 11 - $digito[10];
		}

		//VERIFICA SE O DV CALCULADO É IGUAL AO INFORMADO
		$dv = $digito[9] * 10 + $digito[10];
		if ($dv != $dv_informado) {
			return false;
		}

		return true;
	}
	
?>
