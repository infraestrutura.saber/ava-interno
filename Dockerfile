
FROM tutum/apache-php

LABEL maintainer="infraestrutura SABER <infraestrutura.saber@gmail.com>"
LABEL description="Moodle Interno da UNASUS"
LABEL version="1.0.0"

ENV DEBIAN_FRONTEND=noninteractive

COPY ./000-default.conf /etc/apache2/sites-available/

RUN apt-get update && \
	apt-get install vim -y && \
    apt-get install -y postfix && \
    sed -i 's/512M/512M/g' /etc/php5/apache2/php.ini && \
    sed -i 's/post_max_size\ =\ 512M/post_max_size\ =\ 512M/g' /etc/php5/apache2/php.ini

COPY moodle/ /app/

COPY run.sh /run.sh

RUN chmod +x /run.sh
